
// 必要設定
// var ezecEndpoint = "http://test.eztravel.com.tw/ezec/m2mlogin.jsp";
// var loginUrl = @routes.AjaxController.login()
// var loginFormUrl = @routes.AjaxController.login()
 // var isAuthenticated = false;

var loginForm =  function() {
	
		if(typeof loginUrl === 'undefined' || typeof loginFormUrl === 'undefined') {
			return;
		}
		
        $.ajax({
            url: loginFormUrl,
            type: 'GET',
            cache: false,
            dataType: 'html',
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
            success: function(loginFormData, textStatus, jqXHR) {

                $("#loginFormContainer").html(loginFormData);
                if(loginFormData == "") {
                	isAuthenticated = true;
                    uploadForm();	// 顯示上傳 Form - 小心可能會有無迴圈
                    return;
                }
                
                var $loginFormDialog = $("#loginFormContainer");
                $loginFormDialog.hide();
                $loginFormDialog.dialog({                	                	
                    title: '使用者登入',
                    width: 400,
                    height: 240,
                    modal: true,
                    show: 'slow',
                    hide: 'slow',
                    draggable: true,
                    resizable: false,
                    buttons: {
                        '登入': function() {
                            if ($("#loginForm").validationEngine("validate")) {
              
                            	var id = $("#loginId").val();
                                var password = $("#loginPwd").val();

                                // 透過官網進行身份認證
                                // 會員身份檢核結果（範例：m2m({role:"B2C",status:-1,token:"null"});）
                                $.jsonp({
                                    url: ezecEndpoint + "?id=" + id + "&password= " + password + "&role=WEBORDER",
                                    callback: "m2m",
                                    success: function (data, textStatus, xOptions) {
                                        if( data.status==0){
                                            $("#loginToken").val(data.token);
//                                            $("#loginForm").submit();

                                            var formData = new FormData();
                                            formData.append("loginId", data.token);

                                            $.ajax({
                                                url: loginUrl,
                                                type: 'POST',
                                                data: formData,
                                                cache: false,
                                                dataType: 'html',
                                                processData: false, // Don't process the files
                                                contentType: false, // Set content type to false as jQuery will tell the server its a query string request
                                                success: function(data, textStatus, jqXHR)
                                                {
                                                    var test = 0;
                                                    if(typeof data.error === 'undefined')
                                                    {
                                                        // 顯示 import 的對話框
                                                    	isAuthenticated = true;
                                                    	$loginFormDialog.dialog("close");
                                                    	uploadForm();	// 顯示上傳 Form - 小心可能會有無迴圈
                                                    } else {
                                                        alert("登入失敗!");
                                                    }
                                                },
                                                error:function(xhr, ajaxOptions, thrownError){
                                                    alert("登入驗証失敗!");
                                                }
                                            });
                                        }else{
                                            alert("帳號密碼錯誤");
                                        }
                                    },
                                    error: function (xOptions, textStatus) {
                                        alert("很抱歉，本網目前正在進行系統升級作業，\n\n部分服務將暫時無法使用，為提供更完善的服務，請稍候再試。\n\n\n不便之處，敬請見諒！");
                                    }
                                });
                            }
                        },
                        '關閉': function () {
                            $(this).dialog('close');
                        }
                    }
                });
            }
        });
};      