
// 必要設定
// var uploadFormUrl = '@routes.Index.submit2()', for uploadFilesToServer 

var uploadForm = function(){
	// 先檢查是否有 Login
	if(isAuthenticated == false) {
		loginForm();
	}     
	
     if(isAuthenticated == false) return;

    $("input[type=file]").val("");
     //$("input").val("");
    $("select", "#uploadFormContainer").val("");

    $("#uploadFormContainer").dialog({
    	title: '旅遊平台行程資料匯入',
                    //                                    bgiframe: true,
        width: 640,
        height: 520,
        modal: true,
        show: 'slow',
        hide: 'slow',
        draggable: true,
        resizable: false,
                    //                                    overlay:{opacity: 0.7 , background: "#FF8899"},
        buttons: {
        	'關閉': function () {
        		$(this).dialog('close');
             }
        }
    });
 }

var uploadFilesToServer = function(submitForm) {
	
//        event.preventDefault();
//        var submitForm = $(this);
//        $("input[type=submit]").attr('disabled','disabled');

    $("input[type=submit]").hide("slow");
    $(".wait", "#uploadFormContainer").show();
//            return;

    var selectedFile = 0;
    var errMsg = "";
    var vendNo = $("select", submitForm).val();
    $(".bg-danger", submitForm).html("");

    if(vendNo == "") {    // 供應商
        errMsg += "請輸入平台供應商!\n";
        return false;
    }

    // 驗証上傳資料
    $("input[type=file]", submitForm).each(function (index, e) {
        var files = this.files;
        var label = $(e).parent().prev();

        selectedFile += files.length;

        if (files.length > 0 && files.length % 7 != 0) {
            errMsg += label.text() + "上傳檔案數不為 7 的倍數!" + "\n"
            selectedFile = 0;
            return false;
        }

        for (var i = 0; i < files.length; i++) {
            var file = files[i];

            if (!file.name.match(/1_prod.xlsx|2_prod_imgs.xlsx|3_airinfo.xlsx|4_d_imgs.xlsx|5_scheduled.xlsx|6_prod_price.xlsx|7_allot.xlsx/)) {
                errMsg += label.text() + "上傳檔案檔名 " + file.name + "不符合上傳檔名規定, 請檢查!" + "\n"
                selectedFile = 0;
                return;
            }
        }
    });

    if(selectedFile < 1) {
        alert("請選擇要上傳的行程資料!");
//            $("input[type=submit]").removeAttr('disabled');
        $("input[type=submit]").show("slow");
        $(".wait", "#uploadFormContainer").hide();
        return false;
    }

    if(errMsg != "") {
        alert(errMsg);
//            $("input[type=submit]").removeAttr('disabled');
        $("input[type=submit]").show("slow");
        $(".wait", "#uploadFormContainer").hide();
        return false;
    }

    // Create a new FormData object.
    var formData = new FormData();
    formData.append("vendNo", vendNo);
    // 上傳資料
    $("input[type=file]", submitForm).each(function(index, e){
        var files = this.files;

        if(files.length > 0) {
            for(var i = 0; i < files.length; i++) {
                var file_group_key;
                // webkitRelativePath: "1576/2/1_prod.xlsx"
                if($.trim(files[i].webkitRelativePath) != "") {
                    // 依目錄整批上傳, 目前適用 chrome & opera
                    var path = files[i].webkitRelativePath;
                    file_group_key = path.substring(0, path.length - files[i].name.length);
                } else {
                        file_group_key = $(e).attr("id")
                }
                formData.append(file_group_key, files[i], files[i].name);  // for 檔案上傳
            }
        }
    });

    $.ajax({
        url: '@routes.Index.submit2()',
        type: 'POST',
        data: formData,
        cache: false,
        dataType: 'html',
        processData: false, // Don't process the files
        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
        success: function(data, textStatus, jqXHR) {

            if(typeof data.error === 'undefined') {

                if(data.indexOf("table") != -1) {   // 回傳 table 格式的代表正常回傳
                    // Success so call function to process the form
                    //                            submitForm(event, data);
                    $("#result_container").html(data);

                    $('#myTab li:eq(2) a').show(); // Select third tab (0-indexed)
                    $('#myTab li:eq(2) a').tab("show");

                    $("#result_container table")
                        .dataTable({"bPaginate": true,
                                    "bPaginateTop": true,
                                    "bLengthChange": false,
                                    "searching": false,
                                    "bFilter": true,
                                    "bSort": false,
                                    "bInfo": false,
                                    "iDisplayLength": 30,
                                    "sZeroRecords": "查無符合結果",
                                    "sPaginationType": "full_numbers",
                                    "bAutoWidth": false
                        });
                        
                        $("input[type=file]").val("");
                } else {
                    $(".bg-danger", submitForm).html(data);
                }
            } else {
                    // Handle errors here
                console.log('ERRORS: ' + data.error);
                $(".bg-danger", submitForm).html(jqXHR.responseText);
//                        alert('ERRORS: ' + textStatus)
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            // Handle errors here
            console.log('ERRORS: ' + jqXHR.statusText);
            $(".bg-danger", submitForm).html(jqXHR.statusText);
//                    $('#myTab li:eq(0) a').tab("show");
                // STOP LOADING SPINNER
        },
        done: function() {
            $("input[type=submit]").show("slow");
            $(".wait", "#uploadFormContainer").hide();
        }
    });
//        $("input[type=submit]").removeAttr('disabled');
}