
// 匯入明細查詢
$(function(){

    //        		$('#table01').DataTable({
    //        			"ordering" : false,
    //        			"paging" : false,
    //        			"info" : false
    //        		});
    $("#waitIcon").hide();
    $(".wait", "#uploadFormContainer").hide();
    // 匯入日期
    $("#import_date").datepicker();
    $("#import_date").datepicker("option", "dateFormat", "yy/mm/dd");
    $("#import_date").datepicker("option", $.datepicker.regional["zh-TW"]);

    mainViewToDataTable();

    // nav
    $('#myTab a').click(function (e) {
        e.preventDefault()
        $(this).tab('show')
    })

    $('#myTab li:eq(2) a').hide(); // Select third tab (0-indexed)
    $("#uploadFormContainer").hide();

    // 行程上傳
    $("form").on("submit", function(event) {
        event.preventDefault();
        var submitForm = $(this);

        uploadFilesToServer(submitForm);

        return false;
    });

    //                $("#table01").dataTable({
    //                    "bPaginate": true,
    //                        "bPaginateTop": true,
    //                        "bLengthChange": false,
    //                        "bFilter": false,
    //                        "bSort": false,
    //                        "bInfo": false,
    //                        "iDisplayLength":20,
    //                        "sPaginationType":"full_numbers",
    //                        "bAutoWidth": false
    //                });



    //		$('#vend_no').selectpicker();

    // 匯入商品 form
    $("#btnUpload").click(uploadForm);

    // 查詢按鈕
    $("#btnSearch").click(function() {
        var vend_no = $("#vend_no").val();
        var import_date = $("#import_date").val();

        var msg = "";
        if ($.trim(vend_no) == "") {
            msg = "請輸入平台供應商!"
            // $("#vend_no").focus();
        } else if ($.trim(import_date) == "") {
            msg = "請輸入匯入日期!"
            //	$("#import_date").focus();
        }

        if (msg != "") {
            // 檢核失敗
            $("#alert").html(msg);
            $("#alert").dialog({
                modal: true,
                buttons: {
                    '關閉': function () {
                        $("#alert").html("");
                        $(this).dialog('close');
                    }
                }});
        } else {
            // 成功 - 查詢資料
            $("#waitIcon").show();
            var requestUrl = "/ajax/listMain/vendNo/importDate?vendNo=" + vend_no + "&importDate=" + import_date.replace(/\//g, "");

            $.ajax({type: "GET",
                url: requestUrl,
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#table_container").html("<h2>" + xhr.responseText + "</h2>");
                }})
                .done(function (htmlContent) {
                    $("#table_container").html(htmlContent);

                    mainViewToDataTable();

                    $("td div a").click(function (event) {
                        event.preventDefault();
                        displayDetailDialog(this);
                        return false;
                    });
                });
            $("#waitIcon").hide();
        }
    });

    $("td div a").click(function (event) {
        event.preventDefault();
        displayDetailDialog(this);
        return false;
    });
});

var mainViewToDataTable = function() {
    $("#groupInfo").dataTable({
        "bPaginate": true,
        "bPaginateTop": true,
        "bLengthChange": false,
        "searching": false,
        "bFilter": true,
        "bSort": false,
        "bInfo": false,
        "iDisplayLength": 20,
        "sZeroRecords": "查無符合結果",
        "sPaginationType": "full_numbers",
        "bAutoWidth": false
    });
}

//  顯示明細對話窗
var displayDetailDialog = function(anchor) {

    var jobNo = $(anchor).attr("jobNo");
    $("#waitIcon").show();
    $.ajax({
        type: "GET",
        url: $(anchor).attr("href")
    }).done(function (htmlContent) {
        $("#detailFormContainer").html(htmlContent);
        //                                $("#table_detail").dataTable({
        //                                    "bPaginate": true,
        //                                    "bPaginateTop": true,
        //                                    "bLengthChange": false,
        //                                    "bFilter": true,
        //                                    "bSort": false,
        //                                    "bInfo": false,
        //                                    "iDisplayLength":20,
        //                                    "sPaginationType":"full_numbers",
        //                                    "bAutoWidth": false});

        $("#waitIcon").hide();

        $("#detailFormContainer").dialog({
            title: '工作單號: ' + jobNo + ' 匯入錯誤明細列表',
            //                                    bgiframe: true,
            width: 850,
            height: 600,
            modal: true,
            show: 'slow',
            hide: 'slow',
            draggable: true,
            resizable: true,
            //                                    overlay:{opacity: 0.7 , background: "#FF8899"},
            buttons: {
                '關閉': function () {
                    $("#detailFormContainer").html("");
                    $(this).dialog('close');
                }
            }
        });
    });
}
        //                                $("#import_detail").dialog({
        //                                    resizable: true,
        //                                    width: 600,
        //                                    height: 400,
        //                                    modal: true,
        //                                    buttons: {
        //                                        "關閉": function () {
        //                                            $(this).dialog("close");
        //                                        }
        //                                    }
        //                                });



