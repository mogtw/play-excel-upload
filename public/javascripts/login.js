var ezecEndpoint = "http://test.eztravel.com.tw/ezec/m2mlogin.jsp";

$(document).ready(function(){
    $("#loginButton").click(function() {
        if ($("#loginForm").validationEngine("validate")) {
            var id = $("#loginId").val();
            var password = $("#loginPwd").val();
            
            // 會員身份檢核結果（範例：m2m({role:"B2C",status:-1,token:"null"});）
            $.jsonp({
                url: ezecEndpoint + "?id=" + id + "&password= " + password + "&role=WEBORDER",
                callback: "m2m",
                success: function (data, textStatus, xOptions) {
                    if( data.status==0){
                        $("#loginToken").val(data.token);
                        $("#loginForm").submit();
                    }else{
                        alert("帳號密碼錯誤");
                    }
                },
                error: function (xOptions, textStatus) {
                    alert("很抱歉，本網目前正在進行系統升級作業，\n\n部分服務將暫時無法使用，為提供更完善的服務，請稍候再試。\n\n\n不便之處，敬請見諒！");
                }
            });
        }
    });
    $("#loginForm").validationEngine({autoPositionUpdate: true,promptPosition: 'inline'});    
});