/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  models.enums
 * @FileName: BusinessType.java
 * @author:   tonywang
 * @date:     2013/12/15, 下午 12:47:14
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package models.enums;

/**
 * Business Type.
 * 
 * @author CJWang
 */
public enum BusinessType {
  
  /** The B2 b. */
  B2B, 
 /** The B2 c. */
 B2C, 
 /** The B2 e. */
 B2E, 
 /** The weborder. */
 WEBORDER
}
