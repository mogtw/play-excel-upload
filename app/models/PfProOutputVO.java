package models;

public class PfProOutputVO {
	private int seqNo; // 序號
	private String jobNo;
	private String jobDesc; // 1-500 筆, 501-1000 筆
	private String jobStatus;

	public PfProOutputVO(int seqNo, String jobNo, String jobDesc,
			String jobStatus) {
		this.seqNo = seqNo;
		this.jobNo = jobNo;
		this.jobDesc = jobDesc;
		this.jobStatus = jobStatus;
	}

	public int getSeqNo() {
		return seqNo;
	}

	public void setSeqNo(int seqNo) {
		this.seqNo = seqNo;
	}

	public String getJobNo() {
		return jobNo;
	}

	public void setJobNo(String jobNo) {
		this.jobNo = jobNo;
	}

	public String getJobDesc() {
		return jobDesc;
	}

	public void setJobDesc(String jobDesc) {
		this.jobDesc = jobDesc;
	}

	public String getJobStatus() {
		return jobStatus;
	}

	public void setJobStatus(String jobStatus) {
		this.jobStatus = jobStatus;
	}
}
