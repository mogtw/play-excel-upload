package models.pojo;

public class FileInfo {
	private String fileName;			// 檔名
	private String fileDescription;	// 檔案描述
	
	public FileInfo() {
				
	}
	
	public FileInfo(String fileName, String fileDescription) {
		this.fileName =fileName;
		this.fileDescription = fileDescription;
	}
	
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFileDescription() {
		return fileDescription;
	}
	public void setFileDescription(String fileDescription) {
		this.fileDescription = fileDescription;
	}
	
	
}
