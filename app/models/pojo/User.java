/**
 * EZTRAVEL CONFIDENTIAL
 * 
 * @Package: models.pojo
 * @FileName: User.java
 * @author: CJWang
 * @date: 2014/4/4, 下午 02:26:33
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package models.pojo;

import models.enums.BusinessType;

/**
 * 使用者資訊.
 * 
 * @author CJWang
 */
public class User {

  /** The Constant CONTEXT_KEY. */
  public static final String CONTEXT_KEY = "user";
  /** custNo 已編碼. */
  private String custNo;

  /** The business type. */
  private BusinessType businessType;

  /**
   * <ul>
   * <li>userId (WebOrder)</li>
   * <li>_bbb (personId in B2B)</li>
   * <li>personId (B2E)</li>
   * </ul>
   * .
   */
  private String loginId;

  /** 僅WebOrder、B2E. */
  private String loginPwd;

  /** 使用者ip. */
  private String ip;

  /** The session id. */
  private String sessionId;

  /**
   * Gets the cust no.
   * 
   * @return custNo 已編碼
   */
  public String getCustNo() {
    return custNo;
  }

  /**
   * Sets the cust no.
   * 
   * @param custNo custNo 已編碼
   */
  public void setCustNo(String custNo) {
    this.custNo = custNo;
  }

  /**
   * Gets the business type.
   * 
   * @return the businessType
   */
  public BusinessType getBusinessType() {
    return businessType;
  }

  /**
   * Sets the business type.
   * 
   * @param businessType the businessType to set
   */
  public void setBusinessType(BusinessType businessType) {
    this.businessType = businessType;
  }

  /**
   * Gets the login id.
   * 
   * @return <ul>
   *         <li>userId (WebOrder)</li>
   *         <li>_bbb (personId in B2B)</li>
   *         <li>personId (B2E)</li>
   *         </ul>
   */
  public String getLoginId() {
    return loginId;
  }

  /**
   * Sets the login id.
   * 
   * @param loginId <ul>
   *        <li>userId (WebOrder)</li>
   *        <li>_bbb (personId in B2B)</li>
   *        <li>personId (B2E)</li>
   *        </ul>
   */
  public void setLoginId(String loginId) {
    this.loginId = loginId;
  }

  /**
   * 僅WebOrder、B2E.
   * 
   * @return the login pwd
   */
  public String getLoginPwd() {
    return loginPwd;
  }

  /**
   * 僅WebOrder、B2E.
   * 
   * @param loginPwd the new login pwd
   */
  public void setLoginPwd(String loginPwd) {
    this.loginPwd = loginPwd;
  }

  /**
   * Gets the ip.
   * 
   * @return the ip
   */
  public String getIp() {
    return ip;
  }

  /**
   * Sets the ip.
   * 
   * @param ip the new ip
   */
  public void setIp(String ip) {
    this.ip = ip;
  }

  /**
   * Gets the session id.
   * 
   * @return the session id
   */
  public String getSessionId() {
    return sessionId;
  }

  /**
   * Sets the session id.
   * 
   * @param sessionId the new session id
   */
  public void setSessionId(String sessionId) {
    this.sessionId = sessionId;
  }


}
