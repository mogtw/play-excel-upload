package models;

public class VndMainVO {
	public String vendNo;
	public String vendNameCh;
	public boolean selected;
		
	public VndMainVO() {
		super();
	}
	
	public VndMainVO(String vendNo, String vendNameCh) {	
		this.vendNo = vendNo;
		this.vendNameCh = vendNameCh;
		this.selected = false;
	}
				
	public String getVendNo() {
		return vendNo;
	}

	public void setVendNo(String vendNo) {
		this.vendNo = vendNo;
	}

	public String getVendNameCh() {
		return vendNameCh;
	}

	public void setVendNameCh(String vendNameCh) {
		this.vendNameCh = vendNameCh;
	}

	public boolean isSelected() {
		return selected;
	}
	
	public void setSelected(boolean selected) {
		this.selected = selected;
	}		
}
