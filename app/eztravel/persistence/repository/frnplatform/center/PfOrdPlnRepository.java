/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.persistence.repository.frnplatform.center
 * @FileName: PfOrdPlnRepository.java
 * @author:   Ian Chen
 * @date:     2014/9/23, 上午 11:23:22
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.persistence.repository.frnplatform.center;

import eztravel.core.pojo.frnplatform.PfOrdPln;
import org.apache.ibatis.annotations.Param;

/**
 * <pre> PfOrdPlnRepository </pre>
 *
 * @author Ian Chen
 */
public interface PfOrdPlnRepository {
  
  /**
   * Select by primary key.
   * 
   * @param orderNo the order no
   * @param custSeqno the cust seqno
   * @param prodSeqno the prod seqno
   * @param roundCd the round cd
   * @param seqNo the seq no
   * @return the pf ord pln
   */
  PfOrdPln selectByPrimaryKey(@Param("orderNo") String orderNo, @Param("custSeqno") Short custSeqno, @Param("prodSeqno") Short prodSeqno, @Param("roundCd") Short roundCd, @Param("seqNo") Short seqNo);
}