/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.persistence.repository.frnplatform
 * @FileName: CodeDetailMapper.java
 * @author:   Ian Chen
 * @date:     2014/8/20, 下午 12:07:28
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.persistence.repository.frnplatform.center;

import eztravel.core.pojo.frnplatform.CodeDetail;

import java.util.List;

import org.apache.ibatis.annotations.Param;

/**
 * <pre> CodeDetailMapper </pre>
 *
 * @author Ian Chen
 */
public interface CodeDetailRepository {

  /**
   * Select by item id.
   * 
   * @return the list
   */
  List<CodeDetail> selectByItemId(String itemId);

  /**
   * Select by item id and code name.
   * 
   * @param itemId the item id
   * @param codeName the code name
   * @return the string
   */
  String selectByItemIdAndCodeName(@Param("itemId") String itemId, @Param("codeName") String codeName);
}