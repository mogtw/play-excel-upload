/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.persistence.repository.frnplatform
 * @FileName: PfProImpMainMapper.java
 * @author:   Ian Chen
 * @date:     2014/8/25, 下午 05:27:43
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.persistence.repository.frnplatform.center;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import eztravel.core.pojo.frnplatform.PfProImpMain;

/**
 * <pre> PfProImpMainMapper </pre>
 * 
 * @author Ian Chen
 */
public interface PfProImpMainRepository {

  /**
   * Select by primary key.
   * 
   * @param jobNo the job no
   * @return the pf pro imp main
   */
  PfProImpMain selectByPrimaryKey(Long jobNo);
  
  /**
   * Select by vend no and create date.
   * 
   * @param vendNo the vend no
   * @param createDt the create dt
   * @return the list
   */
  List<PfProImpMain> selectByVendNoAndCreateDate(@Param("vendNo") String vendNo,
      @Param("createDt") Date createDt);
  
  /**
   * Select next job no.
   * 
   * @return the long
   */
  Long selectNextJobNo();

  /**
   * Select next b group no.
   * 
   * @return the long
   */
  Integer selectNextBGroupNo();

  /**
   * Select processed count.
   * 
   * @param bGroupNo the b group no
   * @return the short
   */
  Short selectProcessedCount(Integer bGroupNo);

  /**
   * Select success percent.
   * 
   * @param bGroupNo the b group no
   * @return the big decimal
   */
  BigDecimal selectSuccessPercent(Integer bGroupNo);

  /**
   * Insert selective.
   * 
   * @param record the record
   * @return the int
   */
  int insertSelective(PfProImpMain record);
  
  /**
   * Update job status by primary key.
   * 
   * @param record the record
   * @return the int
   */
  int updateJobStatusByPrimaryKey(PfProImpMain record);
  
  /**
   * Update by primary key.
   * 
   * @param record the record
   * @return the int
   */
  int updateByPrimaryKey(PfProImpMain record);
}