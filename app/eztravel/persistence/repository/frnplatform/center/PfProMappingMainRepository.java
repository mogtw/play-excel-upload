/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.persistence.repository.frnplatform
 * @FileName: PfProMappingMainMapper.java
 * @author:   Ian Chen
 * @date:     2014/8/21, 上午 10:41:40
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.persistence.repository.frnplatform.center;

import eztravel.core.pojo.frnplatform.PfProMappingMain;

/**
 * <pre> PfProMappingMainMapper </pre>
 * 
 * @author Ian Chen
 */
public interface PfProMappingMainRepository {
  
  /**
   * Insert.
   * 
   * @param record the record
   * @return the int
   */
  int insert(PfProMappingMain record);
  
  /**
   * Insert selective.
   * 
   * @param record the record
   * @return the int
   */
  int insertSelective(PfProMappingMain record);
}
