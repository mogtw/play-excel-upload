/**
 * EZTRAVEL CONFIDENTIAL
 * 
 * @Package: eztravel.persistence.repository.edb.frnplatform
 * @FileName: FrnPlatFormQueryRepository.java
 * @author: allenyen
 * @date: 2014/9/16, 上午 11:34:57
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.persistence.repository.frnplatform.center;


import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import eztravel.core.pojo.frnplatform.CodeDetail;
import eztravel.core.pojo.frnplatform.PfPro;
import eztravel.core.pojo.frnplatform.PfProAirinfo;
import eztravel.core.pojo.frnplatform.PfProDScheduled;
import eztravel.core.pojo.frnplatform.PfProPrice;
import eztravel.core.pojo.frnplatform.PfProRef;


/**
 * The Interface FrnPlatFormQueryRepository.
 * 
 * <pre>
 * 
 * </pre>
 */

public interface FrnPlatFormQueryEdbRepository {



  /**
   * Query code detail.
   * 
   * @param itemId the item id
   * @param codeId the code id
   * @return the code detail
   */
  public CodeDetail selectCodeDetail(@Param("itemId") String itemId, @Param("codeId") String codeId);

  /**
   * Query pfpro.
   * 
   * @param pfProdNo the pf prod no
   * @param vendNo the vend no
   * @return the tblpfpro
   */
  public PfPro selectPfpro(@Param("pfProdNo") String pfProdNo, @Param("vendNo") String vendNo);

  /**
   * Query pfpro ref.
   * 
   * @param pfProdNo the pf prod no
   * @param vendNo the vend no
   * @return the tblpfpro ref
   */
  public PfProRef selectPfproRef(@Param("pfProdNo") String pfProdNo,
      @Param("vendNo") String vendNo);



  /**
   * Query pfpro airinfo.
   * 
   * @param pfProdNo the pf prod no
   * @param vendNo the vend no
   * @return the list
   */
  public List<PfProAirinfo> queryPfproAirinfo(@Param("pfProdNo") String pfProdNo,
      @Param("vendNo") String vendNo);


  /**
   * Query pfpro d scheduled.
   * 
   * @param pfProdNo the pf prod no
   * @param vendNo the vend no
   * @return the list
   */
  public List<PfProDScheduled> queryPfproDScheduled(@Param("pfProdNo") String pfProdNo,
      @Param("vendNo") String vendNo);


  public PfProPrice selectPfproPrice(
      @Param("selectPfproPriceParams") Map<String, Object> selectPfproPriceParams);

}
