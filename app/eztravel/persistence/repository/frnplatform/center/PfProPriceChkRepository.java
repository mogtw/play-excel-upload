/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.persistence.repository.frnplatform
 * @FileName: PfProPriceChkMapper.java
 * @author:   Ian Chen
 * @date:     2014/8/28, 下午 02:32:48
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.persistence.repository.frnplatform.center;

import eztravel.core.pojo.frnplatform.PfProPriceChk;
import org.apache.ibatis.annotations.Param;

/**
 * <pre> PfProPriceChkMapper </pre>
 * 
 * @author Ian Chen
 */
public interface PfProPriceChkRepository {

  /**
   * Select by primary key.
   * 
   * @param vendNo the vend no
   * @param countryCd the country cd
   * @return the pf pro price chk
   */
  PfProPriceChk selectByPrimaryKey(@Param("vendNo") String vendNo, @Param("countryCd") String countryCd);
}