/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.persistence.repository.frnplatform.center
 * @FileName: PfOrdProdExtendRepository.java
 * @author:   Ian Chen
 * @date:     2014/9/23, 上午 11:23:43
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.persistence.repository.frnplatform.center;

import eztravel.core.pojo.frnplatform.PfOrdProdExtend;
import org.apache.ibatis.annotations.Param;

/**
 * <pre> PfOrdProdExtendRepository </pre>
 *
 * @author Ian Chen
 */
public interface PfOrdProdExtendRepository {
  
  /**
   * Select by primary key.
   * 
   * @param orderNo the order no
   * @param custSeqno the cust seqno
   * @param prodSeqno the prod seqno
   * @return the pf ord prod extend
   */
  PfOrdProdExtend selectByPrimaryKey(@Param("orderNo") String orderNo, @Param("custSeqno") Short custSeqno, @Param("prodSeqno") Short prodSeqno);
}