/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.persistence.repository.frnplatform
 * @FileName: PfProPriceRuleMapper.java
 * @author:   Ian Chen
 * @date:     2014/8/27, 下午 05:57:13
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.persistence.repository.frnplatform.center;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import eztravel.core.pojo.frnplatform.PfProPriceRule;

/**
 * <pre> PfProPriceRuleMapper </pre>
 * 
 * @author Ian Chen
 */
public interface PfProPriceRuleRepository {

  /**
   * Select by primary key.
   * 
   * @param prodNo the prod no
   * @param vendNo the vend no
   * @return the pf pro price rule
   */
  PfProPriceRule selectByPrimaryKey(@Param("prodNo") String prodNo, @Param("vendNo") String vendNo);
  
  List<PfProPriceRule> selectByVendNo(String vendNo);
}