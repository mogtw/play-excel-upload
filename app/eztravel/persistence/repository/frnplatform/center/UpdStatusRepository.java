/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.persistence.repository.frnplatform
 * @FileName: UpdStatusMapper.java
 * @author:   Ian Chen
 * @date:     2014/9/4, 上午 10:24:16
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.persistence.repository.frnplatform.center;

import org.apache.ibatis.annotations.Param;

import eztravel.core.pojo.frnplatform.UpdStatus;
import eztravel.core.pojo.frnplatform.VenderUpdStatusCount;

/**
 * <pre> UpdStatusMapper </pre>
 *
 * @author Ian Chen
 */
public interface UpdStatusRepository {
  
  /**
   * Select by primary key.
   * 
   * @param vendNo the vend no
   * @param tableName the table name
   * @return the upd status
   */
  UpdStatus selectByPrimaryKey(@Param("vendNo") String vendNo, @Param("tableName") String tableName);
  
  /**
   * Select status count.
   * 
   * @param vendNo the vend no
   * @return the vender upd status count
   */
  VenderUpdStatusCount selectStatusCount(String vendNo);
  
  /**
   * Update upd status by vend no.
   * 
   * @param record the record
   * @return the int
   */
  int updateUpdStatusByVendNo(UpdStatus record);
}