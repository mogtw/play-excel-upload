/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.persistence.repository.frnplatform
 * @FileName: PfProMapper.java
 * @author:   Ian Chen
 * @date:     2014/8/20, 下午 02:59:25
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.persistence.repository.frnplatform.center;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import eztravel.core.pojo.frnplatform.PfPro;
import eztravel.core.pojo.frnplatform.PfProSearch;

/**
 * <pre> PfProMapper </pre>
 *
 * @author Ian Chen
 */
public interface PfProRepository {

  /**
   * Select all.
   * 
   * @return the list
   */
  List<PfPro> selectAll();
  
  /**
   * Select by pf prod no and vend no.
   * 
   * @param pfProdNo the pf prod no
   * @param vendNo the vend no
   * @return the list
   */
  List<PfPro> selectByPfProdNoAndVendNo(@Param("pfProdNo") String pfProdNo, @Param("vendNo") String vendNo);

  /**
   * Select by search condition.
   * 
   * @param conditions the conditions
   * @return the list
   */
  List<PfProSearch> selectBySearchCondition(Map<String, Object> conditions);

  /**
   * Select count by search condition.
   * 
   * @param conditions the conditions
   * @return the int
   */
  int selectCountBySearchCondition(Map<String, Object> conditions);

  /**
   * Insert selective.
   * 
   * @param record the record
   * @return the int
   */
  int insertSelective(PfPro record);

  /**
   * Delete all.
   * 
   * @param tablePrefix the table prefix
   * @return the int
   */
  int deleteAll(String tablePrefix);
}