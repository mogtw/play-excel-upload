/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.persistence.repository.frnplatform
 * @FileName: PfProRefMapper.java
 * @author:   Ian Chen
 * @date:     2014/8/27, 下午 05:23:43
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.persistence.repository.frnplatform.center;

import org.apache.ibatis.annotations.Param;

import eztravel.core.pojo.frnplatform.PfProRef;

/**
 * <pre> PfProRefMapper </pre>
 * 
 * @author Ian Chen
 */
public interface PfProRefRepository {
  
  /**
   * Select by primary key.
   * 
   * @param pfProdNo the pf prod no
   * @param vendNo the vend no
   * @return the pfpro ref
   */
  PfProRef selectByPrimaryKey(@Param("pfProdNo") String pfProdNo, @Param("vendNo") String vendNo);
  
  /**
   * Insert selective.
   * 
   * @param record the record
   * @return the int
   */
  int insertSelective(PfProRef record);
  
  /**
   * Delete all.
   * 
   * @param tablePrefix the table prefix
   * @return the int
   */
  int deleteAll(String tablePrefix);
}