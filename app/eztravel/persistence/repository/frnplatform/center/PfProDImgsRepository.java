/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.persistence.repository.frnplatform
 * @FileName: PfProDImgsMapper.java
 * @author:   Ian Chen
 * @date:     2014/8/20, 下午 02:18:14
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.persistence.repository.frnplatform.center;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import eztravel.core.pojo.frnplatform.PfProDImgs;

/**
 * <pre> PfProDImgsMapper </pre>
 *
 * @author Ian Chen
 */
public interface PfProDImgsRepository {
  
  /**
   * Select by pf prod no and vend no.
   * 
   * @param pfProdNo the pf prod no
   * @param vendNo the vend no
   * @return the list
   */
  List<PfProDImgs> selectByPfProdNoAndVendNo(@Param("pfProdNo") String pfProdNo, @Param("vendNo") String vendNo);

  /**
   * Insert selective.
   * 
   * @param record the record
   * @return the int
   */
  int insertSelective(PfProDImgs record);

  /**
   * Delete all.
   * 
   * @param tablePrefix the table prefix
   * @return the int
   */
  int deleteAll(String tablePrefix);
}