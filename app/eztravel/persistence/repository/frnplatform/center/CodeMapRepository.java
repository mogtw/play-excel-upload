/**
 * EZTRAVEL CONFIDENTIAL
 * 
 * @Package: eztravel.persistence.repository.frnplatform.center
 * @FileName: CodeMapMapper.java
 * @author: CJWang
 * @date: 2014/9/22, 下午 04:41:06
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.persistence.repository.frnplatform.center;

import java.util.List;

import eztravel.core.pojo.frnplatform.CodeMap;

/**
 * The Interface CodeMapMapper.
 * 
 * <pre>
 * 
 * </pre>
 */
public interface CodeMapRepository {

  /**
   * Select all.
   * 
   * @return the list
   */
  public List<CodeMap> selectAll();
}
