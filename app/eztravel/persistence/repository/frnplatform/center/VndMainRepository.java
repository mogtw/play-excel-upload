/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.persistence.repository.frnplatform
 * @FileName: VndMainMapper.java
 * @author:   Ian Chen
 * @date:     2014/9/5, 下午 02:54:49
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.persistence.repository.frnplatform.center;

import java.util.List;

import eztravel.core.pojo.frnplatform.VndMain;

/**
 * <pre> VndMainMapper </pre>
 * 
 * @author Ian Chen
 */
public interface VndMainRepository {

  /**
   * Select by primary key.
   * 
   * @param vendNo the vend no
   * @return the vnd main
   */
  VndMain selectByPrimaryKey(String vendNo);

  /**
   * Select all.
   * 
   * @return the list
   */
  List<VndMain> selectAll();
  
  /**
   * Update b group no by primary key.
   * 
   * @param record the record
   * @return the int
   */
  int updateBGroupNoByPrimaryKey(VndMain record);
}