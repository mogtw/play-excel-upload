/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.persistence.repository.frnplatform.center
 * @FileName: OrdPfProDScheduledRepository.java
 * @author:   Ian Chen
 * @date:     2014/9/23, 上午 11:18:03
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.persistence.repository.frnplatform.center;

import org.apache.ibatis.annotations.Param;

import eztravel.core.pojo.frnplatform.OrdPfProDScheduled;

/**
 * <pre> OrdPfProDScheduledRepository </pre>
 * 
 * @author Ian Chen
 */
public interface OrdPfProDScheduledRepository {

  /**
   * Select by primary key.
   * 
   * @param orderNo the order no
   * @param pfProdNo the pf prod no
   * @param vendNo the vend no
   * @param days the days
   * @param timeSeq the time seq
   * @return the ord pf pro d scheduled
   */
  OrdPfProDScheduled selectByPrimaryKey(@Param("orderNo") String orderNo,
      @Param("pfProdNo") String pfProdNo, @Param("vendNo") String vendNo,
      @Param("days") String days, @Param("timeSeq") String timeSeq);

  /**
   * Insert selective.
   * 
   * @param record the record
   * @return the int
   */
  int insertSelective(OrdPfProDScheduled record);
}