/**
 * EZTRAVEL CONFIDENTIAL
 * 
 * @Package: eztravel.persistence.repository.edb.frnplatform
 * @FileName: FrnPlatFormOrderEdbRepository.java
 * @author: allenyen
 * @date: 2014/9/17, 下午 02:45:25
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.persistence.repository.frnplatform.center;

import java.util.Map;

import org.apache.ibatis.annotations.Param;

/**
 * The Interface FrnPlatFormOrderEdbRepository.
 * 
 * <pre>
 * 
 * </pre>
 */

public interface FrnPlatFormOrderEdbRepository {

  /**
   * 新增訂單行程內容.
   * 
   * @param insertOrdpfproDScheduledParams the insert ordpfpro d scheduled params
   */
  public void insertOrdpfproDSchedule(
      @Param("insertOrdpfproDScheduledParams") Map<String, Object> insertOrdpfproDScheduledParams);

  /**
   * 新增訂單主商品內容.
   * 
   * @param insertOrdpfproParams the insert ordpfpro params
   */
  public void insertOrdpfpro(@Param("insertOrdpfproParams") Map<String, Object> insertOrdpfproParams);



  /**
   * 新增訂單主商品內容參考資訊.
   * 
   * @param insertOrdpfproRefParams the insert ordpfpro ref params
   */
  public void insertOrdpfproRef(
      @Param("insertOrdpfproRefParams") Map<String, Object> insertOrdpfproRefParams);


}
