/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.persistence.repository.frnplatform
 * @FileName: PfProImpDetailMapper.java
 * @author:   Ian Chen
 * @date:     2014/8/25, 下午 05:27:28
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.persistence.repository.frnplatform.center;

import java.util.List;

import eztravel.core.pojo.frnplatform.PfProImpDetail;

import org.apache.ibatis.annotations.Param;

/**
 * <pre> PfProImpDetailMapper </pre>
 * 
 * @author Ian Chen
 */
public interface PfProImpDetailRepository {
    
  /**
   * Select by primary key.
   * 
   * @param jobNo the job no
   * @param pfProdNo the pf prod no
   * @return the pf pro imp detail
   */
  PfProImpDetail selectByPrimaryKey(@Param("jobNo") Long jobNo, @Param("pfProdNo") String pfProdNo);

  /**
   * Select by job no.
   * 
   * @param jobNo the job no
   * @return the list
   */
  List<PfProImpDetail> selectByJobNo(@Param("jobNo") Long jobNo);

  /**
   * Insert.
   * 
   * @param record the record
   * @return the int
   */
  int insert(PfProImpDetail record);

  /**
   * Insert selective.
   * 
   * @param record the record
   * @return the int
   */
  int insertSelective(PfProImpDetail record);
  
  /**
   * Update by primary key.
   * 
   * @param record the record
   * @return the int
   */
  int updateByPrimaryKey(PfProImpDetail record);
  
  /**
   * Update job status by primary key.
   * 
   * @param record the record
   * @return the int
   */
  int updateJobStatusByPrimaryKey(PfProImpDetail record);
}