/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.core.pojo.frnplatform
 * @FileName: PfProAllot.java
 * @author:   Ian Chen
 * @date:     2014/8/28, 下午 02:26:16
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.core.pojo.frnplatform;

import java.util.Date;

/**
 * <pre> PfProAllot </pre>
 * 
 * @author Ian Chen
 */
public class PfProAllot {
  
  /** The table prefix. */
  private String tablePrefix;

  /** The pf prod no. */
  private String pfProdNo;

  /** The vend no. */
  private String vendNo;

  /** The sale dt. */
  private String saleDt;

  /** The tot qty. */
  private Integer totQty;

  /** The allot qty. */
  private Integer allotQty;

  /** The ord qty. */
  private Integer ordQty;

  /** The remark. */
  private String remark;

  /** The use yn. */
  private String useYn;

  /** The creator. */
  private String creator;

  /** The create dt. */
  private Date createDt;

  /** The moder. */
  private String moder;

  /** The mod dt. */
  private Date modDt;

  /**
   * Gets the table prefix.
   * 
   * @return the table prefix
   */
  public String getTablePrefix() {
    return tablePrefix;
  }

  /**
   * Sets the table prefix.
   * 
   * @param tablePrefix the new table prefix
   */
  public void setTablePrefix(String tablePrefix) {
    this.tablePrefix = tablePrefix;
  }

  /**
   * Gets the pf prod no.
   * 
   * @return the pf prod no
   */
  public String getPfProdNo() {
    return pfProdNo;
  }

  /**
   * Sets the pf prod no.
   * 
   * @param pfProdNo the new pf prod no
   */
  public void setPfProdNo(String pfProdNo) {
    this.pfProdNo = pfProdNo == null ? null : pfProdNo.trim();
  }

  /**
   * Gets the vend no.
   * 
   * @return the vend no
   */
  public String getVendNo() {
    return vendNo;
  }

  /**
   * Sets the vend no.
   * 
   * @param vendNo the new vend no
   */
  public void setVendNo(String vendNo) {
    this.vendNo = vendNo == null ? null : vendNo.trim();
  }

  /**
   * Gets the sale dt.
   * 
   * @return the sale dt
   */
  public String getSaleDt() {
    return saleDt;
  }

  /**
   * Sets the sale dt.
   * 
   * @param saleDt the new sale dt
   */
  public void setSaleDt(String saleDt) {
    this.saleDt = saleDt == null ? null : saleDt.trim();
  }

  /**
   * Gets the tot qty.
   * 
   * @return the tot qty
   */
  public Integer getTotQty() {
    return totQty;
  }

  /**
   * Sets the tot qty.
   * 
   * @param totQty the new tot qty
   */
  public void setTotQty(Integer totQty) {
    this.totQty = totQty;
  }

  /**
   * Gets the allot qty.
   * 
   * @return the allot qty
   */
  public Integer getAllotQty() {
    return allotQty;
  }

  /**
   * Sets the allot qty.
   * 
   * @param allotQty the new allot qty
   */
  public void setAllotQty(Integer allotQty) {
    this.allotQty = allotQty;
  }

  /**
   * Gets the ord qty.
   * 
   * @return the ord qty
   */
  public Integer getOrdQty() {
    return ordQty;
  }

  /**
   * Sets the ord qty.
   * 
   * @param ordQty the new ord qty
   */
  public void setOrdQty(Integer ordQty) {
    this.ordQty = ordQty;
  }

  /**
   * Gets the remark.
   * 
   * @return the remark
   */
  public String getRemark() {
    return remark;
  }

  /**
   * Sets the remark.
   * 
   * @param remark the new remark
   */
  public void setRemark(String remark) {
    this.remark = remark == null ? null : remark.trim();
  }

  /**
   * Gets the use yn.
   * 
   * @return the use yn
   */
  public String getUseYn() {
    return useYn;
  }

  /**
   * Sets the use yn.
   * 
   * @param useYn the new use yn
   */
  public void setUseYn(String useYn) {
    this.useYn = useYn == null ? null : useYn.trim();
  }

  /**
   * Gets the creator.
   * 
   * @return the creator
   */
  public String getCreator() {
    return creator;
  }

  /**
   * Sets the creator.
   * 
   * @param creator the new creator
   */
  public void setCreator(String creator) {
    this.creator = creator == null ? null : creator.trim();
  }

  /**
   * Gets the creates the dt.
   * 
   * @return the creates the dt
   */
  public Date getCreateDt() {
    return createDt;
  }

  /**
   * Sets the creates the dt.
   * 
   * @param createDt the new creates the dt
   */
  public void setCreateDt(Date createDt) {
    this.createDt = createDt;
  }

  /**
   * Gets the moder.
   * 
   * @return the moder
   */
  public String getModer() {
    return moder;
  }

  /**
   * Sets the moder.
   * 
   * @param moder the new moder
   */
  public void setModer(String moder) {
    this.moder = moder == null ? null : moder.trim();
  }

  /**
   * Gets the mod dt.
   * 
   * @return the mod dt
   */
  public Date getModDt() {
    return modDt;
  }

  /**
   * Sets the mod dt.
   * 
   * @param modDt the new mod dt
   */
  public void setModDt(Date modDt) {
    this.modDt = modDt;
  }
}