/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.core.pojo.frnplatform
 * @FileName: PfProMappingDetail.java
 * @author:   Ian Chen
 * @date:     2014/8/28, 下午 02:27:48
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.core.pojo.frnplatform;

import java.util.Date;

/**
 * <pre> PfProMappingDetail </pre>
 *
 * @author Ian Chen
 */
public class PfProMappingDetail {
  
  /** The prod no. */
  private String prodNo;

  /** The vend no. */
  private String vendNo;

  /** The pf g prod no. */
  private String pfGProdNo;

  /** The pf prod no. */
  private String pfProdNo;

  /** The su status. */
  private String suStatus;

  /** The creator. */
  private String creator;

  /** The create dt. */
  private Date createDt;

  /** The moder. */
  private String moder;

  /** The mod dt. */
  private Date modDt;

  /** The country cd. */
  private String countryCd;

  /** The line cd. */
  private Short lineCd;

  /** The tour citys. */
  private String tourCitys;

  /**
   * Gets the prod no.
   * 
   * @return the prod no
   */
  public String getProdNo() {
    return prodNo;
  }

  /**
   * Sets the prod no.
   * 
   * @param prodNo the new prod no
   */
  public void setProdNo(String prodNo) {
    this.prodNo = prodNo == null ? null : prodNo.trim();
  }

  /**
   * Gets the vend no.
   * 
   * @return the vend no
   */
  public String getVendNo() {
    return vendNo;
  }

  /**
   * Sets the vend no.
   * 
   * @param vendNo the new vend no
   */
  public void setVendNo(String vendNo) {
    this.vendNo = vendNo == null ? null : vendNo.trim();
  }

  /**
   * Gets the pf g prod no.
   * 
   * @return the pf g prod no
   */
  public String getPfGProdNo() {
    return pfGProdNo;
  }

  /**
   * Sets the pf g prod no.
   * 
   * @param pfGProdNo the new pf g prod no
   */
  public void setPfGProdNo(String pfGProdNo) {
    this.pfGProdNo = pfGProdNo == null ? null : pfGProdNo.trim();
  }

  /**
   * Gets the pf prod no.
   * 
   * @return the pf prod no
   */
  public String getPfProdNo() {
    return pfProdNo;
  }

  /**
   * Sets the pf prod no.
   * 
   * @param pfProdNo the new pf prod no
   */
  public void setPfProdNo(String pfProdNo) {
    this.pfProdNo = pfProdNo == null ? null : pfProdNo.trim();
  }

  /**
   * Gets the su status.
   * 
   * @return the su status
   */
  public String getSuStatus() {
    return suStatus;
  }

  /**
   * Sets the su status.
   * 
   * @param suStatus the new su status
   */
  public void setSuStatus(String suStatus) {
    this.suStatus = suStatus == null ? null : suStatus.trim();
  }

  /**
   * Gets the creator.
   * 
   * @return the creator
   */
  public String getCreator() {
    return creator;
  }

  /**
   * Sets the creator.
   * 
   * @param creator the new creator
   */
  public void setCreator(String creator) {
    this.creator = creator == null ? null : creator.trim();
  }

  /**
   * Gets the creates the dt.
   * 
   * @return the creates the dt
   */
  public Date getCreateDt() {
    return createDt;
  }

  /**
   * Sets the creates the dt.
   * 
   * @param createDt the new creates the dt
   */
  public void setCreateDt(Date createDt) {
    this.createDt = createDt;
  }

  /**
   * Gets the moder.
   * 
   * @return the moder
   */
  public String getModer() {
    return moder;
  }

  /**
   * Sets the moder.
   * 
   * @param moder the new moder
   */
  public void setModer(String moder) {
    this.moder = moder == null ? null : moder.trim();
  }

  /**
   * Gets the mod dt.
   * 
   * @return the mod dt
   */
  public Date getModDt() {
    return modDt;
  }

  /**
   * Sets the mod dt.
   * 
   * @param modDt the new mod dt
   */
  public void setModDt(Date modDt) {
    this.modDt = modDt;
  }

  /**
   * Gets the country cd.
   * 
   * @return the country cd
   */
  public String getCountryCd() {
    return countryCd;
  }

  /**
   * Sets the country cd.
   * 
   * @param countryCd the new country cd
   */
  public void setCountryCd(String countryCd) {
    this.countryCd = countryCd == null ? null : countryCd.trim();
  }

  /**
   * Gets the line cd.
   * 
   * @return the line cd
   */
  public Short getLineCd() {
    return lineCd;
  }

  /**
   * Sets the line cd.
   * 
   * @param lineCd the new line cd
   */
  public void setLineCd(Short lineCd) {
    this.lineCd = lineCd;
  }

  /**
   * Gets the tour citys.
   * 
   * @return the tour citys
   */
  public String getTourCitys() {
    return tourCitys;
  }

  /**
   * Sets the tour citys.
   * 
   * @param tourCitys the new tour citys
   */
  public void setTourCitys(String tourCitys) {
    this.tourCitys = tourCitys == null ? null : tourCitys.trim();
  }
}