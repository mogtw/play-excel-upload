/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.core.pojo.frnplatform
 * @FileName: PfProImpMain.java
 * @author:   Ian Chen
 * @date:     2014/9/9, 上午 09:50:19
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.core.pojo.frnplatform;

import java.util.Date;

/**
 * <pre> PfProImpMain </pre>
 * 
 * @author Ian Chen
 */
public class PfProImpMain {
  
  /** The job no. */
  private Long jobNo;

  /** The vend no. */
  private String vendNo;

  /** The job status. */
  private String jobStatus;

  /** The process dt. */
  private Date processDt;

  /** The total num. */
  private Integer totalNum;

  /** The creator. */
  private String creator;

  /** The create dt. */
  private Date createDt;

  /** The moder. */
  private String moder;

  /** The mod dt. */
  private Date modDt;

  /** The b group no. */
  private Integer bGroupNo;

  /** The b group count. */
  private Short bGroupCount;

  /** The count ok. */
  private Integer countOk;

  /** The count fail. */
  private Integer countFail;

  /**
   * Gets the job no.
   * 
   * @return the job no
   */
  public Long getJobNo() {
    return jobNo;
  }

  /**
   * Sets the job no.
   * 
   * @param jobNo the new job no
   */
  public void setJobNo(Long jobNo) {
    this.jobNo = jobNo;
  }

  /**
   * Gets the vend no.
   * 
   * @return the vend no
   */
  public String getVendNo() {
    return vendNo;
  }

  /**
   * Sets the vend no.
   * 
   * @param vendNo the new vend no
   */
  public void setVendNo(String vendNo) {
    this.vendNo = vendNo == null ? null : vendNo.trim();
  }

  /**
   * Gets the job status.
   * 
   * @return the job status
   */
  public String getJobStatus() {
    return jobStatus;
  }

  /**
   * Sets the job status.
   * 
   * @param jobStatus the new job status
   */
  public void setJobStatus(String jobStatus) {
    this.jobStatus = jobStatus == null ? null : jobStatus.trim();
  }

  /**
   * Gets the process dt.
   * 
   * @return the process dt
   */
  public Date getProcessDt() {
    return processDt;
  }

  /**
   * Sets the process dt.
   * 
   * @param processDt the new process dt
   */
  public void setProcessDt(Date processDt) {
    this.processDt = processDt;
  }

  /**
   * Gets the total num.
   * 
   * @return the total num
   */
  public Integer getTotalNum() {
    return totalNum;
  }

  /**
   * Sets the total num.
   * 
   * @param totalNum the new total num
   */
  public void setTotalNum(Integer totalNum) {
    this.totalNum = totalNum;
  }

  /**
   * Gets the creator.
   * 
   * @return the creator
   */
  public String getCreator() {
    return creator;
  }

  /**
   * Sets the creator.
   * 
   * @param creator the new creator
   */
  public void setCreator(String creator) {
    this.creator = creator == null ? null : creator.trim();
  }

  /**
   * Gets the creates the dt.
   * 
   * @return the creates the dt
   */
  public Date getCreateDt() {
    return createDt;
  }

  /**
   * Sets the creates the dt.
   * 
   * @param createDt the new creates the dt
   */
  public void setCreateDt(Date createDt) {
    this.createDt = createDt;
  }

  /**
   * Gets the moder.
   * 
   * @return the moder
   */
  public String getModer() {
    return moder;
  }

  /**
   * Sets the moder.
   * 
   * @param moder the new moder
   */
  public void setModer(String moder) {
    this.moder = moder == null ? null : moder.trim();
  }

  /**
   * Gets the mod dt.
   * 
   * @return the mod dt
   */
  public Date getModDt() {
    return modDt;
  }

  /**
   * Sets the mod dt.
   * 
   * @param modDt the new mod dt
   */
  public void setModDt(Date modDt) {
    this.modDt = modDt;
  }

  /**
   * Gets the b group no.
   * 
   * @return the b group no
   */
  public Integer getbGroupNo() {
    return bGroupNo;
  }

  /**
   * Sets the b group no.
   * 
   * @param bGroupNo the new b group no
   */
  public void setbGroupNo(Integer bGroupNo) {
    this.bGroupNo = bGroupNo;
  }

  /**
   * Gets the b group count.
   * 
   * @return the b group count
   */
  public Short getbGroupCount() {
    return bGroupCount;
  }

  /**
   * Sets the b group count.
   * 
   * @param bGroupCount the new b group count
   */
  public void setbGroupCount(Short bGroupCount) {
    this.bGroupCount = bGroupCount;
  }

  /**
   * Gets the count ok.
   * 
   * @return the count ok
   */
  public Integer getCountOk() {
    return countOk;
  }

  /**
   * Sets the count ok.
   * 
   * @param countOk the new count ok
   */
  public void setCountOk(Integer countOk) {
    this.countOk = countOk;
  }

  /**
   * Gets the count fail.
   * 
   * @return the count fail
   */
  public Integer getCountFail() {
    return countFail;
  }

  /**
   * Sets the count fail.
   * 
   * @param countFail the new count fail
   */
  public void setCountFail(Integer countFail) {
    this.countFail = countFail;
  }
}