/**
 * EZTRAVEL CONFIDENTIAL
 * 
 * @Package: eztravel.core.pojo.frnplatform
 * @FileName: CodeMap.java
 * @author: CJWang
 * @date: 2014/9/22, 下午 03:07:48
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.core.pojo.frnplatform;

import java.io.Serializable;
import java.util.Date;

/**
 * 全世界區域對照檔.
 * 
 * <pre>
 * 
 * </pre>
 */
public class CodeMap implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -7693645620148968715L;

  /** The wd code id. */
  private int wdCodeId;

  /** 線別. */
  private String lineId;

  /** 線別中文. */
  private String lineNm;

  /** 國家代碼. */
  private String countryCd;

  /** 國家代碼中文. */
  private String countryNm;

  /** 旅遊城市代碼. */
  private String tourCity;

  /** 旅遊城市中文. */
  private String tourCityNm;

  /** 區域代碼. */
  private String regionCd;

  /** 區域代碼中文. */
  private String regionNm;
  /** 建立者. */
  private String creator;

  /** The create dt. */
  private Date createDt;

  /** The moder. */
  private String moder;

  /** The mod dt. */
  private Date modDt;

  /**
   * 建立者.
   * 
   * @return 建立者
   */
  public String getCreator() {
    return creator;
  }

  /**
   * 建立者.
   * 
   * @param creator 建立者
   */
  public void setCreator(String creator) {
    this.creator = creator == null ? null : creator.trim();
  }

  /**
   * Gets the creates the dt.
   * 
   * @return the creates the dt
   */
  public Date getCreateDt() {
    return createDt;
  }

  /**
   * Sets the creates the dt.
   * 
   * @param createDt the new creates the dt
   */
  public void setCreateDt(Date createDt) {
    this.createDt = createDt;
  }

  /**
   * Gets the moder.
   * 
   * @return the moder
   */
  public String getModer() {
    return moder;
  }

  /**
   * Sets the moder.
   * 
   * @param moder the new moder
   */
  public void setModer(String moder) {
    this.moder = moder == null ? null : moder.trim();
  }

  /**
   * Gets the mod dt.
   * 
   * @return the mod dt
   */
  public Date getModDt() {
    return modDt;
  }

  /**
   * Sets the mod dt.
   * 
   * @param modDt the new mod dt
   */
  public void setModDt(Date modDt) {
    this.modDt = modDt;
  }

  /**
   * Gets the wd code id.
   * 
   * @return the wdCodeId
   */
  public int getWdCodeId() {
    return wdCodeId;
  }

  /**
   * Sets the wd code id.
   * 
   * @param wdCodeId the wdCodeId to set
   */
  public void setWdCodeId(int wdCodeId) {
    this.wdCodeId = wdCodeId;
  }

  /**
   * 線別.
   * 
   * @return 線別
   */
  public String getLineId() {
    return lineId;
  }

  /**
   * 線別.
   * 
   * @param lineId 線別
   */
  public void setLineId(String lineId) {
    this.lineId = lineId;
  }

  /**
   * 線別中文.
   * 
   * @return 線別中文
   */
  public String getLineNm() {
    return lineNm;
  }

  /**
   * 線別中文.
   * 
   * @param lineNm 線別中文
   */
  public void setLineNm(String lineNm) {
    this.lineNm = lineNm;
  }

  /**
   * 國家代碼.
   * 
   * @return 國家代碼
   */
  public String getCountryCd() {
    return countryCd;
  }

  /**
   * 國家代碼.
   * 
   * @param countryCd 國家代碼
   */
  public void setCountryCd(String countryCd) {
    this.countryCd = countryCd;
  }

  /**
   * 國家代碼中文.
   * 
   * @return 國家代碼中文
   */
  public String getCountryNm() {
    return countryNm;
  }

  /**
   * 國家代碼中文.
   * 
   * @param countryNm 國家代碼中文
   */
  public void setCountryNm(String countryNm) {
    this.countryNm = countryNm;
  }

  /**
   * 旅遊城市代碼.
   * 
   * @return 旅遊城市代碼
   */
  public String getTourCity() {
    return tourCity;
  }

  /**
   * 旅遊城市代碼.
   * 
   * @param tourCity 旅遊城市代碼
   */
  public void setTourCity(String tourCity) {
    this.tourCity = tourCity;
  }

  /**
   * 旅遊城市中文.
   * 
   * @return 旅遊城市中文
   */
  public String getTourCityNm() {
    return tourCityNm;
  }

  /**
   * 旅遊城市中文.
   * 
   * @param tourCityNm 旅遊城市中文
   */
  public void setTourCityNm(String tourCityNm) {
    this.tourCityNm = tourCityNm;
  }

  /**
   * 區域代碼.
   * 
   * @return 區域代碼
   */
  public String getRegionCd() {
    return regionCd;
  }

  /**
   * 區域代碼.
   * 
   * @param regionCd 區域代碼
   */
  public void setRegionCd(String regionCd) {
    this.regionCd = regionCd;
  }

  /**
   * 區域代碼中文.
   * 
   * @return 區域代碼中文
   */
  public String getRegionNm() {
    return regionNm;
  }

  /**
   * 區域代碼中文.
   * 
   * @param regionNm 區域代碼中文
   */
  public void setRegionNm(String regionNm) {
    this.regionNm = regionNm;
  }
}
