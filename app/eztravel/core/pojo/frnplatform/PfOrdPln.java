/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.core.pojo.frnplatform
 * @FileName: PfOrdPln.java
 * @author:   Ian Chen
 * @date:     2014/9/23, 上午 10:37:22
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.core.pojo.frnplatform;

import java.util.Date;

/**
 * <pre> PfOrdPln </pre>
 *
 * @author Ian Chen
 */
public class PfOrdPln {

  /** The order no. */
  private String orderNo;

  /** The cust seqno. */
  private Short custSeqno;

  /** The prod seqno. */
  private Short prodSeqno;

  /** The round cd. */
  private Short roundCd;

  /** The seq no. */
  private Short seqNo;

  /** The day seq. */
  private String daySeq;

  /** The airline no. */
  private String airlineNo;

  /** The schedule no. */
  private String scheduleNo;

  /** The depart airport. */
  private String departAirport;

  /** The arrive airport. */
  private String arriveAirport;

  /** The depart time. */
  private String departTime;

  /** The arrive time. */
  private String arriveTime;

  /** The order cabin. */
  private String orderCabin;

  /** The day diff. */
  private Short dayDiff;

  /** The creator. */
  private String creator;

  /** The create dt. */
  private Date createDt;

  /** The moder. */
  private String moder;

  /** The mod dt. */
  private Date modDt;

  /**
   * Gets the order no.
   * 
   * @return the order no
   */
  public String getOrderNo() {
    return orderNo;
  }

  /**
   * Sets the order no.
   * 
   * @param orderNo the new order no
   */
  public void setOrderNo(String orderNo) {
    this.orderNo = orderNo == null ? null : orderNo.trim();
  }

  /**
   * Gets the cust seqno.
   * 
   * @return the cust seqno
   */
  public Short getCustSeqno() {
    return custSeqno;
  }

  /**
   * Sets the cust seqno.
   * 
   * @param custSeqno the new cust seqno
   */
  public void setCustSeqno(Short custSeqno) {
    this.custSeqno = custSeqno;
  }

  /**
   * Gets the prod seqno.
   * 
   * @return the prod seqno
   */
  public Short getProdSeqno() {
    return prodSeqno;
  }

  /**
   * Sets the prod seqno.
   * 
   * @param prodSeqno the new prod seqno
   */
  public void setProdSeqno(Short prodSeqno) {
    this.prodSeqno = prodSeqno;
  }

  /**
   * Gets the round cd.
   * 
   * @return the round cd
   */
  public Short getRoundCd() {
    return roundCd;
  }

  /**
   * Sets the round cd.
   * 
   * @param roundCd the new round cd
   */
  public void setRoundCd(Short roundCd) {
    this.roundCd = roundCd;
  }

  /**
   * Gets the seq no.
   * 
   * @return the seq no
   */
  public Short getSeqNo() {
    return seqNo;
  }

  /**
   * Sets the seq no.
   * 
   * @param seqNo the new seq no
   */
  public void setSeqNo(Short seqNo) {
    this.seqNo = seqNo;
  }

  /**
   * Gets the day seq.
   * 
   * @return the day seq
   */
  public String getDaySeq() {
    return daySeq;
  }

  /**
   * Sets the day seq.
   * 
   * @param daySeq the new day seq
   */
  public void setDaySeq(String daySeq) {
    this.daySeq = daySeq == null ? null : daySeq.trim();
  }

  /**
   * Gets the airline no.
   * 
   * @return the airline no
   */
  public String getAirlineNo() {
    return airlineNo;
  }

  /**
   * Sets the airline no.
   * 
   * @param airlineNo the new airline no
   */
  public void setAirlineNo(String airlineNo) {
    this.airlineNo = airlineNo == null ? null : airlineNo.trim();
  }

  /**
   * Gets the schedule no.
   * 
   * @return the schedule no
   */
  public String getScheduleNo() {
    return scheduleNo;
  }

  /**
   * Sets the schedule no.
   * 
   * @param scheduleNo the new schedule no
   */
  public void setScheduleNo(String scheduleNo) {
    this.scheduleNo = scheduleNo == null ? null : scheduleNo.trim();
  }

  /**
   * Gets the depart airport.
   * 
   * @return the depart airport
   */
  public String getDepartAirport() {
    return departAirport;
  }

  /**
   * Sets the depart airport.
   * 
   * @param departAirport the new depart airport
   */
  public void setDepartAirport(String departAirport) {
    this.departAirport = departAirport == null ? null : departAirport.trim();
  }

  /**
   * Gets the arrive airport.
   * 
   * @return the arrive airport
   */
  public String getArriveAirport() {
    return arriveAirport;
  }

  /**
   * Sets the arrive airport.
   * 
   * @param arriveAirport the new arrive airport
   */
  public void setArriveAirport(String arriveAirport) {
    this.arriveAirport = arriveAirport == null ? null : arriveAirport.trim();
  }

  /**
   * Gets the depart time.
   * 
   * @return the depart time
   */
  public String getDepartTime() {
    return departTime;
  }

  /**
   * Sets the depart time.
   * 
   * @param departTime the new depart time
   */
  public void setDepartTime(String departTime) {
    this.departTime = departTime == null ? null : departTime.trim();
  }

  /**
   * Gets the arrive time.
   * 
   * @return the arrive time
   */
  public String getArriveTime() {
    return arriveTime;
  }

  /**
   * Sets the arrive time.
   * 
   * @param arriveTime the new arrive time
   */
  public void setArriveTime(String arriveTime) {
    this.arriveTime = arriveTime == null ? null : arriveTime.trim();
  }

  /**
   * Gets the order cabin.
   * 
   * @return the order cabin
   */
  public String getOrderCabin() {
    return orderCabin;
  }

  /**
   * Sets the order cabin.
   * 
   * @param orderCabin the new order cabin
   */
  public void setOrderCabin(String orderCabin) {
    this.orderCabin = orderCabin == null ? null : orderCabin.trim();
  }

  /**
   * Gets the day diff.
   * 
   * @return the day diff
   */
  public Short getDayDiff() {
    return dayDiff;
  }

  /**
   * Sets the day diff.
   * 
   * @param dayDiff the new day diff
   */
  public void setDayDiff(Short dayDiff) {
    this.dayDiff = dayDiff;
  }

  /**
   * Gets the creator.
   * 
   * @return the creator
   */
  public String getCreator() {
    return creator;
  }

  /**
   * Sets the creator.
   * 
   * @param creator the new creator
   */
  public void setCreator(String creator) {
    this.creator = creator == null ? null : creator.trim();
  }

  /**
   * Gets the creates the dt.
   * 
   * @return the creates the dt
   */
  public Date getCreateDt() {
    return createDt;
  }

  /**
   * Sets the creates the dt.
   * 
   * @param createDt the new creates the dt
   */
  public void setCreateDt(Date createDt) {
    this.createDt = createDt;
  }

  /**
   * Gets the moder.
   * 
   * @return the moder
   */
  public String getModer() {
    return moder;
  }

  /**
   * Sets the moder.
   * 
   * @param moder the new moder
   */
  public void setModer(String moder) {
    this.moder = moder == null ? null : moder.trim();
  }

  /**
   * Gets the mod dt.
   * 
   * @return the mod dt
   */
  public Date getModDt() {
    return modDt;
  }

  /**
   * Sets the mod dt.
   * 
   * @param modDt the new mod dt
   */
  public void setModDt(Date modDt) {
    this.modDt = modDt;
  }
}