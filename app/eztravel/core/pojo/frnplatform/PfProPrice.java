/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.core.pojo.frnplatform
 * @FileName: PfProPrice.java
 * @author:   Ian Chen
 * @date:     2014/8/28, 下午 02:28:19
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.core.pojo.frnplatform;

import java.math.BigDecimal;
import java.util.Date;

/**
 * <pre> PfProPrice </pre>
 * 
 * @author Ian Chen
 */
public class PfProPrice {
  
  /** The table prefix. */
  private String tablePrefix;
  
  /** The pf prod no. */
  private String pfProdNo;

  /** The vend no. */
  private String vendNo;

  /** The sale dt. */
  private String saleDt;

  /** The htl num. */
  private String htlNum;

  /** The cond2 type. */
  private String cond2Type;

  /** The cond3 type. */
  private String cond3Type;

  /** The min price1. */
  private BigDecimal minPrice1;

  /** The agent cost. */
  private BigDecimal agentCost;

  /** The creator. */
  private String creator;

  /** The create dt. */
  private Date createDt;

  /** The moder. */
  private String moder;

  /** The mod dt. */
  private Date modDt;

  /** The ez price1. */
  private BigDecimal ezPrice1;

  /**
   * Gets the table prefix.
   * 
   * @return the table prefix
   */
  public String getTablePrefix() {
    return tablePrefix;
  }

  /**
   * Sets the table prefix.
   * 
   * @param tablePrefix the new table prefix
   */
  public void setTablePrefix(String tablePrefix) {
    this.tablePrefix = tablePrefix;
  }

  /**
   * Gets the pf prod no.
   * 
   * @return the pf prod no
   */
  public String getPfProdNo() {
    return pfProdNo;
  }

  /**
   * Sets the pf prod no.
   * 
   * @param pfProdNo the new pf prod no
   */
  public void setPfProdNo(String pfProdNo) {
    this.pfProdNo = pfProdNo == null ? null : pfProdNo.trim();
  }

  /**
   * Gets the vend no.
   * 
   * @return the vend no
   */
  public String getVendNo() {
    return vendNo;
  }

  /**
   * Sets the vend no.
   * 
   * @param vendNo the new vend no
   */
  public void setVendNo(String vendNo) {
    this.vendNo = vendNo == null ? null : vendNo.trim();
  }

  /**
   * Gets the sale dt.
   * 
   * @return the sale dt
   */
  public String getSaleDt() {
    return saleDt;
  }

  /**
   * Sets the sale dt.
   * 
   * @param saleDt the new sale dt
   */
  public void setSaleDt(String saleDt) {
    this.saleDt = saleDt == null ? null : saleDt.trim();
  }

  /**
   * Gets the htl num.
   * 
   * @return the htl num
   */
  public String getHtlNum() {
    return htlNum;
  }

  /**
   * Sets the htl num.
   * 
   * @param htlNum the new htl num
   */
  public void setHtlNum(String htlNum) {
    this.htlNum = htlNum == null ? null : htlNum.trim();
  }

  /**
   * Gets the cond2 type.
   * 
   * @return the cond2 type
   */
  public String getCond2Type() {
    return cond2Type;
  }

  /**
   * Sets the cond2 type.
   * 
   * @param cond2Type the new cond2 type
   */
  public void setCond2Type(String cond2Type) {
    this.cond2Type = cond2Type == null ? null : cond2Type.trim();
  }

  /**
   * Gets the cond3 type.
   * 
   * @return the cond3 type
   */
  public String getCond3Type() {
    return cond3Type;
  }

  /**
   * Sets the cond3 type.
   * 
   * @param cond3Type the new cond3 type
   */
  public void setCond3Type(String cond3Type) {
    this.cond3Type = cond3Type == null ? null : cond3Type.trim();
  }

  /**
   * Gets the min price1.
   * 
   * @return the min price1
   */
  public BigDecimal getMinPrice1() {
    return minPrice1;
  }

  /**
   * Sets the min price1.
   * 
   * @param minPrice1 the new min price1
   */
  public void setMinPrice1(BigDecimal minPrice1) {
    this.minPrice1 = minPrice1;
  }

  /**
   * Gets the agent cost.
   * 
   * @return the agent cost
   */
  public BigDecimal getAgentCost() {
    return agentCost;
  }

  /**
   * Sets the agent cost.
   * 
   * @param agentCost the new agent cost
   */
  public void setAgentCost(BigDecimal agentCost) {
    this.agentCost = agentCost;
  }

  /**
   * Gets the creator.
   * 
   * @return the creator
   */
  public String getCreator() {
    return creator;
  }

  /**
   * Sets the creator.
   * 
   * @param creator the new creator
   */
  public void setCreator(String creator) {
    this.creator = creator == null ? null : creator.trim();
  }

  /**
   * Gets the creates the dt.
   * 
   * @return the creates the dt
   */
  public Date getCreateDt() {
    return createDt;
  }

  /**
   * Sets the creates the dt.
   * 
   * @param createDt the new creates the dt
   */
  public void setCreateDt(Date createDt) {
    this.createDt = createDt;
  }

  /**
   * Gets the moder.
   * 
   * @return the moder
   */
  public String getModer() {
    return moder;
  }

  /**
   * Sets the moder.
   * 
   * @param moder the new moder
   */
  public void setModer(String moder) {
    this.moder = moder == null ? null : moder.trim();
  }

  /**
   * Gets the mod dt.
   * 
   * @return the mod dt
   */
  public Date getModDt() {
    return modDt;
  }

  /**
   * Sets the mod dt.
   * 
   * @param modDt the new mod dt
   */
  public void setModDt(Date modDt) {
    this.modDt = modDt;
  }

  /**
   * Gets the ez price1.
   * 
   * @return the ez price1
   */
  public BigDecimal getEzPrice1() {
    return ezPrice1;
  }

  /**
   * Sets the ez price1.
   * 
   * @param ezPrice1 the new ez price1
   */
  public void setEzPrice1(BigDecimal ezPrice1) {
    this.ezPrice1 = ezPrice1;
  }
}