/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.core.pojo.frnplatform
 * @FileName: VenderUpdStatusCount.java
 * @author:   Ian Chen
 * @date:     2014/9/5, 下午5:41:57
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.core.pojo.frnplatform;

/**
 * <pre> VenderUpdStatusCount, 目前供應商的各更新狀態筆數 </pre>
 * 
 * @author Ian Chen
 */
public class VenderUpdStatusCount {
  
  /** The count0(處理完成). */
  private Long count0;
  
  /** The count1(等待JOB處理). */
  private Long count1;
  
  /** The count2(處理中).  */
  private Long count2;
  
  /**
   * Gets the count0.
   * 
   * @return the count0
   */
  public Long getCount0() {
    return count0;
  }
  
  /**
   * Sets the count0.
   * 
   * @param count0 the new count0
   */
  public void setCount0(Long count0) {
    this.count0 = count0;
  }
  
  /**
   * Gets the count1.
   * 
   * @return the count1
   */
  public Long getCount1() {
    return count1;
  }
  
  /**
   * Sets the count1.
   * 
   * @param count1 the new count1
   */
  public void setCount1(Long count1) {
    this.count1 = count1;
  }
  
  /**
   * Gets the count2.
   * 
   * @return the count2
   */
  public Long getCount2() {
    return count2;
  }
  
  /**
   * Sets the count2.
   * 
   * @param count2 the new count2
   */
  public void setCount2(Long count2) {
    this.count2 = count2;
  }
}
