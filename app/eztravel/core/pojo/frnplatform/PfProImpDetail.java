/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.core.pojo.frnplatform
 * @FileName: PfProImpDetail.java
 * @author:   Ian Chen
 * @date:     2014/8/28, 下午 02:27:18
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.core.pojo.frnplatform;

import java.util.Date;

/**
 * <pre> PfProImpDetail </pre>
 *
 * @author Ian Chen
 */
public class PfProImpDetail {
  
  /** The job no. */
  private Long jobNo;

  /** The pf prod no. */
  private String pfProdNo;

  /** The vend no. */
  private String vendNo;

  /** The prod content. */
  private String prodContent;

  /** The job status. */
  private String jobStatus;

  /** The response msg. */
  private String responseMsg;

  /** The exception. */
  private String exception;

  /** The return code. */
  private String returnCode;

  /** The creator. */
  private String creator;

  /** The create dt. */
  private Date createDt;

  /** The moder. */
  private String moder;

  /** The mod dt. */
  private Date modDt;

  /**
   * Gets the job no.
   * 
   * @return the job no
   */
  public Long getJobNo() {
    return jobNo;
  }

  /**
   * Sets the job no.
   * 
   * @param jobNo the new job no
   */
  public void setJobNo(Long jobNo) {
    this.jobNo = jobNo;
  }

  /**
   * Gets the pf prod no.
   * 
   * @return the pf prod no
   */
  public String getPfProdNo() {
    return pfProdNo;
  }

  /**
   * Sets the pf prod no.
   * 
   * @param pfProdNo the new pf prod no
   */
  public void setPfProdNo(String pfProdNo) {
    this.pfProdNo = pfProdNo == null ? null : pfProdNo.trim();
  }

  /**
   * Gets the vend no.
   * 
   * @return the vend no
   */
  public String getVendNo() {
    return vendNo;
  }

  /**
   * Sets the vend no.
   * 
   * @param vendNo the new vend no
   */
  public void setVendNo(String vendNo) {
    this.vendNo = vendNo == null ? null : vendNo.trim();
  }

  /**
   * Gets the prod content.
   * 
   * @return the prod content
   */
  public String getProdContent() {
    return prodContent;
  }

  /**
   * Sets the prod content.
   * 
   * @param prodContent the new prod content
   */
  public void setProdContent(String prodContent) {
    this.prodContent = prodContent == null ? null : prodContent.trim();
  }

  /**
   * Gets the job status.
   * 
   * @return the job status
   */
  public String getJobStatus() {
    return jobStatus;
  }

  /**
   * Sets the job status.
   * 
   * @param jobStatus the new job status
   */
  public void setJobStatus(String jobStatus) {
    this.jobStatus = jobStatus == null ? null : jobStatus.trim();
  }

  /**
   * Gets the response msg.
   * 
   * @return the response msg
   */
  public String getResponseMsg() {
    return responseMsg;
  }

  /**
   * Sets the response msg.
   * 
   * @param responseMsg the new response msg
   */
  public void setResponseMsg(String responseMsg) {
    this.responseMsg = responseMsg == null ? null : responseMsg.trim();
  }

  /**
   * Gets the exception.
   * 
   * @return the exception
   */
  public String getException() {
    return exception;
  }

  /**
   * Sets the exception.
   * 
   * @param exception the new exception
   */
  public void setException(String exception) {
    this.exception = exception == null ? null : exception.trim();
  }

  /**
   * Gets the return code.
   * 
   * @return the return code
   */
  public String getReturnCode() {
    return returnCode;
  }

  /**
   * Sets the return code.
   * 
   * @param returnCode the new return code
   */
  public void setReturnCode(String returnCode) {
    this.returnCode = returnCode == null ? null : returnCode.trim();
  }

  /**
   * Gets the creator.
   * 
   * @return the creator
   */
  public String getCreator() {
    return creator;
  }

  /**
   * Sets the creator.
   * 
   * @param creator the new creator
   */
  public void setCreator(String creator) {
    this.creator = creator == null ? null : creator.trim();
  }

  /**
   * Gets the creates the dt.
   * 
   * @return the creates the dt
   */
  public Date getCreateDt() {
    return createDt;
  }

  /**
   * Sets the creates the dt.
   * 
   * @param createDt the new creates the dt
   */
  public void setCreateDt(Date createDt) {
    this.createDt = createDt;
  }

  /**
   * Gets the moder.
   * 
   * @return the moder
   */
  public String getModer() {
    return moder;
  }

  /**
   * Sets the moder.
   * 
   * @param moder the new moder
   */
  public void setModer(String moder) {
    this.moder = moder == null ? null : moder.trim();
  }

  /**
   * Gets the mod dt.
   * 
   * @return the mod dt
   */
  public Date getModDt() {
    return modDt;
  }

  /**
   * Sets the mod dt.
   * 
   * @param modDt the new mod dt
   */
  public void setModDt(Date modDt) {
    this.modDt = modDt;
  }
}