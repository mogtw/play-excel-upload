/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.core.pojo.frnplatform
 * @FileName: PfFrn.java
 * @author:   Ian Chen
 * @date:     2014/9/23, 上午 11:04:17
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.core.pojo.frnplatform;

import java.util.Date;

/**
 * <pre> PfFrn </pre>
 * 
 * @author Ian Chen
 */
public class PfFrn {
  
  /** The pf prod no. */
  private String pfProdNo;

  /** The vend no. */
  private String vendNo;

  /** The from dt. */
  private String fromDt;

  /** The prod no. */
  private String prodNo;

  /** The pf g prod no. */
  private String pfGProdNo;

  /** The grp nm. */
  private String grpNm;

  /** The return dt. */
  private String returnDt;

  /** The people qty. */
  private Integer peopleQty;

  /** The creator. */
  private String creator;

  /** The create dt. */
  private Date createDt;

  /** The moder. */
  private String moder;

  /** The mod dt. */
  private Date modDt;

  /**
   * Gets the pf prod no.
   * 
   * @return the pf prod no
   */
  public String getPfProdNo() {
    return pfProdNo;
  }

  /**
   * Sets the pf prod no.
   * 
   * @param pfProdNo the new pf prod no
   */
  public void setPfProdNo(String pfProdNo) {
    this.pfProdNo = pfProdNo == null ? null : pfProdNo.trim();
  }

  /**
   * Gets the vend no.
   * 
   * @return the vend no
   */
  public String getVendNo() {
    return vendNo;
  }

  /**
   * Sets the vend no.
   * 
   * @param vendNo the new vend no
   */
  public void setVendNo(String vendNo) {
    this.vendNo = vendNo == null ? null : vendNo.trim();
  }

  /**
   * Gets the from dt.
   * 
   * @return the from dt
   */
  public String getFromDt() {
    return fromDt;
  }

  /**
   * Sets the from dt.
   * 
   * @param fromDt the new from dt
   */
  public void setFromDt(String fromDt) {
    this.fromDt = fromDt == null ? null : fromDt.trim();
  }

  /**
   * Gets the prod no.
   * 
   * @return the prod no
   */
  public String getProdNo() {
    return prodNo;
  }

  /**
   * Sets the prod no.
   * 
   * @param prodNo the new prod no
   */
  public void setProdNo(String prodNo) {
    this.prodNo = prodNo == null ? null : prodNo.trim();
  }

  /**
   * Gets the pf g prod no.
   * 
   * @return the pf g prod no
   */
  public String getPfGProdNo() {
    return pfGProdNo;
  }

  /**
   * Sets the pf g prod no.
   * 
   * @param pfGProdNo the new pf g prod no
   */
  public void setPfGProdNo(String pfGProdNo) {
    this.pfGProdNo = pfGProdNo == null ? null : pfGProdNo.trim();
  }

  /**
   * Gets the grp nm.
   * 
   * @return the grp nm
   */
  public String getGrpNm() {
    return grpNm;
  }

  /**
   * Sets the grp nm.
   * 
   * @param grpNm the new grp nm
   */
  public void setGrpNm(String grpNm) {
    this.grpNm = grpNm == null ? null : grpNm.trim();
  }

  /**
   * Gets the return dt.
   * 
   * @return the return dt
   */
  public String getReturnDt() {
    return returnDt;
  }

  /**
   * Sets the return dt.
   * 
   * @param returnDt the new return dt
   */
  public void setReturnDt(String returnDt) {
    this.returnDt = returnDt == null ? null : returnDt.trim();
  }

  /**
   * Gets the people qty.
   * 
   * @return the people qty
   */
  public Integer getPeopleQty() {
    return peopleQty;
  }

  /**
   * Sets the people qty.
   * 
   * @param peopleQty the new people qty
   */
  public void setPeopleQty(Integer peopleQty) {
    this.peopleQty = peopleQty;
  }

  /**
   * Gets the creator.
   * 
   * @return the creator
   */
  public String getCreator() {
    return creator;
  }

  /**
   * Sets the creator.
   * 
   * @param creator the new creator
   */
  public void setCreator(String creator) {
    this.creator = creator == null ? null : creator.trim();
  }

  /**
   * Gets the creates the dt.
   * 
   * @return the creates the dt
   */
  public Date getCreateDt() {
    return createDt;
  }

  /**
   * Sets the creates the dt.
   * 
   * @param createDt the new creates the dt
   */
  public void setCreateDt(Date createDt) {
    this.createDt = createDt;
  }

  /**
   * Gets the moder.
   * 
   * @return the moder
   */
  public String getModer() {
    return moder;
  }

  /**
   * Sets the moder.
   * 
   * @param moder the new moder
   */
  public void setModer(String moder) {
    this.moder = moder == null ? null : moder.trim();
  }

  /**
   * Gets the mod dt.
   * 
   * @return the mod dt
   */
  public Date getModDt() {
    return modDt;
  }

  /**
   * Sets the mod dt.
   * 
   * @param modDt the new mod dt
   */
  public void setModDt(Date modDt) {
    this.modDt = modDt;
  }
}