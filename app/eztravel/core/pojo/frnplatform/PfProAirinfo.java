/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.core.pojo.frnplatform
 * @FileName: PfProAirinfo.java
 * @author:   Ian Chen
 * @date:     2014/8/28, 下午 02:26:00
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.core.pojo.frnplatform;

import java.util.Date;

/**
 * <pre> PfProAirinfo </pre>
 * 
 * @author Ian Chen
 */
public class PfProAirinfo {
  
  /** The table prefix. */
  private String tablePrefix;
  
  /** The pf prod no. */
  private String pfProdNo;

  /** The vend no. */
  private String vendNo;

  /** The seqno. */
  private Short seqno;

  /** The round cd. */
  private Short roundCd;

  /** The depart date. */
  private String departDate;

  /** The qty. */
  private Short qty;

  /** The aircompany. */
  private String aircompany;

  /** The aircompany code. */
  private String aircompanyCode;

  /** The schedule no. */
  private String scheduleNo;

  /** The depart airport code. */
  private String departAirportCode;

  /** The arrive airport code. */
  private String arriveAirportCode;
  
  /** The depart airport nm. */
  private String departAirportNm;

  /** The arrive airport nm. */
  private String arriveAirportNm;

  /** The depart time. */
  private String departTime;

  /** The arrive time. */
  private String arriveTime;

  /** The creator. */
  private String creator;

  /** The create dt. */
  private Date createDt;

  /** The moder. */
  private String moder;

  /** The mod dt. */
  private Date modDt;

  /**
   * Gets the table prefix.
   * 
   * @return the table prefix
   */
  public String getTablePrefix() {
    return tablePrefix;
  }

  /**
   * Sets the table prefix.
   * 
   * @param tablePrefix the new table prefix
   */
  public void setTablePrefix(String tablePrefix) {
    this.tablePrefix = tablePrefix;
  }

  /**
   * Gets the pf prod no.
   * 
   * @return the pf prod no
   */
  public String getPfProdNo() {
    return pfProdNo;
  }

  /**
   * Sets the pf prod no.
   * 
   * @param pfProdNo the new pf prod no
   */
  public void setPfProdNo(String pfProdNo) {
    this.pfProdNo = pfProdNo == null ? null : pfProdNo.trim();
  }

  /**
   * Gets the vend no.
   * 
   * @return the vend no
   */
  public String getVendNo() {
    return vendNo;
  }

  /**
   * Sets the vend no.
   * 
   * @param vendNo the new vend no
   */
  public void setVendNo(String vendNo) {
    this.vendNo = vendNo == null ? null : vendNo.trim();
  }

  /**
   * Gets the seqno.
   * 
   * @return the seqno
   */
  public Short getSeqno() {
    return seqno;
  }

  /**
   * Sets the seqno.
   * 
   * @param seqno the new seqno
   */
  public void setSeqno(Short seqno) {
    this.seqno = seqno;
  }

  /**
   * Gets the round cd.
   * 
   * @return the round cd
   */
  public Short getRoundCd() {
    return roundCd;
  }

  /**
   * Sets the round cd.
   * 
   * @param roundCd the new round cd
   */
  public void setRoundCd(Short roundCd) {
    this.roundCd = roundCd;
  }

  /**
   * Gets the depart date.
   * 
   * @return the depart date
   */
  public String getDepartDate() {
    return departDate;
  }

  /**
   * Sets the depart date.
   * 
   * @param departDate the new depart date
   */
  public void setDepartDate(String departDate) {
    this.departDate = departDate == null ? null : departDate.trim();
  }

  /**
   * Gets the qty.
   * 
   * @return the qty
   */
  public Short getQty() {
    return qty;
  }

  /**
   * Sets the qty.
   * 
   * @param qty the new qty
   */
  public void setQty(Short qty) {
    this.qty = qty;
  }

  /**
   * Gets the aircompany.
   * 
   * @return the aircompany
   */
  public String getAircompany() {
    return aircompany;
  }

  /**
   * Sets the aircompany.
   * 
   * @param aircompany the new aircompany
   */
  public void setAircompany(String aircompany) {
    this.aircompany = aircompany == null ? null : aircompany.trim();
  }

  /**
   * Gets the aircompany code.
   * 
   * @return the aircompany code
   */
  public String getAircompanyCode() {
    return aircompanyCode;
  }

  /**
   * Sets the aircompany code.
   * 
   * @param aircompanyCode the new aircompany code
   */
  public void setAircompanyCode(String aircompanyCode) {
    this.aircompanyCode = aircompanyCode == null ? null : aircompanyCode.trim();
  }

  /**
   * Gets the schedule no.
   * 
   * @return the schedule no
   */
  public String getScheduleNo() {
    return scheduleNo;
  }

  /**
   * Sets the schedule no.
   * 
   * @param scheduleNo the new schedule no
   */
  public void setScheduleNo(String scheduleNo) {
    this.scheduleNo = scheduleNo == null ? null : scheduleNo.trim();
  }

  /**
   * Gets the depart airport code.
   * 
   * @return the depart airport code
   */
  public String getDepartAirportCode() {
    return departAirportCode;
  }

  /**
   * Sets the depart airport code.
   * 
   * @param departAirportCode the new depart airport code
   */
  public void setDepartAirportCode(String departAirportCode) {
    this.departAirportCode = departAirportCode == null ? null : departAirportCode.trim();
  }

  /**
   * Gets the arrive airport code.
   * 
   * @return the arrive airport code
   */
  public String getArriveAirportCode() {
    return arriveAirportCode;
  }

  /**
   * Sets the arrive airport code.
   * 
   * @param arriveAirportCode the new arrive airport code
   */
  public void setArriveAirportCode(String arriveAirportCode) {
    this.arriveAirportCode = arriveAirportCode == null ? null : arriveAirportCode.trim();
  }

  /**
   * Gets the depart airport nm.
   * 
   * @return the depart airport nm
   */
  public String getDepartAirportNm() {
    return departAirportNm;
  }

  /**
   * Sets the depart airport nm.
   * 
   * @param departAirportNm the new depart airport nm
   */
  public void setDepartAirportNm(String departAirportNm) {
    this.departAirportNm = departAirportNm == null ? null : departAirportNm.trim();
  }

  /**
   * Gets the arrive airport nm.
   * 
   * @return the arrive airport nm
   */
  public String getArriveAirportNm() {
    return arriveAirportNm;
  }

  /**
   * Sets the arrive airport nm.
   * 
   * @param arriveAirportNm the new arrive airport nm
   */
  public void setArriveAirportNm(String arriveAirportNm) {
    this.arriveAirportNm = arriveAirportNm == null ? null : arriveAirportNm.trim();
  }

  /**
   * Gets the depart time.
   * 
   * @return the depart time
   */
  public String getDepartTime() {
    return departTime;
  }

  /**
   * Sets the depart time.
   * 
   * @param departTime the new depart time
   */
  public void setDepartTime(String departTime) {
    this.departTime = departTime == null ? null : departTime.trim();
  }

  /**
   * Gets the arrive time.
   * 
   * @return the arrive time
   */
  public String getArriveTime() {
    return arriveTime;
  }

  /**
   * Sets the arrive time.
   * 
   * @param arriveTime the new arrive time
   */
  public void setArriveTime(String arriveTime) {
    this.arriveTime = arriveTime == null ? null : arriveTime.trim();
  }

  /**
   * Gets the creator.
   * 
   * @return the creator
   */
  public String getCreator() {
    return creator;
  }

  /**
   * Sets the creator.
   * 
   * @param creator the new creator
   */
  public void setCreator(String creator) {
    this.creator = creator == null ? null : creator.trim();
  }

  /**
   * Gets the creates the dt.
   * 
   * @return the creates the dt
   */
  public Date getCreateDt() {
    return createDt;
  }

  /**
   * Sets the creates the dt.
   * 
   * @param createDt the new creates the dt
   */
  public void setCreateDt(Date createDt) {
    this.createDt = createDt;
  }

  /**
   * Gets the moder.
   * 
   * @return the moder
   */
  public String getModer() {
    return moder;
  }

  /**
   * Sets the moder.
   * 
   * @param moder the new moder
   */
  public void setModer(String moder) {
    this.moder = moder == null ? null : moder.trim();
  }

  /**
   * Gets the mod dt.
   * 
   * @return the mod dt
   */
  public Date getModDt() {
    return modDt;
  }

  /**
   * Sets the mod dt.
   * 
   * @param modDt the new mod dt
   */
  public void setModDt(Date modDt) {
    this.modDt = modDt;
  }
}