/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.core.pojo.frnplatform
 * @FileName: PfOrdMain.java
 * @author:   Ian Chen
 * @date:     2014/9/23, 上午 10:36:54
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.core.pojo.frnplatform;

import java.util.Date;

/**
 * <pre> PfOrdMain </pre>
 *
 * @author Ian Chen
 */
public class PfOrdMain {

  /** The order no. */
  private String orderNo;

  /** The sale man. */
  private String saleMan;

  /** The sale dept id. */
  private String saleDeptId;

  /** The order dt. */
  private String orderDt;

  /** The order type. */
  private String orderType;

  /** The order status. */
  private String orderStatus;

  /** The recv status. */
  private String recvStatus;

  /** The order amt. */
  private Integer orderAmt;

  /** The in need amt. */
  private Integer inNeedAmt;

  /** The order num. */
  private Integer orderNum;

  /** The ordder man. */
  private String ordderMan;

  /** The order man nm. */
  private String orderManNm;

  /** The creator. */
  private String creator;

  /** The create dt. */
  private Date createDt;

  /** The moder. */
  private String moder;

  /** The mod dt. */
  private Date modDt;

  /** The grp no. */
  private String grpNo;

  /** The pf g prod no. */
  private String pfGProdNo;

  /** The prod no. */
  private String prodNo;

  /**
   * Gets the order no.
   * 
   * @return the order no
   */
  public String getOrderNo() {
    return orderNo;
  }

  /**
   * Sets the order no.
   * 
   * @param orderNo the new order no
   */
  public void setOrderNo(String orderNo) {
    this.orderNo = orderNo == null ? null : orderNo.trim();
  }

  /**
   * Gets the sale man.
   * 
   * @return the sale man
   */
  public String getSaleMan() {
    return saleMan;
  }

  /**
   * Sets the sale man.
   * 
   * @param saleMan the new sale man
   */
  public void setSaleMan(String saleMan) {
    this.saleMan = saleMan == null ? null : saleMan.trim();
  }

  /**
   * Gets the sale dept id.
   * 
   * @return the sale dept id
   */
  public String getSaleDeptId() {
    return saleDeptId;
  }

  /**
   * Sets the sale dept id.
   * 
   * @param saleDeptId the new sale dept id
   */
  public void setSaleDeptId(String saleDeptId) {
    this.saleDeptId = saleDeptId == null ? null : saleDeptId.trim();
  }

  /**
   * Gets the order dt.
   * 
   * @return the order dt
   */
  public String getOrderDt() {
    return orderDt;
  }

  /**
   * Sets the order dt.
   * 
   * @param orderDt the new order dt
   */
  public void setOrderDt(String orderDt) {
    this.orderDt = orderDt == null ? null : orderDt.trim();
  }

  /**
   * Gets the order type.
   * 
   * @return the order type
   */
  public String getOrderType() {
    return orderType;
  }

  /**
   * Sets the order type.
   * 
   * @param orderType the new order type
   */
  public void setOrderType(String orderType) {
    this.orderType = orderType == null ? null : orderType.trim();
  }

  /**
   * Gets the order status.
   * 
   * @return the order status
   */
  public String getOrderStatus() {
    return orderStatus;
  }

  /**
   * Sets the order status.
   * 
   * @param orderStatus the new order status
   */
  public void setOrderStatus(String orderStatus) {
    this.orderStatus = orderStatus == null ? null : orderStatus.trim();
  }

  /**
   * Gets the recv status.
   * 
   * @return the recv status
   */
  public String getRecvStatus() {
    return recvStatus;
  }

  /**
   * Sets the recv status.
   * 
   * @param recvStatus the new recv status
   */
  public void setRecvStatus(String recvStatus) {
    this.recvStatus = recvStatus == null ? null : recvStatus.trim();
  }

  /**
   * Gets the order amt.
   * 
   * @return the order amt
   */
  public Integer getOrderAmt() {
    return orderAmt;
  }

  /**
   * Sets the order amt.
   * 
   * @param orderAmt the new order amt
   */
  public void setOrderAmt(Integer orderAmt) {
    this.orderAmt = orderAmt;
  }

  /**
   * Gets the in need amt.
   * 
   * @return the in need amt
   */
  public Integer getInNeedAmt() {
    return inNeedAmt;
  }

  /**
   * Sets the in need amt.
   * 
   * @param inNeedAmt the new in need amt
   */
  public void setInNeedAmt(Integer inNeedAmt) {
    this.inNeedAmt = inNeedAmt;
  }

  /**
   * Gets the order num.
   * 
   * @return the order num
   */
  public Integer getOrderNum() {
    return orderNum;
  }

  /**
   * Sets the order num.
   * 
   * @param orderNum the new order num
   */
  public void setOrderNum(Integer orderNum) {
    this.orderNum = orderNum;
  }

  /**
   * Gets the ordder man.
   * 
   * @return the ordder man
   */
  public String getOrdderMan() {
    return ordderMan;
  }

  /**
   * Sets the ordder man.
   * 
   * @param ordderMan the new ordder man
   */
  public void setOrdderMan(String ordderMan) {
    this.ordderMan = ordderMan == null ? null : ordderMan.trim();
  }

  /**
   * Gets the order man nm.
   * 
   * @return the order man nm
   */
  public String getOrderManNm() {
    return orderManNm;
  }

  /**
   * Sets the order man nm.
   * 
   * @param orderManNm the new order man nm
   */
  public void setOrderManNm(String orderManNm) {
    this.orderManNm = orderManNm == null ? null : orderManNm.trim();
  }

  /**
   * Gets the creator.
   * 
   * @return the creator
   */
  public String getCreator() {
    return creator;
  }

  /**
   * Sets the creator.
   * 
   * @param creator the new creator
   */
  public void setCreator(String creator) {
    this.creator = creator == null ? null : creator.trim();
  }

  /**
   * Gets the creates the dt.
   * 
   * @return the creates the dt
   */
  public Date getCreateDt() {
    return createDt;
  }

  /**
   * Sets the creates the dt.
   * 
   * @param createDt the new creates the dt
   */
  public void setCreateDt(Date createDt) {
    this.createDt = createDt;
  }

  /**
   * Gets the moder.
   * 
   * @return the moder
   */
  public String getModer() {
    return moder;
  }

  /**
   * Sets the moder.
   * 
   * @param moder the new moder
   */
  public void setModer(String moder) {
    this.moder = moder == null ? null : moder.trim();
  }

  /**
   * Gets the mod dt.
   * 
   * @return the mod dt
   */
  public Date getModDt() {
    return modDt;
  }

  /**
   * Sets the mod dt.
   * 
   * @param modDt the new mod dt
   */
  public void setModDt(Date modDt) {
    this.modDt = modDt;
  }

  /**
   * Gets the grp no.
   * 
   * @return the grp no
   */
  public String getGrpNo() {
    return grpNo;
  }

  /**
   * Sets the grp no.
   * 
   * @param grpNo the new grp no
   */
  public void setGrpNo(String grpNo) {
    this.grpNo = grpNo == null ? null : grpNo.trim();
  }

  /**
   * Gets the pf g prod no.
   * 
   * @return the pf g prod no
   */
  public String getPfGProdNo() {
    return pfGProdNo;
  }

  /**
   * Sets the pf g prod no.
   * 
   * @param pfGProdNo the new pf g prod no
   */
  public void setPfGProdNo(String pfGProdNo) {
    this.pfGProdNo = pfGProdNo == null ? null : pfGProdNo.trim();
  }

  /**
   * Gets the prod no.
   * 
   * @return the prod no
   */
  public String getProdNo() {
    return prodNo;
  }

  /**
   * Sets the prod no.
   * 
   * @param prodNo the new prod no
   */
  public void setProdNo(String prodNo) {
    this.prodNo = prodNo == null ? null : prodNo.trim();
  }
}