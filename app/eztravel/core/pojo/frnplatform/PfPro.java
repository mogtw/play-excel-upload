/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.core.pojo.frnplatform
 * @FileName: PfPro.java
 * @author:   Ian Chen
 * @date:     2014/8/28, 下午 02:25:30
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.core.pojo.frnplatform;

import java.math.BigDecimal;
import java.util.Date;

/**
 * <pre> PfPro </pre>
 *
 * @author Ian Chen
 */
public class PfPro {
  
  /** The table prefix. */
  private String tablePrefix;
  
  /** The pf prod no. */
  private String pfProdNo;

  /** The vend no. */
  private String vendNo;

  /** The country cd. */
  private String countryCd;

  /** The line cd. */
  private Short lineCd;

  /** The prod type. */
  private String prodType;

  /** The region cd. */
  private String regionCd;

  /** The pf g prod no. */
  private String pfGProdNo;

  /** The prod nm. */
  private String prodNm;

  /** The beg valid dt. */
  private String begValidDt;

  /** The end vallid dt. */
  private String endVallidDt;

  /** The beg sale dt. */
  private String begSaleDt;

  /** The end sale dt. */
  private String endSaleDt;

  /** The up dt. */
  private String upDt;

  /** The down dt. */
  private String downDt;

  /** The promotion msg. */
  private String promotionMsg;

  /** The remark. */
  private String remark;

  /** The promotion seq. */
  private Short promotionSeq;

  /** The promotion yn. */
  private String promotionYn;

  /** The sale type. */
  private Short saleType;

  /** The bonus prod yn. */
  private String bonusProdYn;

  /** The prod promotion. */
  private String prodPromotion;

  /** The price1. */
  private BigDecimal price1;

  /** The depart area. */
  private String departArea;

  /** The passport yn. */
  private String passportYn;

  /** The creator. */
  private String creator;

  /** The create dt. */
  private Date createDt;

  /** The moder. */
  private String moder;

  /** The mod dt. */
  private Date modDt;

  /** The b2e yn. */
  private String b2eYn;

  /** The b2b yn. */
  private String b2bYn;

  /** The deadline. */
  private String deadline;

  /** The up yn. */
  private String upYn;

  /**
   * Gets the table prefix.
   * 
   * @return the table prefix
   */
  public String getTablePrefix() {
    return tablePrefix;
  }

  /**
   * Sets the table prefix.
   * 
   * @param tablePrefix the new table prefix
   */
  public void setTablePrefix(String tablePrefix) {
    this.tablePrefix = tablePrefix;
  }

  /**
   * Gets the pf prod no.
   * 
   * @return the pf prod no
   */
  public String getPfProdNo() {
    return pfProdNo;
  }

  /**
   * Sets the pf prod no.
   * 
   * @param pfProdNo the new pf prod no
   */
  public void setPfProdNo(String pfProdNo) {
    this.pfProdNo = pfProdNo == null ? null : pfProdNo.trim();
  }

  /**
   * Gets the vend no.
   * 
   * @return the vend no
   */
  public String getVendNo() {
    return vendNo;
  }

  /**
   * Sets the vend no.
   * 
   * @param vendNo the new vend no
   */
  public void setVendNo(String vendNo) {
    this.vendNo = vendNo == null ? null : vendNo.trim();
  }

  /**
   * Gets the country cd.
   * 
   * @return the country cd
   */
  public String getCountryCd() {
    return countryCd;
  }

  /**
   * Sets the country cd.
   * 
   * @param countryCd the new country cd
   */
  public void setCountryCd(String countryCd) {
    this.countryCd = countryCd == null ? null : countryCd.trim();
  }

  /**
   * Gets the line cd.
   * 
   * @return the line cd
   */
  public Short getLineCd() {
    return lineCd;
  }

  /**
   * Sets the line cd.
   * 
   * @param lineCd the new line cd
   */
  public void setLineCd(Short lineCd) {
    this.lineCd = lineCd;
  }

  /**
   * Gets the prod type.
   * 
   * @return the prod type
   */
  public String getProdType() {
    return prodType;
  }

  /**
   * Sets the prod type.
   * 
   * @param prodType the new prod type
   */
  public void setProdType(String prodType) {
    this.prodType = prodType == null ? null : prodType.trim();
  }

  /**
   * Gets the region cd.
   * 
   * @return the region cd
   */
  public String getRegionCd() {
    return regionCd;
  }

  /**
   * Sets the region cd.
   * 
   * @param regionCd the new region cd
   */
  public void setRegionCd(String regionCd) {
    this.regionCd = regionCd == null ? null : regionCd.trim();
  }

  /**
   * Gets the pf g prod no.
   * 
   * @return the pf g prod no
   */
  public String getPfGProdNo() {
    return pfGProdNo;
  }

  /**
   * Sets the pf g prod no.
   * 
   * @param pfGProdNo the new pf g prod no
   */
  public void setPfGProdNo(String pfGProdNo) {
    this.pfGProdNo = pfGProdNo == null ? null : pfGProdNo.trim();
  }

  /**
   * Gets the prod nm.
   * 
   * @return the prod nm
   */
  public String getProdNm() {
    return prodNm;
  }

  /**
   * Sets the prod nm.
   * 
   * @param prodNm the new prod nm
   */
  public void setProdNm(String prodNm) {
    this.prodNm = prodNm == null ? null : prodNm.trim();
  }

  /**
   * Gets the beg valid dt.
   * 
   * @return the beg valid dt
   */
  public String getBegValidDt() {
    return begValidDt;
  }

  /**
   * Sets the beg valid dt.
   * 
   * @param begValidDt the new beg valid dt
   */
  public void setBegValidDt(String begValidDt) {
    this.begValidDt = begValidDt == null ? null : begValidDt.trim();
  }

  /**
   * Gets the end vallid dt.
   * 
   * @return the end vallid dt
   */
  public String getEndVallidDt() {
    return endVallidDt;
  }

  /**
   * Sets the end vallid dt.
   * 
   * @param endVallidDt the new end vallid dt
   */
  public void setEndVallidDt(String endVallidDt) {
    this.endVallidDt = endVallidDt == null ? null : endVallidDt.trim();
  }

  /**
   * Gets the beg sale dt.
   * 
   * @return the beg sale dt
   */
  public String getBegSaleDt() {
    return begSaleDt;
  }

  /**
   * Sets the beg sale dt.
   * 
   * @param begSaleDt the new beg sale dt
   */
  public void setBegSaleDt(String begSaleDt) {
    this.begSaleDt = begSaleDt == null ? null : begSaleDt.trim();
  }

  /**
   * Gets the end sale dt.
   * 
   * @return the end sale dt
   */
  public String getEndSaleDt() {
    return endSaleDt;
  }

  /**
   * Sets the end sale dt.
   * 
   * @param endSaleDt the new end sale dt
   */
  public void setEndSaleDt(String endSaleDt) {
    this.endSaleDt = endSaleDt == null ? null : endSaleDt.trim();
  }

  /**
   * Gets the up dt.
   * 
   * @return the up dt
   */
  public String getUpDt() {
    return upDt;
  }

  /**
   * Sets the up dt.
   * 
   * @param upDt the new up dt
   */
  public void setUpDt(String upDt) {
    this.upDt = upDt == null ? null : upDt.trim();
  }

  /**
   * Gets the down dt.
   * 
   * @return the down dt
   */
  public String getDownDt() {
    return downDt;
  }

  /**
   * Sets the down dt.
   * 
   * @param downDt the new down dt
   */
  public void setDownDt(String downDt) {
    this.downDt = downDt == null ? null : downDt.trim();
  }

  /**
   * Gets the promotion msg.
   * 
   * @return the promotion msg
   */
  public String getPromotionMsg() {
    return promotionMsg;
  }

  /**
   * Sets the promotion msg.
   * 
   * @param promotionMsg the new promotion msg
   */
  public void setPromotionMsg(String promotionMsg) {
    this.promotionMsg = promotionMsg == null ? null : promotionMsg.trim();
  }

  /**
   * Gets the remark.
   * 
   * @return the remark
   */
  public String getRemark() {
    return remark;
  }

  /**
   * Sets the remark.
   * 
   * @param remark the new remark
   */
  public void setRemark(String remark) {
    this.remark = remark == null ? null : remark.trim();
  }

  /**
   * Gets the promotion seq.
   * 
   * @return the promotion seq
   */
  public Short getPromotionSeq() {
    return promotionSeq;
  }

  /**
   * Sets the promotion seq.
   * 
   * @param promotionSeq the new promotion seq
   */
  public void setPromotionSeq(Short promotionSeq) {
    this.promotionSeq = promotionSeq;
  }

  /**
   * Gets the promotion yn.
   * 
   * @return the promotion yn
   */
  public String getPromotionYn() {
    return promotionYn;
  }

  /**
   * Sets the promotion yn.
   * 
   * @param promotionYn the new promotion yn
   */
  public void setPromotionYn(String promotionYn) {
    this.promotionYn = promotionYn == null ? null : promotionYn.trim();
  }

  /**
   * Gets the sale type.
   * 
   * @return the sale type
   */
  public Short getSaleType() {
    return saleType;
  }

  /**
   * Sets the sale type.
   * 
   * @param saleType the new sale type
   */
  public void setSaleType(Short saleType) {
    this.saleType = saleType;
  }

  /**
   * Gets the bonus prod yn.
   * 
   * @return the bonus prod yn
   */
  public String getBonusProdYn() {
    return bonusProdYn;
  }

  /**
   * Sets the bonus prod yn.
   * 
   * @param bonusProdYn the new bonus prod yn
   */
  public void setBonusProdYn(String bonusProdYn) {
    this.bonusProdYn = bonusProdYn == null ? null : bonusProdYn.trim();
  }

  /**
   * Gets the prod promotion.
   * 
   * @return the prod promotion
   */
  public String getProdPromotion() {
    return prodPromotion;
  }

  /**
   * Sets the prod promotion.
   * 
   * @param prodPromotion the new prod promotion
   */
  public void setProdPromotion(String prodPromotion) {
    this.prodPromotion = prodPromotion == null ? null : prodPromotion.trim();
  }

  /**
   * Gets the price1.
   * 
   * @return the price1
   */
  public BigDecimal getPrice1() {
    return price1;
  }

  /**
   * Sets the price1.
   * 
   * @param price1 the new price1
   */
  public void setPrice1(BigDecimal price1) {
    this.price1 = price1;
  }

  /**
   * Gets the depart area.
   * 
   * @return the depart area
   */
  public String getDepartArea() {
    return departArea;
  }

  /**
   * Sets the depart area.
   * 
   * @param departArea the new depart area
   */
  public void setDepartArea(String departArea) {
    this.departArea = departArea == null ? null : departArea.trim();
  }

  /**
   * Gets the passport yn.
   * 
   * @return the passport yn
   */
  public String getPassportYn() {
    return passportYn;
  }

  /**
   * Sets the passport yn.
   * 
   * @param passportYn the new passport yn
   */
  public void setPassportYn(String passportYn) {
    this.passportYn = passportYn == null ? null : passportYn.trim();
  }

  /**
   * Gets the creator.
   * 
   * @return the creator
   */
  public String getCreator() {
    return creator;
  }

  /**
   * Sets the creator.
   * 
   * @param creator the new creator
   */
  public void setCreator(String creator) {
    this.creator = creator == null ? null : creator.trim();
  }

  /**
   * Gets the creates the dt.
   * 
   * @return the creates the dt
   */
  public Date getCreateDt() {
    return createDt;
  }

  /**
   * Sets the creates the dt.
   * 
   * @param createDt the new creates the dt
   */
  public void setCreateDt(Date createDt) {
    this.createDt = createDt;
  }

  /**
   * Gets the moder.
   * 
   * @return the moder
   */
  public String getModer() {
    return moder;
  }

  /**
   * Sets the moder.
   * 
   * @param moder the new moder
   */
  public void setModer(String moder) {
    this.moder = moder == null ? null : moder.trim();
  }

  /**
   * Gets the mod dt.
   * 
   * @return the mod dt
   */
  public Date getModDt() {
    return modDt;
  }

  /**
   * Sets the mod dt.
   * 
   * @param modDt the new mod dt
   */
  public void setModDt(Date modDt) {
    this.modDt = modDt;
  }

  /**
   * Gets the b2e yn.
   * 
   * @return the b2e yn
   */
  public String getB2eYn() {
    return b2eYn;
  }

  /**
   * Sets the b2e yn.
   * 
   * @param b2eYn the new b2e yn
   */
  public void setB2eYn(String b2eYn) {
    this.b2eYn = b2eYn == null ? null : b2eYn.trim();
  }

  /**
   * Gets the b2b yn.
   * 
   * @return the b2b yn
   */
  public String getB2bYn() {
    return b2bYn;
  }

  /**
   * Sets the b2b yn.
   * 
   * @param b2bYn the new b2b yn
   */
  public void setB2bYn(String b2bYn) {
    this.b2bYn = b2bYn == null ? null : b2bYn.trim();
  }

  /**
   * Gets the deadline.
   * 
   * @return the deadline
   */
  public String getDeadline() {
    return deadline;
  }

  /**
   * Sets the deadline.
   * 
   * @param deadline the new deadline
   */
  public void setDeadline(String deadline) {
    this.deadline = deadline == null ? null : deadline.trim();
  }

  /**
   * Gets the up yn.
   * 
   * @return the up yn
   */
  public String getUpYn() {
    return upYn;
  }

  /**
   * Sets the up yn.
   * 
   * @param upYn the new up yn
   */
  public void setUpYn(String upYn) {
    this.upYn = upYn == null ? null : upYn.trim();
  }
}