/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.core.pojo.frnplatform
 * @FileName: VndMain.java
 * @author:   Ian Chen
 * @date:     2014/9/5, 下午 02:59:33
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.core.pojo.frnplatform;

import java.util.Date;

/**
 * <pre> VndMain </pre>
 * 
 * @author Ian Chen
 */
public class VndMain {
  
  /** The vend no. */
  private String vendNo;

  /** The sht file. */
  private String shtFile;

  /** The b group no. */
  private Integer bGroupNo;

  /** The vend name ch. */
  private String vendNameCh;

  /** The creator. */
  private String creator;

  /** The create dt. */
  private Date createDt;

  /** The moder. */
  private String moder;

  /** The mod dt. */
  private Date modDt;

  /**
   * Gets the vend no.
   * 
   * @return the vend no
   */
  public String getVendNo() {
    return vendNo;
  }

  /**
   * Sets the vend no.
   * 
   * @param vendNo the new vend no
   */
  public void setVendNo(String vendNo) {
    this.vendNo = vendNo == null ? null : vendNo.trim();
  }

  /**
   * Gets the sht file.
   * 
   * @return the sht file
   */
  public String getShtFile() {
    return shtFile;
  }

  /**
   * Sets the sht file.
   * 
   * @param shtFile the new sht file
   */
  public void setShtFile(String shtFile) {
    this.shtFile = shtFile == null ? null : shtFile.trim();
  }

  /**
   * Gets the b group no.
   * 
   * @return the b group no
   */
  public Integer getbGroupNo() {
    return bGroupNo;
  }

  /**
   * Sets the b group no.
   * 
   * @param bGroupNo the new b group no
   */
  public void setbGroupNo(Integer bGroupNo) {
    this.bGroupNo = bGroupNo;
  }

  /**
   * Gets the vend name ch.
   * 
   * @return the vend name ch
   */
  public String getVendNameCh() {
    return vendNameCh;
  }

  /**
   * Sets the vend name ch.
   * 
   * @param vendNameCh the new vend name ch
   */
  public void setVendNameCh(String vendNameCh) {
    this.vendNameCh = vendNameCh == null ? null : vendNameCh.trim();
  }

  /**
   * Gets the creator.
   * 
   * @return the creator
   */
  public String getCreator() {
    return creator;
  }

  /**
   * Sets the creator.
   * 
   * @param creator the new creator
   */
  public void setCreator(String creator) {
    this.creator = creator == null ? null : creator.trim();
  }

  /**
   * Gets the creates the dt.
   * 
   * @return the creates the dt
   */
  public Date getCreateDt() {
    return createDt;
  }

  /**
   * Sets the creates the dt.
   * 
   * @param createDt the new creates the dt
   */
  public void setCreateDt(Date createDt) {
    this.createDt = createDt;
  }

  /**
   * Gets the moder.
   * 
   * @return the moder
   */
  public String getModer() {
    return moder;
  }

  /**
   * Sets the moder.
   * 
   * @param moder the new moder
   */
  public void setModer(String moder) {
    this.moder = moder == null ? null : moder.trim();
  }

  /**
   * Gets the mod dt.
   * 
   * @return the mod dt
   */
  public Date getModDt() {
    return modDt;
  }

  /**
   * Sets the mod dt.
   * 
   * @param modDt the new mod dt
   */
  public void setModDt(Date modDt) {
    this.modDt = modDt;
  }
}