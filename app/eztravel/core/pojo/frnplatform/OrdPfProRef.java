/**
 * EZTRAVEL CONFIDENTIAL
 * 
 * @Package: eztravel.core.pojo.frnplatform
 * @FileName: OrdPfProRef.java
 * @author: allenyen
 * @date: 2014/9/17, 下午 03:44:24
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.core.pojo.frnplatform;

import java.util.Date;

/**
 * The Class OrdPfProRef. 訂單主商品內容參考資訊
 * 
 * <pre>
 * 
 * </pre>
 */
public class OrdPfProRef {


  /** The order no. */
  private String orderNo;// 訂單編號

  /** The pf prod no. */
  private String pfProdNo;// 商品編號

  /** The vend no. */
  private String vendNo;// 供應商編號

  /** The features. */
  private String features;// 主要特點

  /** The service fee. */
  private Integer serviceFee;// 小費

  /** The deposit. */
  private Integer deposit;// 訂金

  /** The tour citys. */
  private String tourCitys;// 旅遊城市代碼群

  /** The tour citys nm. */
  private String tourCitysNm;// 旅遊城市說明

  /** The depart airport nm. */
  private String departAirportNm;// 出發地機場

  /** The fee includes. */
  private String feeIncludes;// 費用包含項目

  /** The excluding items. */
  private String excludingItems;// 費用不包含項目

  /** The tour h lights. */
  private String tourHLights;// 行程特色

  /** The sp plan. */
  private String spPlan;// 特別安排

  /** The g tour rule. */
  private String gTourRule;// 出團規定

  /** The man limit. */
  private Integer manLimit;// 成團人數限制

  /** The travel day. */
  private Integer travelDay;// 旅遊天數

  /** The travel day nm. */
  private String travelDayNm;// 旅遊天數說明

  /** The taxes. */
  private Integer taxes;// 稅金

  /** The creator. */
  private String creator;// 建立者

  /** The create dt. */
  private Date createDt;// 建立日期

  /** The moder. */
  private String moder;// 修改者

  /** The mod dt. */
  private Date modDt;// 修改日期

  public String getOrderNo() {
    return orderNo;
  }

  public void setOrderNo(String orderNo) {
    this.orderNo = orderNo;
  }

  public String getPfProdNo() {
    return pfProdNo;
  }

  public void setPfProdNo(String pfProdNo) {
    this.pfProdNo = pfProdNo;
  }

  public String getVendNo() {
    return vendNo;
  }

  public void setVendNo(String vendNo) {
    this.vendNo = vendNo;
  }

  public String getFeatures() {
    return features;
  }

  public void setFeatures(String features) {
    this.features = features;
  }

  public Integer getServiceFee() {
    return serviceFee;
  }

  public void setServiceFee(Integer serviceFee) {
    this.serviceFee = serviceFee;
  }

  public Integer getDeposit() {
    return deposit;
  }

  public void setDeposit(Integer deposit) {
    this.deposit = deposit;
  }

  public String getTourCitys() {
    return tourCitys;
  }

  public void setTourCitys(String tourCitys) {
    this.tourCitys = tourCitys;
  }

  public String getTourCitysNm() {
    return tourCitysNm;
  }

  public void setTourCitysNm(String tourCitysNm) {
    this.tourCitysNm = tourCitysNm;
  }

  public String getDepartAirportNm() {
    return departAirportNm;
  }

  public void setDepartAirportNm(String departAirportNm) {
    this.departAirportNm = departAirportNm;
  }

  public String getFeeIncludes() {
    return feeIncludes;
  }

  public void setFeeIncludes(String feeIncludes) {
    this.feeIncludes = feeIncludes;
  }

  public String getExcludingItems() {
    return excludingItems;
  }

  public void setExcludingItems(String excludingItems) {
    this.excludingItems = excludingItems;
  }

  public String getTourHLights() {
    return tourHLights;
  }

  public void setTourHLights(String tourHLights) {
    this.tourHLights = tourHLights;
  }

  public String getSpPlan() {
    return spPlan;
  }

  public void setSpPlan(String spPlan) {
    this.spPlan = spPlan;
  }

  public String getgTourRule() {
    return gTourRule;
  }

  public void setgTourRule(String gTourRule) {
    this.gTourRule = gTourRule;
  }

  public Integer getManLimit() {
    return manLimit;
  }

  public void setManLimit(Integer manLimit) {
    this.manLimit = manLimit;
  }

  public Integer getTravelDay() {
    return travelDay;
  }

  public void setTravelDay(Integer travelDay) {
    this.travelDay = travelDay;
  }

  public String getTravelDayNm() {
    return travelDayNm;
  }

  public void setTravelDayNm(String travelDayNm) {
    this.travelDayNm = travelDayNm;
  }

  public Integer getTaxes() {
    return taxes;
  }

  public void setTaxes(Integer taxes) {
    this.taxes = taxes;
  }

  public String getCreator() {
    return creator;
  }

  public void setCreator(String creator) {
    this.creator = creator;
  }

  public Date getCreateDt() {
    return createDt;
  }

  public void setCreateDt(Date createDt) {
    this.createDt = createDt;
  }

  public String getModer() {
    return moder;
  }

  public void setModer(String moder) {
    this.moder = moder;
  }

  public Date getModDt() {
    return modDt;
  }

  public void setModDt(Date modDt) {
    this.modDt = modDt;
  }



}
