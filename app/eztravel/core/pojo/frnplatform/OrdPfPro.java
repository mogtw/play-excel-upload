/**
 * EZTRAVEL CONFIDENTIAL
 * 
 * @Package: eztravel.core.pojo.frnplatform
 * @FileName: OrdPfPro.java
 * @author: allenyen
 * @date: 2014/9/17, 下午 03:30:56
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.core.pojo.frnplatform;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The Class OrdPfPro. 訂單主商品內容
 * 
 * <pre>
 * 
 * </pre>
 */
public class OrdPfPro {

  /** The order no. */
  private String orderNo;// 訂單編號

  /** The pf prod no. */
  private String pfProdNo;// 商品編號

  /** The vend no. */
  private String vendNo;// 供應商編號

  /** The country cd. */
  private String countryCd;// 國家代碼

  /** The line cd. */
  private Short lineCd;// 線別代碼

  /** The prod type. */
  private String prodType;// 商品主類別代碼

  /** The region cd. */
  private String regionCd;// 區域代碼

  /** The pf g prod no. */
  private String pfGProdNo;// 基本團型編號

  /** The prod nm. */
  private String prodNm;// 商品名稱

  /** The beg valid dt. */
  private String begValidDt;// 可以曝光日期

  /** The end valid dt. */
  private String endValidDt;// 結束曝光日期

  /** The beg sale dt. */
  private String begSaleDt;// 開始銷售日期

  /** The end sale dt. */
  private String endSaleDt;// 結束銷售日期

  /** The up yn. */
  private String upYn;// 銷售與否Y/N

  /** The up dt. */
  private String upDt;// 上架日期

  /** The down dt. */
  private String downDt;// 下架日期

  /** The promotion msg. */
  private String promotionMsg;// 促銷訊息

  /** The remark. */
  private String remark;// 促銷說明

  /** The promotion seq. */
  private Short promotionSeq;// 促銷順位

  /** The promotion yn. */
  private String promotionYn;// 促銷YN

  /** The sale type. */
  private Short saleType;// 銷售模式1/2/3/4

  /** The bonus prod yn. */
  private String bonusProdYn;// 紅利可累積抵用

  /** The prod promotion. */
  private String prodPromotion;// 優惠內容

  /** The price1. */
  private BigDecimal price1;// 最低價格

  /** The depart area. */
  private String departArea;// 出發地區域

  /** The passport yn. */
  private String passportYn;// 證照YN

  /** The creator. */
  private String creator;// 建立者

  /** The create dt. */
  private Date createDt;// 建立時間

  /** The moder. */
  private String moder;// 修改者

  /** The mod dt. */
  private Date modDt;// 修改時間

  /** The b2e yn. */
  private String b2eYn;// 企業通路適用

  /** The b2b yn. */
  private String b2bYn;// 同業通路適用

  /** The deadline. */
  private String deadline;// 報名截止日期

  /**
   * Gets the order no.
   * 
   * @return the order no
   */
  public String getOrderNo() {
    return orderNo;
  }

  /**
   * Sets the order no.
   * 
   * @param orderNo the new order no
   */
  public void setOrderNo(String orderNo) {
    this.orderNo = orderNo;
  }

  /**
   * Gets the pf prod no.
   * 
   * @return the pf prod no
   */
  public String getPfProdNo() {
    return pfProdNo;
  }

  /**
   * Sets the pf prod no.
   * 
   * @param pfProdNo the new pf prod no
   */
  public void setPfProdNo(String pfProdNo) {
    this.pfProdNo = pfProdNo;
  }

  /**
   * Gets the vend no.
   * 
   * @return the vend no
   */
  public String getVendNo() {
    return vendNo;
  }

  /**
   * Sets the vend no.
   * 
   * @param vendNo the new vend no
   */
  public void setVendNo(String vendNo) {
    this.vendNo = vendNo;
  }

  /**
   * Gets the country cd.
   * 
   * @return the country cd
   */
  public String getCountryCd() {
    return countryCd;
  }

  /**
   * Sets the country cd.
   * 
   * @param countryCd the new country cd
   */
  public void setCountryCd(String countryCd) {
    this.countryCd = countryCd;
  }

  /**
   * Gets the line cd.
   * 
   * @return the line cd
   */
  public Short getLineCd() {
    return lineCd;
  }

  /**
   * Sets the line cd.
   * 
   * @param lineCd the new line cd
   */
  public void setLineCd(Short lineCd) {
    this.lineCd = lineCd;
  }

  /**
   * Gets the prod type.
   * 
   * @return the prod type
   */
  public String getProdType() {
    return prodType;
  }

  /**
   * Sets the prod type.
   * 
   * @param prodType the new prod type
   */
  public void setProdType(String prodType) {
    this.prodType = prodType;
  }

  /**
   * Gets the region cd.
   * 
   * @return the region cd
   */
  public String getRegionCd() {
    return regionCd;
  }

  /**
   * Sets the region cd.
   * 
   * @param regionCd the new region cd
   */
  public void setRegionCd(String regionCd) {
    this.regionCd = regionCd;
  }

  /**
   * Gets the pf g prod no.
   * 
   * @return the pf g prod no
   */
  public String getPfGProdNo() {
    return pfGProdNo;
  }

  /**
   * Sets the pf g prod no.
   * 
   * @param pfGProdNo the new pf g prod no
   */
  public void setPfGProdNo(String pfGProdNo) {
    this.pfGProdNo = pfGProdNo;
  }

  /**
   * Gets the prod nm.
   * 
   * @return the prod nm
   */
  public String getProdNm() {
    return prodNm;
  }

  /**
   * Sets the prod nm.
   * 
   * @param prodNm the new prod nm
   */
  public void setProdNm(String prodNm) {
    this.prodNm = prodNm;
  }

  /**
   * Gets the beg valid dt.
   * 
   * @return the beg valid dt
   */
  public String getBegValidDt() {
    return begValidDt;
  }

  /**
   * Sets the beg valid dt.
   * 
   * @param begValidDt the new beg valid dt
   */
  public void setBegValidDt(String begValidDt) {
    this.begValidDt = begValidDt;
  }

  /**
   * Gets the end valid dt.
   * 
   * @return the end valid dt
   */
  public String getEndValidDt() {
    return endValidDt;
  }

  /**
   * Sets the end valid dt.
   * 
   * @param endValidDt the new end valid dt
   */
  public void setEndValidDt(String endValidDt) {
    this.endValidDt = endValidDt;
  }

  /**
   * Gets the beg sale dt.
   * 
   * @return the beg sale dt
   */
  public String getBegSaleDt() {
    return begSaleDt;
  }

  /**
   * Sets the beg sale dt.
   * 
   * @param begSaleDt the new beg sale dt
   */
  public void setBegSaleDt(String begSaleDt) {
    this.begSaleDt = begSaleDt;
  }

  /**
   * Gets the end sale dt.
   * 
   * @return the end sale dt
   */
  public String getEndSaleDt() {
    return endSaleDt;
  }

  /**
   * Sets the end sale dt.
   * 
   * @param endSaleDt the new end sale dt
   */
  public void setEndSaleDt(String endSaleDt) {
    this.endSaleDt = endSaleDt;
  }

  /**
   * Gets the up yn.
   * 
   * @return the up yn
   */
  public String getUpYn() {
    return upYn;
  }

  /**
   * Sets the up yn.
   * 
   * @param upYn the new up yn
   */
  public void setUpYn(String upYn) {
    this.upYn = upYn;
  }

  /**
   * Gets the up dt.
   * 
   * @return the up dt
   */
  public String getUpDt() {
    return upDt;
  }

  /**
   * Sets the up dt.
   * 
   * @param upDt the new up dt
   */
  public void setUpDt(String upDt) {
    this.upDt = upDt;
  }

  /**
   * Gets the down dt.
   * 
   * @return the down dt
   */
  public String getDownDt() {
    return downDt;
  }

  /**
   * Sets the down dt.
   * 
   * @param downDt the new down dt
   */
  public void setDownDt(String downDt) {
    this.downDt = downDt;
  }

  /**
   * Gets the promotion msg.
   * 
   * @return the promotion msg
   */
  public String getPromotionMsg() {
    return promotionMsg;
  }

  /**
   * Sets the promotion msg.
   * 
   * @param promotionMsg the new promotion msg
   */
  public void setPromotionMsg(String promotionMsg) {
    this.promotionMsg = promotionMsg;
  }

  /**
   * Gets the remark.
   * 
   * @return the remark
   */
  public String getRemark() {
    return remark;
  }

  /**
   * Sets the remark.
   * 
   * @param remark the new remark
   */
  public void setRemark(String remark) {
    this.remark = remark;
  }

  /**
   * Gets the promotion seq.
   * 
   * @return the promotion seq
   */
  public Short getPromotionSeq() {
    return promotionSeq;
  }

  /**
   * Sets the promotion seq.
   * 
   * @param promotionSeq the new promotion seq
   */
  public void setPromotionSeq(Short promotionSeq) {
    this.promotionSeq = promotionSeq;
  }

  /**
   * Gets the promotion yn.
   * 
   * @return the promotion yn
   */
  public String getPromotionYn() {
    return promotionYn;
  }

  /**
   * Sets the promotion yn.
   * 
   * @param promotionYn the new promotion yn
   */
  public void setPromotionYn(String promotionYn) {
    this.promotionYn = promotionYn;
  }

  /**
   * Gets the sale type.
   * 
   * @return the sale type
   */
  public Short getSaleType() {
    return saleType;
  }

  /**
   * Sets the sale type.
   * 
   * @param saleType the new sale type
   */
  public void setSaleType(Short saleType) {
    this.saleType = saleType;
  }

  /**
   * Gets the bonus prod yn.
   * 
   * @return the bonus prod yn
   */
  public String getBonusProdYn() {
    return bonusProdYn;
  }

  /**
   * Sets the bonus prod yn.
   * 
   * @param bonusProdYn the new bonus prod yn
   */
  public void setBonusProdYn(String bonusProdYn) {
    this.bonusProdYn = bonusProdYn;
  }

  /**
   * Gets the prod promotion.
   * 
   * @return the prod promotion
   */
  public String getProdPromotion() {
    return prodPromotion;
  }

  /**
   * Sets the prod promotion.
   * 
   * @param prodPromotion the new prod promotion
   */
  public void setProdPromotion(String prodPromotion) {
    this.prodPromotion = prodPromotion;
  }

  /**
   * Gets the price1.
   * 
   * @return the price1
   */
  public BigDecimal getPrice1() {
    return price1;
  }

  /**
   * Sets the price1.
   * 
   * @param price1 the new price1
   */
  public void setPrice1(BigDecimal price1) {
    this.price1 = price1;
  }
  
  /**
   * Gets the depart area.
   * 
   * @return the depart area
   */
  public String getDepartArea() {
    return departArea;
  }

  /**
   * Sets the depart area.
   * 
   * @param departArea the new depart area
   */
  public void setDepartArea(String departArea) {
    this.departArea = departArea;
  }

  /**
   * Gets the passport yn.
   * 
   * @return the passport yn
   */
  public String getPassportYn() {
    return passportYn;
  }

  /**
   * Sets the passport yn.
   * 
   * @param passportYn the new passport yn
   */
  public void setPassportYn(String passportYn) {
    this.passportYn = passportYn;
  }

  /**
   * Gets the creator.
   * 
   * @return the creator
   */
  public String getCreator() {
    return creator;
  }

  /**
   * Sets the creator.
   * 
   * @param creator the new creator
   */
  public void setCreator(String creator) {
    this.creator = creator;
  }

  /**
   * Gets the creates the dt.
   * 
   * @return the creates the dt
   */
  public Date getCreateDt() {
    return createDt;
  }

  /**
   * Sets the creates the dt.
   * 
   * @param createDt the new creates the dt
   */
  public void setCreateDt(Date createDt) {
    this.createDt = createDt;
  }

  /**
   * Gets the moder.
   * 
   * @return the moder
   */
  public String getModer() {
    return moder;
  }

  /**
   * Sets the moder.
   * 
   * @param moder the new moder
   */
  public void setModer(String moder) {
    this.moder = moder;
  }

  /**
   * Gets the mod dt.
   * 
   * @return the mod dt
   */
  public Date getModDt() {
    return modDt;
  }

  /**
   * Sets the mod dt.
   * 
   * @param modDt the new mod dt
   */
  public void setModDt(Date modDt) {
    this.modDt = modDt;
  }

  /**
   * Gets the b2e yn.
   * 
   * @return the b2e yn
   */
  public String getB2eYn() {
    return b2eYn;
  }

  /**
   * Sets the b2e yn.
   * 
   * @param b2eYn the new b2e yn
   */
  public void setB2eYn(String b2eYn) {
    this.b2eYn = b2eYn;
  }

  /**
   * Gets the b2b yn.
   * 
   * @return the b2b yn
   */
  public String getB2bYn() {
    return b2bYn;
  }

  /**
   * Sets the b2b yn.
   * 
   * @param b2bYn the new b2b yn
   */
  public void setB2bYn(String b2bYn) {
    this.b2bYn = b2bYn;
  }

  /**
   * Gets the deadline.
   * 
   * @return the deadline
   */
  public String getDeadline() {
    return deadline;
  }

  /**
   * Sets the deadline.
   * 
   * @param deadline the new deadline
   */
  public void setDeadline(String deadline) {
    this.deadline = deadline;
  }



}
