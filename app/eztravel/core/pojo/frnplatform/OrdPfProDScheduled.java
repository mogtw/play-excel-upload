/**
 * EZTRAVEL CONFIDENTIAL
 * 
 * @Package: eztravel.core.pojo.frnplatform
 * @FileName: OrdPfProDScheduled.java
 * @author: allenyen
 * @date: 2014/9/17, 下午 03:17:53
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.core.pojo.frnplatform;

import java.util.Date;

/**
 * The Class OrdPfProDScheduled. 訂單行程內容
 * 
 * <pre>
 * 
 * </pre>
 */
public class OrdPfProDScheduled {

  /** The order no. */
  private String orderNo;// 訂單編號

  /** The pf prod no. */
  private String pfProdNo;// 商品編號

  /** The vend no. */
  private String vendNo;// 供應商編號

  /** The days. */
  private String days;// 日序

  /** The time seq. */
  private String timeSeq;// 時序

  /** The proc brief. */
  private String procBrief;// 行程簡述

  /** The proc context. */
  private String procContext;// 行程內容

  /** The breakfast. */
  private String breakfast;// 早餐

  /** The morning tea. */
  private String morningTea;// 早茶

  /** The lunch. */
  private String lunch;// 午餐

  /** The afternoon tea. */
  private String afternoonTea;// 下午茶

  /** The dinner. */
  private String dinner;// 晚餐

  /** The night snack. */
  private String nightSnack;// 宵夜

  /** The htl desc. */
  private String htlDesc;// 飯店說明

  /** The htl desc url. */
  private String htlDescUrl;// 飯店說明網址

  /** The creator. */
  private String creator;// 建立者

  /** The create dt. */
  private Date createDt;// 建立時間

  /** The moder. */
  private String moder;// 修改者

  /** The mod dt. */
  private Date modDt;// 修改時間

  /**
   * Gets the order no.
   * 
   * @return the order no
   */
  public String getOrderNo() {
    return orderNo;
  }

  /**
   * Sets the order no.
   * 
   * @param orderNo the new order no
   */
  public void setOrderNo(String orderNo) {
    this.orderNo = orderNo;
  }

  /**
   * Gets the pf prod no.
   * 
   * @return the pf prod no
   */
  public String getPfProdNo() {
    return pfProdNo;
  }

  /**
   * Sets the pf prod no.
   * 
   * @param pfProdNo the new pf prod no
   */
  public void setPfProdNo(String pfProdNo) {
    this.pfProdNo = pfProdNo;
  }

  /**
   * Gets the vend no.
   * 
   * @return the vend no
   */
  public String getVendNo() {
    return vendNo;
  }

  /**
   * Sets the vend no.
   * 
   * @param vendNo the new vend no
   */
  public void setVendNo(String vendNo) {
    this.vendNo = vendNo;
  }

  /**
   * Gets the days.
   * 
   * @return the days
   */
  public String getDays() {
    return days;
  }

  /**
   * Sets the days.
   * 
   * @param days the new days
   */
  public void setDays(String days) {
    this.days = days;
  }

  /**
   * Gets the time seq.
   * 
   * @return the time seq
   */
  public String getTimeSeq() {
    return timeSeq;
  }

  /**
   * Sets the time seq.
   * 
   * @param timeSeq the new time seq
   */
  public void setTimeSeq(String timeSeq) {
    this.timeSeq = timeSeq;
  }

  /**
   * Gets the proc brief.
   * 
   * @return the proc brief
   */
  public String getProcBrief() {
    return procBrief;
  }

  /**
   * Sets the proc brief.
   * 
   * @param procBrief the new proc brief
   */
  public void setProcBrief(String procBrief) {
    this.procBrief = procBrief;
  }

  /**
   * Gets the proc context.
   * 
   * @return the proc context
   */
  public String getProcContext() {
    return procContext;
  }

  /**
   * Sets the proc context.
   * 
   * @param procContext the new proc context
   */
  public void setProcContext(String procContext) {
    this.procContext = procContext;
  }

  /**
   * Gets the breakfast.
   * 
   * @return the breakfast
   */
  public String getBreakfast() {
    return breakfast;
  }

  /**
   * Sets the breakfast.
   * 
   * @param breakfast the new breakfast
   */
  public void setBreakfast(String breakfast) {
    this.breakfast = breakfast;
  }

  /**
   * Gets the morning tea.
   * 
   * @return the morning tea
   */
  public String getMorningTea() {
    return morningTea;
  }

  /**
   * Sets the morning tea.
   * 
   * @param morningTea the new morning tea
   */
  public void setMorningTea(String morningTea) {
    this.morningTea = morningTea;
  }

  /**
   * Gets the lunch.
   * 
   * @return the lunch
   */
  public String getLunch() {
    return lunch;
  }

  /**
   * Sets the lunch.
   * 
   * @param lunch the new lunch
   */
  public void setLunch(String lunch) {
    this.lunch = lunch;
  }

  /**
   * Gets the afternoon tea.
   * 
   * @return the afternoon tea
   */
  public String getAfternoonTea() {
    return afternoonTea;
  }

  /**
   * Sets the afternoon tea.
   * 
   * @param afternoonTea the new afternoon tea
   */
  public void setAfternoonTea(String afternoonTea) {
    this.afternoonTea = afternoonTea;
  }

  /**
   * Gets the dinner.
   * 
   * @return the dinner
   */
  public String getDinner() {
    return dinner;
  }

  /**
   * Sets the dinner.
   * 
   * @param dinner the new dinner
   */
  public void setDinner(String dinner) {
    this.dinner = dinner;
  }

  /**
   * Gets the night snack.
   * 
   * @return the night snack
   */
  public String getNightSnack() {
    return nightSnack;
  }

  /**
   * Sets the night snack.
   * 
   * @param nightSnack the new night snack
   */
  public void setNightSnack(String nightSnack) {
    this.nightSnack = nightSnack;
  }

  /**
   * Gets the htl desc.
   * 
   * @return the htl desc
   */
  public String getHtlDesc() {
    return htlDesc;
  }

  /**
   * Sets the htl desc.
   * 
   * @param htlDesc the new htl desc
   */
  public void setHtlDesc(String htlDesc) {
    this.htlDesc = htlDesc;
  }

  /**
   * Gets the htl desc url.
   * 
   * @return the htl desc url
   */
  public String getHtlDescUrl() {
    return htlDescUrl;
  }

  /**
   * Sets the htl desc url.
   * 
   * @param htlDescUrl the new htl desc url
   */
  public void setHtlDescUrl(String htlDescUrl) {
    this.htlDescUrl = htlDescUrl;
  }

  /**
   * Gets the creator.
   * 
   * @return the creator
   */
  public String getCreator() {
    return creator;
  }

  /**
   * Sets the creator.
   * 
   * @param creator the new creator
   */
  public void setCreator(String creator) {
    this.creator = creator;
  }

  /**
   * Gets the creates the dt.
   * 
   * @return the creates the dt
   */
  public Date getCreateDt() {
    return createDt;
  }

  /**
   * Sets the creates the dt.
   * 
   * @param createDt the new creates the dt
   */
  public void setCreateDt(Date createDt) {
    this.createDt = createDt;
  }

  /**
   * Gets the moder.
   * 
   * @return the moder
   */
  public String getModer() {
    return moder;
  }

  /**
   * Sets the moder.
   * 
   * @param moder the new moder
   */
  public void setModer(String moder) {
    this.moder = moder;
  }

  /**
   * Gets the mod dt.
   * 
   * @return the mod dt
   */
  public Date getModDt() {
    return modDt;
  }

  /**
   * Sets the mod dt.
   * 
   * @param modDt the new mod dt
   */
  public void setModDt(Date modDt) {
    this.modDt = modDt;
  }


}
