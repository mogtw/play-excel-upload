/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.core.pojo.frnplatform
 * @FileName: CodeDetail.java
 * @author:   Ian Chen
 * @date:     2014/8/28, 下午 02:24:43
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.core.pojo.frnplatform;

import java.util.Date;

/**
 * <pre> CodeDetail </pre>
 *
 * @author Ian Chen
 */
public class CodeDetail {

  /** The item id. */
  private String itemId;

  /** The code id. */
  private String codeId;

  /** The code name. */
  private String codeName;

  /** The code desc. */
  private String codeDesc;

  /** The code type. */
  private String codeType;

  /** The display seq. */
  private Short displaySeq;

  /** The code name s. */
  private String codeNameS;

  /** The creator. */
  private String creator;

  /** The create dt. */
  private Date createDt;

  /** The moder. */
  private String moder;

  /** The mod dt. */
  private Date modDt;

  /**
   * Gets the item id.
   * 
   * @return the item id
   */
  public String getItemId() {
    return itemId;
  }

  /**
   * Sets the item id.
   * 
   * @param itemId the new item id
   */
  public void setItemId(String itemId) {
    this.itemId = itemId == null ? null : itemId.trim();
  }

  /**
   * Gets the code id.
   * 
   * @return the code id
   */
  public String getCodeId() {
    return codeId;
  }

  /**
   * Sets the code id.
   * 
   * @param codeId the new code id
   */
  public void setCodeId(String codeId) {
    this.codeId = codeId == null ? null : codeId.trim();
  }

  /**
   * Gets the code name.
   * 
   * @return the code name
   */
  public String getCodeName() {
    return codeName;
  }

  /**
   * Sets the code name.
   * 
   * @param codeName the new code name
   */
  public void setCodeName(String codeName) {
    this.codeName = codeName == null ? null : codeName.trim();
  }

  /**
   * Gets the code desc.
   * 
   * @return the code desc
   */
  public String getCodeDesc() {
    return codeDesc;
  }

  /**
   * Sets the code desc.
   * 
   * @param codeDesc the new code desc
   */
  public void setCodeDesc(String codeDesc) {
    this.codeDesc = codeDesc == null ? null : codeDesc.trim();
  }

  /**
   * Gets the code type.
   * 
   * @return the code type
   */
  public String getCodeType() {
    return codeType;
  }

  /**
   * Sets the code type.
   * 
   * @param codeType the new code type
   */
  public void setCodeType(String codeType) {
    this.codeType = codeType == null ? null : codeType.trim();
  }

  /**
   * Gets the display seq.
   * 
   * @return the display seq
   */
  public Short getDisplaySeq() {
    return displaySeq;
  }

  /**
   * Sets the display seq.
   * 
   * @param displaySeq the new display seq
   */
  public void setDisplaySeq(Short displaySeq) {
    this.displaySeq = displaySeq;
  }

  /**
   * Gets the code name s.
   * 
   * @return the code name s
   */
  public String getCodeNameS() {
    return codeNameS;
  }

  /**
   * Sets the code name s.
   * 
   * @param codeNameS the new code name s
   */
  public void setCodeNameS(String codeNameS) {
    this.codeNameS = codeNameS == null ? null : codeNameS.trim();
  }

  /**
   * Gets the creator.
   * 
   * @return the creator
   */
  public String getCreator() {
    return creator;
  }

  /**
   * Sets the creator.
   * 
   * @param creator the new creator
   */
  public void setCreator(String creator) {
    this.creator = creator == null ? null : creator.trim();
  }

  /**
   * Gets the creates the dt.
   * 
   * @return the creates the dt
   */
  public Date getCreateDt() {
    return createDt;
  }

  /**
   * Sets the creates the dt.
   * 
   * @param createDt the new creates the dt
   */
  public void setCreateDt(Date createDt) {
    this.createDt = createDt;
  }

  /**
   * Gets the moder.
   * 
   * @return the moder
   */
  public String getModer() {
    return moder;
  }

  /**
   * Sets the moder.
   * 
   * @param moder the new moder
   */
  public void setModer(String moder) {
    this.moder = moder == null ? null : moder.trim();
  }

  /**
   * Gets the mod dt.
   * 
   * @return the mod dt
   */
  public Date getModDt() {
    return modDt;
  }

  /**
   * Sets the mod dt.
   * 
   * @param modDt the new mod dt
   */
  public void setModDt(Date modDt) {
    this.modDt = modDt;
  }
}