/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.core.pojo.frnplatform
 * @FileName: PfProImpJobStatus.java
 * @author:   Ian Chen
 * @date:     2014/9/3, 下午4:58:16
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.core.pojo.frnplatform;

/**
 * <pre> PfProImpJobStatus </pre>
 * 
 * @author Ian Chen
 */
public final class PfProImpJobStatus {
  
  /** The Constant WAIT. */
  public static final String WAIT = "WAIT";
  
  /** The Constant WORK. */
  public static final String WORK = "WORK";
  
  /** The Constant OK. */
  public static final String OK = "OK";
  
  /** The Constant FAIL. */
  public static final String FAIL = "FAIL";
}
