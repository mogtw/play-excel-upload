/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.core.pojo.frnplatform
 * @FileName: PfProSearch.java
 * @author:   Ian Chen
 * @date:     2014/9/18, 下午5:13:31
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.core.pojo.frnplatform;

import java.math.BigDecimal;

/**
 * 搜尋主畫面之欄位
 * 
 * @author Ian Chen
 */
public class PfProSearch {
  
  /** The pf prod no. */
  private String pfProdNo;

  /** The vend no. */
  private String vendNo;

  /** The sale dt. */
  private String saleDt;
  
  /** The travel day. */
  private Integer travelDay;
  
  /** The prod nm. */
  private String prodNm;
  
  /** The man limit. */
  private Integer manLimit;
  
  /** The tot qty. */
  private Integer totQty;
  
  /** The ord qty. */
  private Integer ordQty;
  
  /** The remark. */
  private String remark;
  
  /** The ez price1. */
  private BigDecimal ezPrice1;
  
  /**
   * Gets the pf prod no.
   * 
   * @return the pf prod no
   */
  public String getPfProdNo() {
    return pfProdNo;
  }

  /**
   * Sets the pf prod no.
   * 
   * @param pfProdNo the new pf prod no
   */
  public void setPfProdNo(String pfProdNo) {
    this.pfProdNo = pfProdNo;
  }

  /**
   * Gets the vend no.
   * 
   * @return the vend no
   */
  public String getVendNo() {
    return vendNo;
  }

  /**
   * Sets the vend no.
   * 
   * @param vendNo the new vend no
   */
  public void setVendNo(String vendNo) {
    this.vendNo = vendNo;
  }

  /**
   * Gets the sale dt.
   * 
   * @return the sale dt
   */
  public String getSaleDt() {
    return saleDt;
  }
  
  /**
   * Sets the sale dt.
   * 
   * @param saleDt the new sale dt
   */
  public void setSaleDt(String saleDt) {
    this.saleDt = saleDt;
  }
  
  /**
   * Gets the travel day.
   * 
   * @return the travel day
   */
  public Integer getTravelDay() {
    return travelDay;
  }
  
  /**
   * Sets the travel day.
   * 
   * @param travelDay the new travel day
   */
  public void setTravelDay(Integer travelDay) {
    this.travelDay = travelDay;
  }
  
  /**
   * Gets the prod nm.
   * 
   * @return the prod nm
   */
  public String getProdNm() {
    return prodNm;
  }
  
  /**
   * Sets the prod nm.
   * 
   * @param prodNm the new prod nm
   */
  public void setProdNm(String prodNm) {
    this.prodNm = prodNm;
  }
  
  /**
   * Gets the man limit.
   * 
   * @return the man limit
   */
  public Integer getManLimit() {
    return manLimit;
  }
  
  /**
   * Sets the man limit.
   * 
   * @param manLimit the new man limit
   */
  public void setManLimit(Integer manLimit) {
    this.manLimit = manLimit;
  }
  
  /**
   * Gets the tot qty.
   * 
   * @return the tot qty
   */
  public Integer getTotQty() {
    return totQty;
  }
  
  /**
   * Sets the tot qty.
   * 
   * @param totQty the new tot qty
   */
  public void setTotQty(Integer totQty) {
    this.totQty = totQty;
  }
  
  /**
   * Gets the ord qty.
   * 
   * @return the ord qty
   */
  public Integer getOrdQty() {
    return ordQty;
  }
  
  /**
   * Sets the ord qty.
   * 
   * @param ordQty the new ord qty
   */
  public void setOrdQty(Integer ordQty) {
    this.ordQty = ordQty;
  }
  
  /**
   * Gets the remark.
   * 
   * @return the remark
   */
  public String getRemark() {
    return remark;
  }
  
  /**
   * Sets the remark.
   * 
   * @param remark the new remark
   */
  public void setRemark(String remark) {
    this.remark = remark;
  }
  
  /**
   * Gets the ez price1.
   * 
   * @return the ez price1
   */
  public BigDecimal getEzPrice1() {
    return ezPrice1;
  }
  
  /**
   * Sets the ez price1.
   * 
   * @param ezPrice1 the new ez price1
   */
  public void setEzPrice1(BigDecimal ezPrice1) {
    this.ezPrice1 = ezPrice1;
  }
}
