/**
 * EZTRAVEL CONFIDENTIAL
 * 
 * @Package: eztravel.core.pojo.frnplatform
 * @FileName: PfPrdPln.java
 * @author: allenyen
 * @date: 2014/9/22, 上午 09:37:39
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.core.pojo.frnplatform;

import java.util.Date;

/**
 * The Class PfPrdPln.
 * 
 * <pre>
 * 
 * </pre>
 */
public class PfPrdPln {

  /** The order no. */
  private String orderNo;// 訂單編號

  /** The cust seqno. */
  private String custSeqno;// 客戶流水號

  /** The prod seqno. */
  private String prodSeqno;// 商品流水號

  /** The round cd. */
  private Short roundCd;// 航班來回編號

  /** The seq no. */
  private Short seqNo;// 航班順序號


  /** The airline no. */
  private String airlineNo;// 航空公司代表號

  /** The schedule no. */
  private String scheduleNo;// 航班編號

  /** The depart airport. */
  private String departAirport;// 出發機場

  /** The arrive airport. */
  private String arriveAirport;// 抵達機場

  /** The depart time. */
  private String departTime;// 出發時間

  /** The arrive time. */
  private String arriveTime;// 抵達時間


  /** The day diff. */
  private String dayDiff;// 差異日

  /** The creator. */
  private String creator;// 建立者

  /** The create dt. */
  private Date createDt;// 建立時間

  /** The moder. */
  private String moder;// 修改者

  /** The mod dt. */
  private Date modDt;// 修改時間

  /**
   * Gets the order no.
   * 
   * @return the order no
   */
  public String getOrderNo() {
    return orderNo;
  }

  /**
   * Sets the order no.
   * 
   * @param orderNo the new order no
   */
  public void setOrderNo(String orderNo) {
    this.orderNo = orderNo;
  }

  /**
   * Gets the cust seqno.
   * 
   * @return the cust seqno
   */
  public String getCustSeqno() {
    return custSeqno;
  }

  /**
   * Sets the cust seqno.
   * 
   * @param custSeqno the new cust seqno
   */
  public void setCustSeqno(String custSeqno) {
    this.custSeqno = custSeqno;
  }

  /**
   * Gets the prod seqno.
   * 
   * @return the prod seqno
   */
  public String getProdSeqno() {
    return prodSeqno;
  }

  /**
   * Sets the prod seqno.
   * 
   * @param prodSeqno the new prod seqno
   */
  public void setProdSeqno(String prodSeqno) {
    this.prodSeqno = prodSeqno;
  }

  /**
   * Gets the round cd.
   * 
   * @return the round cd
   */
  public Short getRoundCd() {
    return roundCd;
  }

  /**
   * Sets the round cd.
   * 
   * @param roundCd the new round cd
   */
  public void setRoundCd(Short roundCd) {
    this.roundCd = roundCd;
  }

  /**
   * Gets the seq no.
   * 
   * @return the seq no
   */
  public Short getSeqNo() {
    return seqNo;
  }

  /**
   * Sets the seq no.
   * 
   * @param seqNo the new seq no
   */
  public void setSeqNo(Short seqNo) {
    this.seqNo = seqNo;
  }


  /**
   * Gets the airline no.
   * 
   * @return the airline no
   */
  public String getAirlineNo() {
    return airlineNo;
  }

  /**
   * Sets the airline no.
   * 
   * @param airlineNo the new airline no
   */
  public void setAirlineNo(String airlineNo) {
    this.airlineNo = airlineNo;
  }

  /**
   * Gets the schedule no.
   * 
   * @return the schedule no
   */
  public String getScheduleNo() {
    return scheduleNo;
  }

  /**
   * Sets the schedule no.
   * 
   * @param scheduleNo the new schedule no
   */
  public void setScheduleNo(String scheduleNo) {
    this.scheduleNo = scheduleNo;
  }

  /**
   * Gets the depart airport.
   * 
   * @return the depart airport
   */
  public String getDepartAirport() {
    return departAirport;
  }

  /**
   * Sets the depart airport.
   * 
   * @param departAirport the new depart airport
   */
  public void setDepartAirport(String departAirport) {
    this.departAirport = departAirport;
  }

  /**
   * Gets the arrive airport.
   * 
   * @return the arrive airport
   */
  public String getArriveAirport() {
    return arriveAirport;
  }

  /**
   * Sets the arrive airport.
   * 
   * @param arriveAirport the new arrive airport
   */
  public void setArriveAirport(String arriveAirport) {
    this.arriveAirport = arriveAirport;
  }

  /**
   * Gets the depart time.
   * 
   * @return the depart time
   */
  public String getDepartTime() {
    return departTime;
  }

  /**
   * Sets the depart time.
   * 
   * @param departTime the new depart time
   */
  public void setDepartTime(String departTime) {
    this.departTime = departTime;
  }

  /**
   * Gets the arrive time.
   * 
   * @return the arrive time
   */
  public String getArriveTime() {
    return arriveTime;
  }

  /**
   * Sets the arrive time.
   * 
   * @param arriveTime the new arrive time
   */
  public void setArriveTime(String arriveTime) {
    this.arriveTime = arriveTime;
  }

  /**
   * Gets the day diff.
   * 
   * @return the day diff
   */
  public String getDayDiff() {
    return dayDiff;
  }

  /**
   * Sets the day diff.
   * 
   * @param dayDiff the new day diff
   */
  public void setDayDiff(String dayDiff) {
    this.dayDiff = dayDiff;
  }

  /**
   * Gets the creator.
   * 
   * @return the creator
   */
  public String getCreator() {
    return creator;
  }

  /**
   * Sets the creator.
   * 
   * @param creator the new creator
   */
  public void setCreator(String creator) {
    this.creator = creator;
  }

  /**
   * Gets the creates the dt.
   * 
   * @return the creates the dt
   */
  public Date getCreateDt() {
    return createDt;
  }

  /**
   * Sets the creates the dt.
   * 
   * @param createDt the new creates the dt
   */
  public void setCreateDt(Date createDt) {
    this.createDt = createDt;
  }

  /**
   * Gets the moder.
   * 
   * @return the moder
   */
  public String getModer() {
    return moder;
  }

  /**
   * Sets the moder.
   * 
   * @param moder the new moder
   */
  public void setModer(String moder) {
    this.moder = moder;
  }

  /**
   * Gets the mod dt.
   * 
   * @return the mod dt
   */
  public Date getModDt() {
    return modDt;
  }

  /**
   * Sets the mod dt.
   * 
   * @param modDt the new mod dt
   */
  public void setModDt(Date modDt) {
    this.modDt = modDt;
  }
}
