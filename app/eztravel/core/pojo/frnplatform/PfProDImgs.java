/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.core.pojo.frnplatform
 * @FileName: PfProDImgs.java
 * @author:   Ian Chen
 * @date:     2014/8/28, 下午 02:26:30
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.core.pojo.frnplatform;

import java.util.Date;

/**
 * <pre> PfProDImgs </pre>
 * 
 * @author Ian Chen
 */
public class PfProDImgs {
  
  /** The table prefix. */
  private String tablePrefix;
  
  /** The pf prod no. */
  private String pfProdNo;

  /** The vend no. */
  private String vendNo;

  /** The img seqno. */
  private Short imgSeqno;

  /** The days. */
  private String days;

  /** The img url. */
  private String imgUrl;

  /** The img name. */
  private String imgName;

  /** The creator. */
  private String creator;

  /** The create dt. */
  private Date createDt;

  /** The moder. */
  private String moder;

  /** The mod dt. */
  private Date modDt;

  /**
   * Gets the table prefix.
   * 
   * @return the table prefix
   */
  public String getTablePrefix() {
    return tablePrefix;
  }

  /**
   * Sets the table prefix.
   * 
   * @param tablePrefix the new table prefix
   */
  public void setTablePrefix(String tablePrefix) {
    this.tablePrefix = tablePrefix;
  }

  /**
   * Gets the pf prod no.
   * 
   * @return the pf prod no
   */
  public String getPfProdNo() {
    return pfProdNo;
  }

  /**
   * Sets the pf prod no.
   * 
   * @param pfProdNo the new pf prod no
   */
  public void setPfProdNo(String pfProdNo) {
    this.pfProdNo = pfProdNo == null ? null : pfProdNo.trim();
  }

  /**
   * Gets the vend no.
   * 
   * @return the vend no
   */
  public String getVendNo() {
    return vendNo;
  }

  /**
   * Sets the vend no.
   * 
   * @param vendNo the new vend no
   */
  public void setVendNo(String vendNo) {
    this.vendNo = vendNo == null ? null : vendNo.trim();
  }

  /**
   * Gets the img seqno.
   * 
   * @return the img seqno
   */
  public Short getImgSeqno() {
    return imgSeqno;
  }

  /**
   * Sets the img seqno.
   * 
   * @param imgSeqno the new img seqno
   */
  public void setImgSeqno(Short imgSeqno) {
    this.imgSeqno = imgSeqno;
  }

  /**
   * Gets the days.
   * 
   * @return the days
   */
  public String getDays() {
    return days;
  }

  /**
   * Sets the days.
   * 
   * @param days the new days
   */
  public void setDays(String days) {
    this.days = days == null ? null : days.trim();
  }

  /**
   * Gets the img url.
   * 
   * @return the img url
   */
  public String getImgUrl() {
    return imgUrl;
  }

  /**
   * Sets the img url.
   * 
   * @param imgUrl the new img url
   */
  public void setImgUrl(String imgUrl) {
    this.imgUrl = imgUrl == null ? null : imgUrl.trim();
  }

  /**
   * Gets the img name.
   * 
   * @return the img name
   */
  public String getImgName() {
    return imgName;
  }

  /**
   * Sets the img name.
   * 
   * @param imgName the new img name
   */
  public void setImgName(String imgName) {
    this.imgName = imgName == null ? null : imgName.trim();
  }

  /**
   * Gets the creator.
   * 
   * @return the creator
   */
  public String getCreator() {
    return creator;
  }

  /**
   * Sets the creator.
   * 
   * @param creator the new creator
   */
  public void setCreator(String creator) {
    this.creator = creator == null ? null : creator.trim();
  }

  /**
   * Gets the creates the dt.
   * 
   * @return the creates the dt
   */
  public Date getCreateDt() {
    return createDt;
  }

  /**
   * Sets the creates the dt.
   * 
   * @param createDt the new creates the dt
   */
  public void setCreateDt(Date createDt) {
    this.createDt = createDt;
  }

  /**
   * Gets the moder.
   * 
   * @return the moder
   */
  public String getModer() {
    return moder;
  }

  /**
   * Sets the moder.
   * 
   * @param moder the new moder
   */
  public void setModer(String moder) {
    this.moder = moder == null ? null : moder.trim();
  }

  /**
   * Gets the mod dt.
   * 
   * @return the mod dt
   */
  public Date getModDt() {
    return modDt;
  }

  /**
   * Sets the mod dt.
   * 
   * @param modDt the new mod dt
   */
  public void setModDt(Date modDt) {
    this.modDt = modDt;
  }
}