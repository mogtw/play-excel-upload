/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.core.pojo.frnplatform.wrapper
 * @FileName: PfProOutput.java
 * @author:   Ian Chen
 * @date:     2014/8/25, 下午 04:44:39
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.core.pojo.frnplatform.wrapper;

/**
 * <pre> PfProOutput, 匯入後的輸出物件. </pre>
 *
 * @author Ian Chen
 */
public class PfProOutput {
  
  /** The job no. */
  private String jobNo;
  
  /** The job status. */
  private String jobStatus;
  
  /**
   * Instantiates a new pf pro output.
   */
  private PfProOutput() {}
  
  /**
   * Instantiates a new pf pro output.
   * 
   * @param jobNo the job no
   * @param jobStatus the job status
   */
  public PfProOutput(String jobNo, String jobStatus) {
    this();
    this.jobNo = jobNo;
    this.jobStatus = jobStatus;
  }

  /**
   * Gets the job no.
   * 
   * @return the job no
   */
  public String getJobNo() {
    return jobNo;
  }

  /**
   * Sets the job no.
   * 
   * @param jobNo the new job no
   */
  public void setJobNo(String jobNo) {
    this.jobNo = jobNo;
  }

  /**
   * Gets the job status.
   * 
   * @return the job status
   */
  public String getJobStatus() {
    return jobStatus;
  }

  /**
   * Sets the job status.
   * 
   * @param jobStatus the new job status
   */
  public void setJobStatus(String jobStatus) {
    this.jobStatus = jobStatus;
  }
}
