/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.core.pojo.frnplatform.wrapper
 * @FileName: PfProGroup.java
 * @author:   Ian Chen
 * @date:     2014/8/25, 上午11:17:28
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.core.pojo.frnplatform.wrapper;

import java.util.List;

import eztravel.core.pojo.frnplatform.PfPro;
import eztravel.core.pojo.frnplatform.PfProAirinfo;
import eztravel.core.pojo.frnplatform.PfProAllot;
import eztravel.core.pojo.frnplatform.PfProDImgs;
import eztravel.core.pojo.frnplatform.PfProDScheduled;
import eztravel.core.pojo.frnplatform.PfProImgs;
import eztravel.core.pojo.frnplatform.PfProPrice;
import eztravel.core.pojo.frnplatform.PfProRef;

/**
 * <pre>
 * PfProGroup
 * 以『一個』主商品及其相關明細 組成之單一主商品群組
 * 1個:
 *  PfPro
 *  PfProRef
 *  PfProAllot
 * 多筆:
 *  PfProAirinfo
 *  PfProDImgs
 *  pfProDScheduleds
 *  pfProImgs
 *  pfProPrices
 *  
 * </pre>
 * 
 * @author Ian Chen
 */
public class PfProGroup {
  
  /** The pf pro. */
  private PfPro pfPro;

  /** The pfpro ref. */
  private PfProRef pfProRef;
  
  /** The Pf pro airinfos. */
  private List<PfProAirinfo> pfProAirinfos;

  /** The pf pro allot. */
  private PfProAllot pfProAllot;

  /** The pf pro d imgs. */
  private List<PfProDImgs> pfProDImgs;

  /** The pf pro d scheduleds. */
  private List<PfProDScheduled> pfProDScheduleds;

  /** The pf pro imgs. */
  private List<PfProImgs> pfProImgs;

  /** The pf pro prices. */
  private List<PfProPrice> pfProPrices;

  /**
   * Gets the pf pro.
   * 
   * @return the pf pro
   */
  public PfPro getPfPro() {
    return pfPro;
  }

  /**
   * Sets the pf pro.
   * 
   * @param pfPro the new pf pro
   */
  public void setPfPro(PfPro pfPro) {
    this.pfPro = pfPro;
  }

  /**
   * Gets the pfpro ref.
   * 
   * @return the pfpro ref
   */
  public PfProRef getPfProRef() {
    return pfProRef;
  }

  /**
   * Sets the pfpro ref.
   * 
   * @param pfProRef the new pfpro ref
   */
  public void setPfProRef(PfProRef pfProRef) {
    this.pfProRef = pfProRef;
  }

  /**
   * Gets the pf pro airinfos.
   * 
   * @return the pf pro airinfos
   */
  public List<PfProAirinfo> getPfProAirinfos() {
    return pfProAirinfos;
  }

  /**
   * Sets the pf pro airinfos.
   * 
   * @param pfProAirinfos the new pf pro airinfos
   */
  public void setPfProAirinfos(List<PfProAirinfo> pfProAirinfos) {
    this.pfProAirinfos = pfProAirinfos;
  }

  /**
   * Gets the pf pro allot.
   * 
   * @return the pf pro allot
   */
  public PfProAllot getPfProAllot() {
    return pfProAllot;
  }

  /**
   * Sets the pf pro allot.
   * 
   * @param pfProAllot the new pf pro allot
   */
  public void setPfProAllot(PfProAllot pfProAllot) {
    this.pfProAllot = pfProAllot;
  }

  /**
   * Gets the pf pro d imgs.
   * 
   * @return the pf pro d imgs
   */
  public List<PfProDImgs> getPfProDImgs() {
    return pfProDImgs;
  }

  /**
   * Sets the pf pro d imgs.
   * 
   * @param pfProDImgs the new pf pro d imgs
   */
  public void setPfProDImgs(List<PfProDImgs> pfProDImgs) {
    this.pfProDImgs = pfProDImgs;
  }

  /**
   * Gets the pf pro d scheduleds.
   * 
   * @return the pf pro d scheduleds
   */
  public List<PfProDScheduled> getPfProDScheduleds() {
    return pfProDScheduleds;
  }

  /**
   * Sets the pf pro d scheduleds.
   * 
   * @param pfProDScheduleds the new pf pro d scheduleds
   */
  public void setPfProDScheduleds(List<PfProDScheduled> pfProDScheduleds) {
    this.pfProDScheduleds = pfProDScheduleds;
  }

  /**
   * Gets the pf pro imgs.
   * 
   * @return the pf pro imgs
   */
  public List<PfProImgs> getPfProImgs() {
    return pfProImgs;
  }

  /**
   * Sets the pf pro imgs.
   * 
   * @param pfProImgs the new pf pro imgs
   */
  public void setPfProImgs(List<PfProImgs> pfProImgs) {
    this.pfProImgs = pfProImgs;
  }

  /**
   * Gets the pf pro prices.
   * 
   * @return the pf pro prices
   */
  public List<PfProPrice> getPfProPrices() {
    return pfProPrices;
  }

  /**
   * Sets the pf pro prices.
   * 
   * @param pfProPrices the new pf pro prices
   */
  public void setPfProPrices(List<PfProPrice> pfProPrices) {
    this.pfProPrices = pfProPrices;
  }
}
