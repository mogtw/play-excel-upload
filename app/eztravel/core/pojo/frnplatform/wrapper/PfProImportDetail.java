/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.core.pojo.frnplatform.wrapper
 * @FileName: PfProImportDetail.java
 * @author:   Ian Chen
 * @date:     2014/8/26, 上午9:23:48
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.core.pojo.frnplatform.wrapper;

/**
 * <pre> PfProImportDetail </pre>
 * 
 * @author Ian Chen
 */
public class PfProImportDetail {
  
  /** The pf prod no. */
  private String pfProdNo;
  
  /** The prod content. */
  private PfProGroup prodContent;

  /**
   * Gets the pf prod no.
   * 
   * @return the pf prod no
   */
  public String getPfProdNo() {
    return pfProdNo;
  }

  /**
   * Sets the pf prod no.
   * 
   * @param pfProdNo the new pf prod no
   */
  public void setPfProdNo(String pfProdNo) {
    this.pfProdNo = pfProdNo;
  }

  /**
   * Gets the prod content.
   * 
   * @return the prod content
   */
  public PfProGroup getProdContent() {
    return prodContent;
  }

  /**
   * Sets the prod content.
   * 
   * @param prodContent the new prod content
   */
  public void setProdContent(PfProGroup prodContent) {
    this.prodContent = prodContent;
  }
}
