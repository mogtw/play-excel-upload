/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.core.pojo.frnplatform.wrapper
 * @FileName: PfProImport.java
 * @author:   Ian Chen
 * @date:     2014/8/25, 下午4:12:01
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.core.pojo.frnplatform.wrapper;

import java.util.List;

/**
 * <pre> PfProImport, 匯入接收的物件. </pre>
 *
 * @author Ian Chen
 */
public class PfProImport {

  /** The vend no. */
  private String vendNo;
  
  /** The total num. */
  private Integer totalNum;
  
  /** The creator. */
  private String creator;
  
  /** The b group no. */
  private Integer bGroupNo;
  
  /** The b group count. */
  private Short bGroupCount;
  
  /** The pf pro import details. */
  private List<PfProImportDetail> pfProImportDetails;
  
  /**
   * Instantiates a new pf pro import.
   * 
   * @param vendNo the vend no
   * @param totalNum the total num
   * @param creator the creator
   * @param bGroupNo the b group no
   * @param bGroupCount the b group count
   */
  public PfProImport(String vendNo, Integer totalNum, String creator, Integer bGroupNo, Short bGroupCount) {
    this.vendNo = vendNo;
    this.totalNum = totalNum;
    this.creator = creator;
    this.bGroupNo = bGroupNo;
    this.bGroupCount = bGroupCount;
  }

  /**
   * Gets the vend no.
   * 
   * @return the vend no
   */
  public String getVendNo() {
    return vendNo;
  }

  /**
   * Sets the vend no.
   * 
   * @param vendNo the new vend no
   */
  public void setVendNo(String vendNo) {
    this.vendNo = vendNo;
  }

  public Integer getTotalNum() {
    return totalNum;
  }

  public void setTotalNum(Integer totalNum) {
    this.totalNum = totalNum;
  }

  /**
   * Gets the creator.
   * 
   * @return the creator
   */
  public String getCreator() {
    return creator;
  }

  /**
   * Sets the creator.
   * 
   * @param creator the new creator
   */
  public void setCreator(String creator) {
    this.creator = creator;
  }

  /**
   * Gets the b group no.
   * 
   * @return the b group no
   */
  public Integer getbGroupNo() {
    return bGroupNo;
  }

  /**
   * Sets the b group no.
   * 
   * @param bGroupNo the new b group no
   */
  public void setbGroupNo(Integer bGroupNo) {
    this.bGroupNo = bGroupNo;
  }

  /**
   * Gets the b group count.
   * 
   * @return the b group count
   */
  public Short getbGroupCount() {
    return bGroupCount;
  }

  /**
   * Sets the b group count.
   * 
   * @param bGroupCount the new b group count
   */
  public void setbGroupCount(Short bGroupCount) {
    this.bGroupCount = bGroupCount;
  }

  /**
   * Gets the pf pro import details.
   * 
   * @return the pf pro import details
   */
  public List<PfProImportDetail> getPfProImportDetails() {
    return pfProImportDetails;
  }

  /**
   * Sets the pf pro import details.
   * 
   * @param pfProImportDetails the new pf pro import details
   */
  public void setPfProImportDetails(List<PfProImportDetail> pfProImportDetails) {
    this.pfProImportDetails = pfProImportDetails;
  }
}
