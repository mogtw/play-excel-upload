/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.core.pojo.frnplatform
 * @FileName: PfProRef.java
 * @author:   Ian Chen
 * @date:     2014/8/28, 下午 02:29:09
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.core.pojo.frnplatform;

import java.util.Date;

/**
 * <pre> PfProRef </pre>
 * 
 * @author Ian Chen
 */
public class PfProRef {
  
  /** The table prefix. */
  private String tablePrefix;
  
  /** The pf prod no. */
  private String pfProdNo;

  /** The vend no. */
  private String vendNo;

  /** The features. */
  private String features;

  /** The service fee. */
  private Integer serviceFee;

  /** The deposit. */
  private Integer deposit;

  /** The tour citys. */
  private String tourCitys;

  /** The tour citys nm. */
  private String tourCitysNm;

  /** The depart airport nm. */
  private String departAirportNm;

  /** The fee includes. */
  private String feeIncludes;

  /** The excluding items. */
  private String excludingItems;

  /** The tour h lights. */
  private String tourHLights;

  /** The sp plan. */
  private String spPlan;

  /** The g tour rule. */
  private String gTourRule;

  /** The man limit. */
  private Integer manLimit;

  /** The travel day. */
  private Integer travelDay;

  /** The travel day nm. */
  private String travelDayNm;

  /** The creator. */
  private String creator;

  /** The create dt. */
  private Date createDt;

  /** The moder. */
  private String moder;

  /** The mod dt. */
  private Date modDt;

  /** The taxes. */
  private Integer taxes;

  /**
   * Gets the table prefix.
   * 
   * @return the table prefix
   */
  public String getTablePrefix() {
    return tablePrefix;
  }

  /**
   * Sets the table prefix.
   * 
   * @param tablePrefix the new table prefix
   */
  public void setTablePrefix(String tablePrefix) {
    this.tablePrefix = tablePrefix;
  }

  /**
   * Gets the pf prod no.
   * 
   * @return the pf prod no
   */
  public String getPfProdNo() {
    return pfProdNo;
  }

  /**
   * Sets the pf prod no.
   * 
   * @param pfProdNo the new pf prod no
   */
  public void setPfProdNo(String pfProdNo) {
    this.pfProdNo = pfProdNo == null ? null : pfProdNo.trim();
  }

  /**
   * Gets the vend no.
   * 
   * @return the vend no
   */
  public String getVendNo() {
    return vendNo;
  }

  /**
   * Sets the vend no.
   * 
   * @param vendNo the new vend no
   */
  public void setVendNo(String vendNo) {
    this.vendNo = vendNo == null ? null : vendNo.trim();
  }

  /**
   * Gets the features.
   * 
   * @return the features
   */
  public String getFeatures() {
    return features;
  }

  /**
   * Sets the features.
   * 
   * @param features the new features
   */
  public void setFeatures(String features) {
    this.features = features == null ? null : features.trim();
  }

  /**
   * Gets the service fee.
   * 
   * @return the service fee
   */
  public Integer getServiceFee() {
    return serviceFee;
  }

  /**
   * Sets the service fee.
   * 
   * @param serviceFee the new service fee
   */
  public void setServiceFee(Integer serviceFee) {
    this.serviceFee = serviceFee;
  }

  /**
   * Gets the deposit.
   * 
   * @return the deposit
   */
  public Integer getDeposit() {
    return deposit;
  }

  /**
   * Sets the deposit.
   * 
   * @param deposit the new deposit
   */
  public void setDeposit(Integer deposit) {
    this.deposit = deposit;
  }

  /**
   * Gets the tour citys.
   * 
   * @return the tour citys
   */
  public String getTourCitys() {
    return tourCitys;
  }

  /**
   * Sets the tour citys.
   * 
   * @param tourCitys the new tour citys
   */
  public void setTourCitys(String tourCitys) {
    this.tourCitys = tourCitys == null ? null : tourCitys.trim();
  }

  /**
   * Gets the tour citys nm.
   * 
   * @return the tour citys nm
   */
  public String getTourCitysNm() {
    return tourCitysNm;
  }

  /**
   * Sets the tour citys nm.
   * 
   * @param tourCitysNm the new tour citys nm
   */
  public void setTourCitysNm(String tourCitysNm) {
    this.tourCitysNm = tourCitysNm == null ? null : tourCitysNm.trim();
  }

  /**
   * Gets the depart airport nm.
   * 
   * @return the depart airport nm
   */
  public String getDepartAirportNm() {
    return departAirportNm;
  }

  /**
   * Sets the depart airport nm.
   * 
   * @param departAirportNm the new depart airport nm
   */
  public void setDepartAirportNm(String departAirportNm) {
    this.departAirportNm = departAirportNm == null ? null : departAirportNm.trim();
  }

  /**
   * Gets the fee includes.
   * 
   * @return the fee includes
   */
  public String getFeeIncludes() {
    return feeIncludes;
  }

  /**
   * Sets the fee includes.
   * 
   * @param feeIncludes the new fee includes
   */
  public void setFeeIncludes(String feeIncludes) {
    this.feeIncludes = feeIncludes == null ? null : feeIncludes.trim();
  }

  /**
   * Gets the excluding items.
   * 
   * @return the excluding items
   */
  public String getExcludingItems() {
    return excludingItems;
  }

  /**
   * Sets the excluding items.
   * 
   * @param excludingItems the new excluding items
   */
  public void setExcludingItems(String excludingItems) {
    this.excludingItems = excludingItems == null ? null : excludingItems.trim();
  }

  /**
   * Gets the tour h lights.
   * 
   * @return the tour h lights
   */
  public String getTourHLights() {
    return tourHLights;
  }

  /**
   * Sets the tour h lights.
   * 
   * @param tourHLights the new tour h lights
   */
  public void setTourHLights(String tourHLights) {
    this.tourHLights = tourHLights == null ? null : tourHLights.trim();
  }

  /**
   * Gets the sp plan.
   * 
   * @return the sp plan
   */
  public String getSpPlan() {
    return spPlan;
  }

  /**
   * Sets the sp plan.
   * 
   * @param spPlan the new sp plan
   */
  public void setSpPlan(String spPlan) {
    this.spPlan = spPlan == null ? null : spPlan.trim();
  }

  /**
   * Gets the g tour rule.
   * 
   * @return the g tour rule
   */
  public String getgTourRule() {
    return gTourRule;
  }

  /**
   * Sets the g tour rule.
   * 
   * @param gTourRule the new g tour rule
   */
  public void setgTourRule(String gTourRule) {
    this.gTourRule = gTourRule == null ? null : gTourRule.trim();
  }

  /**
   * Gets the man limit.
   * 
   * @return the man limit
   */
  public Integer getManLimit() {
    return manLimit;
  }

  /**
   * Sets the man limit.
   * 
   * @param manLimit the new man limit
   */
  public void setManLimit(Integer manLimit) {
    this.manLimit = manLimit;
  }

  /**
   * Gets the travel day.
   * 
   * @return the travel day
   */
  public Integer getTravelDay() {
    return travelDay;
  }

  /**
   * Sets the travel day.
   * 
   * @param travelDay the new travel day
   */
  public void setTravelDay(Integer travelDay) {
    this.travelDay = travelDay;
  }

  /**
   * Gets the travel day nm.
   * 
   * @return the travel day nm
   */
  public String getTravelDayNm() {
    return travelDayNm;
  }

  /**
   * Sets the travel day nm.
   * 
   * @param travelDayNm the new travel day nm
   */
  public void setTravelDayNm(String travelDayNm) {
    this.travelDayNm = travelDayNm == null ? null : travelDayNm.trim();
  }

  /**
   * Gets the creator.
   * 
   * @return the creator
   */
  public String getCreator() {
    return creator;
  }

  /**
   * Sets the creator.
   * 
   * @param creator the new creator
   */
  public void setCreator(String creator) {
    this.creator = creator == null ? null : creator.trim();
  }

  /**
   * Gets the creates the dt.
   * 
   * @return the creates the dt
   */
  public Date getCreateDt() {
    return createDt;
  }

  /**
   * Sets the creates the dt.
   * 
   * @param createDt the new creates the dt
   */
  public void setCreateDt(Date createDt) {
    this.createDt = createDt;
  }

  /**
   * Gets the moder.
   * 
   * @return the moder
   */
  public String getModer() {
    return moder;
  }

  /**
   * Sets the moder.
   * 
   * @param moder the new moder
   */
  public void setModer(String moder) {
    this.moder = moder == null ? null : moder.trim();
  }

  /**
   * Gets the mod dt.
   * 
   * @return the mod dt
   */
  public Date getModDt() {
    return modDt;
  }

  /**
   * Sets the mod dt.
   * 
   * @param modDt the new mod dt
   */
  public void setModDt(Date modDt) {
    this.modDt = modDt;
  }

  /**
   * Gets the taxes.
   * 
   * @return the taxes
   */
  public Integer getTaxes() {
    return taxes;
  }

  /**
   * Sets the taxes.
   * 
   * @param taxes the new taxes
   */
  public void setTaxes(Integer taxes) {
    this.taxes = taxes;
  }
}