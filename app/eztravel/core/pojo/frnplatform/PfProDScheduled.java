/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.core.pojo.frnplatform
 * @FileName: PfProDScheduled.java
 * @author:   Ian Chen
 * @date:     2014/8/28, 下午 02:26:46
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.core.pojo.frnplatform;

import java.util.Date;

/**
 * <pre> PfProDScheduled </pre>
 * 
 * @author Ian Chen
 */
public class PfProDScheduled {

  /** The table prefix. */
  private String tablePrefix;
  
  /** The pf prod no. */
  private String pfProdNo;

  /** The vend no. */
  private String vendNo;

  /** The days. */
  private String days;

  /** The time seq. */
  private String timeSeq;

  /** The proc brief. */
  private String procBrief;

  /** The proc context. */
  private String procContext;

  /** The breakfast. */
  private String breakfast;

  /** The morning tea. */
  private String morningTea;

  /** The lunch. */
  private String lunch;

  /** The afternoon tea. */
  private String afternoonTea;

  /** The dinner. */
  private String dinner;

  /** The night snack. */
  private String nightSnack;

  /** The htl desc. */
  private String htlDesc;

  /** The htl desc url. */
  private String htlDescUrl;

  /** The creator. */
  private String creator;

  /** The create dt. */
  private Date createDt;

  /** The moder. */
  private String moder;

  /** The mod dt. */
  private Date modDt;

  /**
   * Gets the table prefix.
   * 
   * @return the table prefix
   */
  public String getTablePrefix() {
    return tablePrefix;
  }

  /**
   * Sets the table prefix.
   * 
   * @param tablePrefix the new table prefix
   */
  public void setTablePrefix(String tablePrefix) {
    this.tablePrefix = tablePrefix;
  }

  /**
   * Gets the pf prod no.
   * 
   * @return the pf prod no
   */
  public String getPfProdNo() {
    return pfProdNo;
  }

  /**
   * Sets the pf prod no.
   * 
   * @param pfProdNo the new pf prod no
   */
  public void setPfProdNo(String pfProdNo) {
    this.pfProdNo = pfProdNo == null ? null : pfProdNo.trim();
  }

  /**
   * Gets the vend no.
   * 
   * @return the vend no
   */
  public String getVendNo() {
    return vendNo;
  }

  /**
   * Sets the vend no.
   * 
   * @param vendNo the new vend no
   */
  public void setVendNo(String vendNo) {
    this.vendNo = vendNo == null ? null : vendNo.trim();
  }

  /**
   * Gets the days.
   * 
   * @return the days
   */
  public String getDays() {
    return days;
  }

  /**
   * Sets the days.
   * 
   * @param days the new days
   */
  public void setDays(String days) {
    this.days = days == null ? null : days.trim();
  }

  /**
   * Gets the time seq.
   * 
   * @return the time seq
   */
  public String getTimeSeq() {
    return timeSeq;
  }

  /**
   * Sets the time seq.
   * 
   * @param timeSeq the new time seq
   */
  public void setTimeSeq(String timeSeq) {
    this.timeSeq = timeSeq == null ? null : timeSeq.trim();
  }

  /**
   * Gets the proc brief.
   * 
   * @return the proc brief
   */
  public String getProcBrief() {
    return procBrief;
  }

  /**
   * Sets the proc brief.
   * 
   * @param procBrief the new proc brief
   */
  public void setProcBrief(String procBrief) {
    this.procBrief = procBrief == null ? null : procBrief.trim();
  }

  /**
   * Gets the proc context.
   * 
   * @return the proc context
   */
  public String getProcContext() {
    return procContext;
  }

  /**
   * Sets the proc context.
   * 
   * @param procContext the new proc context
   */
  public void setProcContext(String procContext) {
    this.procContext = procContext == null ? null : procContext.trim();
  }

  /**
   * Gets the breakfast.
   * 
   * @return the breakfast
   */
  public String getBreakfast() {
    return breakfast;
  }

  /**
   * Sets the breakfast.
   * 
   * @param breakfast the new breakfast
   */
  public void setBreakfast(String breakfast) {
    this.breakfast = breakfast == null ? null : breakfast.trim();
  }

  /**
   * Gets the morning tea.
   * 
   * @return the morning tea
   */
  public String getMorningTea() {
    return morningTea;
  }

  /**
   * Sets the morning tea.
   * 
   * @param morningTea the new morning tea
   */
  public void setMorningTea(String morningTea) {
    this.morningTea = morningTea == null ? null : morningTea.trim();
  }

  /**
   * Gets the lunch.
   * 
   * @return the lunch
   */
  public String getLunch() {
    return lunch;
  }

  /**
   * Sets the lunch.
   * 
   * @param lunch the new lunch
   */
  public void setLunch(String lunch) {
    this.lunch = lunch == null ? null : lunch.trim();
  }

  /**
   * Gets the afternoon tea.
   * 
   * @return the afternoon tea
   */
  public String getAfternoonTea() {
    return afternoonTea;
  }

  /**
   * Sets the afternoon tea.
   * 
   * @param afternoonTea the new afternoon tea
   */
  public void setAfternoonTea(String afternoonTea) {
    this.afternoonTea = afternoonTea == null ? null : afternoonTea.trim();
  }

  /**
   * Gets the dinner.
   * 
   * @return the dinner
   */
  public String getDinner() {
    return dinner;
  }

  /**
   * Sets the dinner.
   * 
   * @param dinner the new dinner
   */
  public void setDinner(String dinner) {
    this.dinner = dinner == null ? null : dinner.trim();
  }

  /**
   * Gets the night snack.
   * 
   * @return the night snack
   */
  public String getNightSnack() {
    return nightSnack;
  }

  /**
   * Sets the night snack.
   * 
   * @param nightSnack the new night snack
   */
  public void setNightSnack(String nightSnack) {
    this.nightSnack = nightSnack == null ? null : nightSnack.trim();
  }

  /**
   * Gets the htl desc.
   * 
   * @return the htl desc
   */
  public String getHtlDesc() {
    return htlDesc;
  }

  /**
   * Sets the htl desc.
   * 
   * @param htlDesc the new htl desc
   */
  public void setHtlDesc(String htlDesc) {
    this.htlDesc = htlDesc == null ? null : htlDesc.trim();
  }

  /**
   * Gets the htl desc url.
   * 
   * @return the htl desc url
   */
  public String getHtlDescUrl() {
    return htlDescUrl;
  }

  /**
   * Sets the htl desc url.
   * 
   * @param htlDescUrl the new htl desc url
   */
  public void setHtlDescUrl(String htlDescUrl) {
    this.htlDescUrl = htlDescUrl == null ? null : htlDescUrl.trim();
  }

  /**
   * Gets the creator.
   * 
   * @return the creator
   */
  public String getCreator() {
    return creator;
  }

  /**
   * Sets the creator.
   * 
   * @param creator the new creator
   */
  public void setCreator(String creator) {
    this.creator = creator == null ? null : creator.trim();
  }

  /**
   * Gets the creates the dt.
   * 
   * @return the creates the dt
   */
  public Date getCreateDt() {
    return createDt;
  }

  /**
   * Sets the creates the dt.
   * 
   * @param createDt the new creates the dt
   */
  public void setCreateDt(Date createDt) {
    this.createDt = createDt;
  }

  /**
   * Gets the moder.
   * 
   * @return the moder
   */
  public String getModer() {
    return moder;
  }

  /**
   * Sets the moder.
   * 
   * @param moder the new moder
   */
  public void setModer(String moder) {
    this.moder = moder == null ? null : moder.trim();
  }

  /**
   * Gets the mod dt.
   * 
   * @return the mod dt
   */
  public Date getModDt() {
    return modDt;
  }

  /**
   * Sets the mod dt.
   * 
   * @param modDt the new mod dt
   */
  public void setModDt(Date modDt) {
    this.modDt = modDt;
  }
}