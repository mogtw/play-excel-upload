/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.core.pojo.frnplatform
 * @FileName: PfOrdProdExtend.java
 * @author:   Ian Chen
 * @date:     2014/9/23, 上午 10:37:46
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.core.pojo.frnplatform;

import java.util.Date;

/**
 * <pre> PfOrdProdExtend </pre>
 * 
 * @author Ian Chen
 */
public class PfOrdProdExtend {

  /** The order no. */
  private String orderNo;

  /** The cust seqno. */
  private Short custSeqno;

  /** The prod seqno. */
  private Short prodSeqno;

  /** The extend type. */
  private String extendType;

  /** The value1. */
  private String value1;

  /** The value2. */
  private String value2;

  /** The value3. */
  private String value3;

  /** The creator. */
  private String creator;

  /** The create dt. */
  private Date createDt;

  /** The moder. */
  private String moder;

  /** The mod dt. */
  private Date modDt;

  /**
   * Gets the order no.
   * 
   * @return the order no
   */
  public String getOrderNo() {
    return orderNo;
  }

  /**
   * Sets the order no.
   * 
   * @param orderNo the new order no
   */
  public void setOrderNo(String orderNo) {
    this.orderNo = orderNo == null ? null : orderNo.trim();
  }

  /**
   * Gets the cust seqno.
   * 
   * @return the cust seqno
   */
  public Short getCustSeqno() {
    return custSeqno;
  }

  /**
   * Sets the cust seqno.
   * 
   * @param custSeqno the new cust seqno
   */
  public void setCustSeqno(Short custSeqno) {
    this.custSeqno = custSeqno;
  }

  /**
   * Gets the prod seqno.
   * 
   * @return the prod seqno
   */
  public Short getProdSeqno() {
    return prodSeqno;
  }

  /**
   * Sets the prod seqno.
   * 
   * @param prodSeqno the new prod seqno
   */
  public void setProdSeqno(Short prodSeqno) {
    this.prodSeqno = prodSeqno;
  }

  /**
   * Gets the extend type.
   * 
   * @return the extend type
   */
  public String getExtendType() {
    return extendType;
  }

  /**
   * Sets the extend type.
   * 
   * @param extendType the new extend type
   */
  public void setExtendType(String extendType) {
    this.extendType = extendType == null ? null : extendType.trim();
  }

  /**
   * Gets the value1.
   * 
   * @return the value1
   */
  public String getValue1() {
    return value1;
  }

  /**
   * Sets the value1.
   * 
   * @param value1 the new value1
   */
  public void setValue1(String value1) {
    this.value1 = value1 == null ? null : value1.trim();
  }

  /**
   * Gets the value2.
   * 
   * @return the value2
   */
  public String getValue2() {
    return value2;
  }

  /**
   * Sets the value2.
   * 
   * @param value2 the new value2
   */
  public void setValue2(String value2) {
    this.value2 = value2 == null ? null : value2.trim();
  }

  /**
   * Gets the value3.
   * 
   * @return the value3
   */
  public String getValue3() {
    return value3;
  }

  /**
   * Sets the value3.
   * 
   * @param value3 the new value3
   */
  public void setValue3(String value3) {
    this.value3 = value3 == null ? null : value3.trim();
  }

  /**
   * Gets the creator.
   * 
   * @return the creator
   */
  public String getCreator() {
    return creator;
  }

  /**
   * Sets the creator.
   * 
   * @param creator the new creator
   */
  public void setCreator(String creator) {
    this.creator = creator == null ? null : creator.trim();
  }

  /**
   * Gets the creates the dt.
   * 
   * @return the creates the dt
   */
  public Date getCreateDt() {
    return createDt;
  }

  /**
   * Sets the creates the dt.
   * 
   * @param createDt the new creates the dt
   */
  public void setCreateDt(Date createDt) {
    this.createDt = createDt;
  }

  /**
   * Gets the moder.
   * 
   * @return the moder
   */
  public String getModer() {
    return moder;
  }

  /**
   * Sets the moder.
   * 
   * @param moder the new moder
   */
  public void setModer(String moder) {
    this.moder = moder == null ? null : moder.trim();
  }

  /**
   * Gets the mod dt.
   * 
   * @return the mod dt
   */
  public Date getModDt() {
    return modDt;
  }

  /**
   * Sets the mod dt.
   * 
   * @param modDt the new mod dt
   */
  public void setModDt(Date modDt) {
    this.modDt = modDt;
  }
}