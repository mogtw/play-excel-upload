/**
 * EZTRAVEL CONFIDENTIAL
 * 
 * @Package: eztravel.rest.enums.common
 * @FileName: FareType.java
 * @author: tonywang
 * @date: 2013/12/15, 下午 12:47:16
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.rest.enums.common;

import javax.xml.bind.annotation.XmlEnum;

/**
 * The Enum FareType.
 * 
 * <pre>
 * 
 * </pre>
 */
@XmlEnum
public enum FareType {

  /** The B2 b. */
  B2B("同業優惠"),
  /** The B2 c. */
  B2C(""),
  /** The B2 e. */
  B2E("企業優惠");

  /** The name. */
  final private String name;

  /**
   * Instantiates a new fare type.
   * 
   * @param name the name
   */
  private FareType(String name) {
    this.name = name;
  }

  /**
   * Gets the name.
   * 
   * @return the name
   */
  public String getName() {
    return name;
  }


}
