/**
 * EZTRAVEL CONFIDENTIAL
 * 
 * @Package: eztravel.rest.util.common
 * @FileName: StringUtils.java
 * @author: 003084
 * @date: 2013/12/17, 下午 05:11:45
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.rest.util.common;

import java.text.DecimalFormat;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * The Class StringUtils.
 * 
 * <pre>
 * 
 * </pre>
 */
public class StringUtils {

  /** The Constant logger. */
  private static final Logger logger = LoggerFactory.getLogger(StringUtils.class);

  /**
   * Instantiates a new string utils.
   */
  public StringUtils() {}

  /**
   * 將字串先做urlencoder處理,再轉base64.
   * 
   * @param str the str
   * @return 加密後的資料
   */
  public String ezEncoder(String str) {
    str = org.apache.commons.lang.StringUtils.defaultString(str);
    try {
      return new String(org.apache.commons.codec.binary.Base64.encodeBase64(java.net.URLEncoder
          .encode(str, "utf-8").getBytes()));
    } catch (Exception e) {
      logger.error("Util_string_ezEncoder Exception:" + e.getMessage(), e);
      return "";
    }
  }

  /**
   * 將字串以base64還原再 urldecoder處理.
   * 
   * @param str the str
   * @return 未加密的資料
   */
  public String ezDecoder(String str) {
    str = org.apache.commons.lang.StringUtils.defaultString(str);
    try {
      return java.net.URLDecoder.decode(
          new String(org.apache.commons.codec.binary.Base64.decodeBase64(str.getBytes())), "utf-8");
    } catch (Exception e) {
      logger.error("Util_string_ezDecoder Exception:" + e.getMessage(), e);
      return "";
    }
  }

  /**
   * 將數值前面補零並轉成字串.
   * 
   * @param mun the mun
   * @param n the n
   * @return 補零後的字串
   */
  public static String to_String(int mun, int n) {
    return to_String(String.valueOf(mun), n);
  }

  /**
   * 將數值前面補零並轉成字串.
   * 
   * @param mun the mun
   * @param n the n
   * @return 補零後的字串
   */
  public static String to_String(long mun, int n) {
    return to_String(String.valueOf(mun), n);
  }

  /**
   * 將數值前面補零並轉成字串.
   * 
   * @param mun the mun
   * @param n the n
   * @return 補零後的字串
   */
  public static String to_String(String mun, int n) {
    n = n - 1;
    int mun_len = mun.length() - 1;
    for (int i = 0; i < (n - mun_len); i++) {
      mun = "0" + mun;
    }
    return (mun);
  }

  /**
   * To_ string2.
   * 
   * @param mun the mun
   * @param n the n
   * @return the string
   */
  public static String to_String2(String mun, int n) {
    n = n - 1;
    int mun_len = mun.length() - 1;
    for (int i = 0; i < n - mun_len; i++)
      mun = mun.concat(" ");

    return mun;
  }

  /**
   * 將數值字串格式化.
   * 
   * @param num the num
   * @param sFormat the s format
   * @return 數值字串格式化
   */
  public static String numSwitch(String num, String sFormat) {
    if (!fp_isNull(num).equals("")) {
      try {
        // NumberFormat nf = NumberFormat.getInstance(Locale.TAIWAN);
        DecimalFormat df = new DecimalFormat(sFormat);

        return df.format(Long.parseLong(num));
      } catch (java.lang.NumberFormatException e) {
        // e.printStackTrace();
        return num;

      }
    }
    return "0";
  }

  /**
   * 將數值字串格式化.
   * 
   * @param num the num
   * @param sFormat the s format
   * @return 數值字串格式化
   */
  public static String numSwitch(long num, String sFormat) {
    try {
      // NumberFormat nf = NumberFormat.getInstance(Locale.TAIWAN);
      DecimalFormat df = new DecimalFormat(sFormat);
      return df.format(num);
    } catch (Exception e) {
      logger.error("Util_string_numSwitch Exception:" + e.getMessage(), e);
    }
    return "0";
  }

  /**
   * 將物件 null 或 空白 轉成 空字串.
   * 
   * @param s1 the s1
   * @return 空字串或原字串
   */
  public static String fp_isNull(Object s1) {
    int count = 0;
    String ss = "";
    if (s1 == null)
      return ss;
    else if (s1.equals(""))
      return ss;
    else if (s1.equals("null")) return ss;

    // 判斷字串是否全部都是空白
    String s = s1.toString();
    for (int i = 0; i < s.length(); i++) {
      if (s.charAt(i) == ' ') count++;
    }
    // 若全部為空白傳回一空字串
    if (count == s.length())
      return ss;
    else
      return s.trim();

  }

  /**
   * Fp ord_is null.
   * 
   * @param s1 the s1
   * @return the string
   */
  public static String fpOrd_isNull(Object s1) {
    int count = 0;
    String ss = " ";
    if (s1 == null)
      return ss;
    else if (s1.equals(""))
      return ss;
    else if (s1.equals("null")) return ss;

    // 判斷字串是否全部都是空白
    String s = s1.toString();
    for (int i = 0; i < s.length(); i++) {
      if (s.charAt(i) == ' ') count++;
    }
    // 若全部為空白傳回一空字串
    if (count == s.length())
      return ss;
    else
      return s.trim();

  }

  /**
   * 判斷是否違反系統商品編號格式.
   * 
   * @param prodNo the prod no
   * @return true 不符合,false 符合
   */
  public static boolean checkProdNo(String prodNo) {
    Pattern p = Pattern.compile("[A-Z]{3}\\w{1}\\d{9}");
    Matcher m = p.matcher(prodNo);
    return !m.matches();
  }

  /**
   * 判斷是否違反系統日期格式.
   * 
   * @param date the date
   * @return true 不符合,false 符合
   */
  public static boolean checkDate(String date) {
    Pattern p = Pattern.compile("20\\d{2}(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])");
    Matcher m = p.matcher(date);
    return !m.matches();
  }

  /**
   * 判斷是否違反系統訂單編號格式.
   * 
   * @param orderNo the order no
   * @return true 不符合,false 符合
   */
  public static boolean checkOrderNo(String orderNo) {
    Pattern p = Pattern.compile("ORD\\d{10}");
    Matcher m = p.matcher(orderNo);
    return !m.matches();
  }

  /**
   * 判斷是否違反系統房型編號格式.
   * 
   * @param roomTypeNo the room type no
   * @return true 不符合,false 符合
   */
  public static boolean checkRoomTypeNo(String roomTypeNo) {
    Pattern p = Pattern.compile("\\d{3}");
    Matcher m = p.matcher(roomTypeNo);
    return !m.matches();
  }


  /**
   * 將字串物件 null 或 空白 轉成 空字串.
   * 
   * @param s1 the s1
   * @return 空字串或原字串
   */
  public static String fp_isNull(String s1) {
    int count = 0;
    String ss = "";
    if (s1 == null)
      return ss;
    else if (s1.equals(""))
      return ss;
    else if (s1.equals("null")) return ss;

    // 判斷字串是否全部都是空白
    String s = s1.toString();
    for (int i = 0; i < s.length(); i++) {
      if (s.charAt(i) == ' ') count++;
    }
    // 若全部為空白傳回一空字串
    if (count == s.length())
      return ss;
    else
      return s.trim();
  }

  /**
   * 將字串物件 null 或 空白 轉成 s2.
   * 
   * @param s1 字串
   * @param s2 空字串時回傳的字串
   * @return the string
   */
  public static String fp_isNull(String s1, String s2) {
    String result = StringUtils.fp_isNull(s1);
    if (result.equals("")) {
      return s2;
    } else {
      return result;
    }
  }

  /**
   * 將字串重新編碼 Defalul utf-8.
   * 
   * @param s the s
   * @return 字串重編碼結果
   */
  public static String getString(String s) {
    s = fp_isNull(s);
    if (s.length() != 0) {
      return s;
    }
    return ""; // getString(s,"utf-8");
  }

  /**
   * 將字串重新編碼 Defalul utf-8.
   * 
   * @param s the s
   * @param encode the encode
   * @return 字串重編碼結果
   */
  public static String getString(String s, String encode) {
    s = fp_isNull(s);
    if (s.length() != 0) {
      try {
        return new String(s.getBytes("iso-8859-1"), encode);
      } catch (Exception e) {
        logger.error("Util_string_getString Exception:" + e.getMessage(), e);
      }
    }
    return "";
  }

  /**
   * 將字串物件以某符號來切割成陣列.
   * 
   * @param s the s
   * @param v the v
   * @return 字串陣列
   */
  public static String[] split(String s, String v) {
    StringTokenizer parser = new StringTokenizer(s, v);
    String[] str = new String[parser.countTokens()];
    try {
      int i = 0;
      while (parser.hasMoreElements()) {
        str[i++] = (String) parser.nextToken();
      }
    } catch (NoSuchElementException e) {
      return null;
    }
    return str;
  }

  /**
   * 將字串物件加密.
   * 
   * @param buf the buf
   * @return 加密後結果
   */
  public static String Encode(byte buf[]) {
    StringBuffer sb = new StringBuffer(2 * buf.length);
    for (int i = 0; i < buf.length; i++) {
      int h = (buf[i] & 0xf0) >> 4;
      int l = (buf[i] & 0x0f);
      sb.append(new Character((char) ((h > 9) ? 'a' + h - 10 : '0' + h)));
      sb.append(new Character((char) ((l > 9) ? 'a' + l - 10 : '0' + l)));
    }
    return sb.toString();
  }

  /**
   * 將字串物件解密.
   * 
   * @param s the s
   * @return the string
   * @return　字串解密後結果
   */
  public static String Decode(String s) {
    StringBuffer sb = new StringBuffer();
    for (int i = 0; i < s.length() / 2; i++) {
      int digit1 = s.charAt(i * 2);
      int digit2 = s.charAt(i * 2 + 1);
      if ((digit1 >= '0') && (digit1 <= '9'))
        digit1 -= '0';
      else if ((digit1 >= 'a') && (digit1 <= 'f')) digit1 -= 'a' - 10;
      if ((digit2 >= '0') && (digit2 <= '9'))
        digit2 -= '0';
      else if ((digit2 >= 'a') && (digit2 <= 'f')) digit2 -= 'a' - 10;
      sb.append((char) ((digit1 << 4) + digit2));
    }
    return sb.toString();
  }

  /**
   * 將物件 /n 轉成 <BR>
   * .
   * 
   * @param temp 需轉換的字串
   * @return /n 轉成 <BR>
   */
  public static String transMultiline(String temp) {

    // int i;
    int pos = 0;

    if (temp == null) {
      temp = "";
    } else {
      StringBuffer sb = new StringBuffer(fp_isNull(temp));
      pos = temp.indexOf("\n");

      while (pos != -1) {
        if (sb.length() > pos) {
          sb.replace(pos, pos + 1, "<br>");
        }

        temp = sb.toString();
        pos = temp.indexOf("\n");
      }
    }

    return temp;
  }


  /**
   * Date switch.
   * 
   * @param aDate the a date
   * @param aFormat the a format
   * @return the string
   */
  public static String dateSwitch(String aDate, String aFormat) {
    String tDate = "";
    if (aFormat.equals("date") && (aDate.length() == 8)) {
      tDate = aDate.substring(0, 4) + "/" + aDate.substring(4, 6) + "/" + aDate.substring(6, 8);
    } else if (aFormat.equals("time") && (aDate.length() == 4)) {
      tDate = aDate.substring(0, 2) + ":" + aDate.substring(2, 4);
    } else if (aFormat.equals("times") && (aDate.length() == 6)) {
      tDate = aDate.substring(0, 2) + ":" + aDate.substring(2, 4) + ":" + aDate.substring(4, 6);
    } else if (aFormat.equals("datetime") && aDate.length() >= 12) {
      tDate =
          dateSwitch(aDate.substring(0, 8), "date") + " "
              + dateSwitch(aDate.substring(8, 12), "time");
    } else if (aFormat.equals("datetimes") && aDate.length() >= 14) {
      tDate =
          dateSwitch(aDate.substring(0, 8), "date") + " "
              + dateSwitch(aDate.substring(8, 14), "times");
    }
    return tDate;
  }

  /**
   * 去除月及日數值<10 不顯示0.
   * 
   * @param aDate the a date
   * @param aFormat the a format
   * @return the string
   */
  public static String noZeroDateSwitch(String aDate, String aFormat) {
    String tDate = "";
    if (aFormat.equals("date") && (aDate.length() == 8)) {
      tDate =
          aDate.substring(0, 4) + "/" + String.valueOf(Integer.parseInt(aDate.substring(4, 6)))
              + "/" + String.valueOf(Integer.parseInt(aDate.substring(6, 8)));
    } else if (aFormat.equals("chn_date") && (aDate.length() == 8)) {
      tDate =
          aDate.substring(0, 4) + "年" + String.valueOf(Integer.parseInt(aDate.substring(4, 6)))
              + "月" + String.valueOf(Integer.parseInt(aDate.substring(6, 8))) + "日";
    } else if (aFormat.equals("date") && (aDate.length() == 4)) {
      tDate =
          String.valueOf(Integer.parseInt(aDate.substring(0, 2))) + "/"
              + String.valueOf(Integer.parseInt(aDate.substring(2, 4)));
    } else if (aFormat.equals("time") && (aDate.length() == 4)) {
      tDate = aDate.substring(0, 2) + ":" + aDate.substring(2, 4);
    } else if (aFormat.equals("times") && (aDate.length() == 6)) {
      tDate = aDate.substring(0, 2) + ":" + aDate.substring(2, 4) + ":" + aDate.substring(4, 6);
    } else if (aFormat.equals("datetime") && aDate.length() >= 12) {
      tDate =
          noZeroDateSwitch(aDate.substring(0, 8), "date") + " "
              + dateSwitch(aDate.substring(8, 12), "time");
    } else if (aFormat.equals("datetimes") && aDate.length() >= 14) {
      tDate =
          noZeroDateSwitch(aDate.substring(0, 8), "date") + " "
              + dateSwitch(aDate.substring(8, 14), "times");
    }
    return tDate;
  }

  /**
   * Replace str.
   * 
   * @param line the line
   * @param oldString the old string
   * @param newString the new string
   * @return the string
   */
  public static String replaceStr(String line, String oldString, String newString) {
    int index = 0;
    while ((index = line.indexOf(oldString, index)) >= 0) {
      line = line.substring(0, index) + newString + line.substring(index + oldString.length());
      index += newString.length();
    }
    return line;
  }

  /**
   * Replace strignore case.
   * 
   * @param line the line
   * @param oldString the old string
   * @param newString the new string
   * @return the string
   */
  public static String replaceStrignoreCase(String line, String oldString, String newString) {
    int index = 0;
    while ((index = line.toUpperCase().indexOf(oldString.toUpperCase(), index)) >= 0) {
      line = line.substring(0, index) + newString + line.substring(index + oldString.length());
      index += newString.length();
    }
    return line;
  }

  /**
   * Strip html tags.
   * 
   * @param message the message
   * @return the string
   */
  public static String stripHTMLTags(String message) {
    StringBuffer returnMessage = new StringBuffer(message);
    int startPosition = message.indexOf("<");
    for (int endPosition = message.indexOf(">"); startPosition != -1; endPosition =
        returnMessage.toString().indexOf(">")) {
      if (startPosition > endPosition) {
        returnMessage = returnMessage.delete(startPosition, returnMessage.toString().length() + 1);
      } else {
        returnMessage = returnMessage.delete(startPosition, endPosition + 1);
      }
      startPosition = returnMessage.toString().indexOf("<");
    }

    return returnMessage.toString();
  }

  // 20060803 新增刪除E-Mail問號
  /**
   * Delete question mark.
   * 
   * @param sb the sb
   * @return the string
   */
  public static String deleteQuestionMark(StringBuffer sb) {
    int pos = 0;

    for (int i = 0; i < sb.length(); i = pos + 1) {
      pos = sb.indexOf("?", i);

      if (pos != -1) {
        if (pos != 0) {
          if (sb.charAt(pos - 1) == '\u0020' || sb.charAt(pos - 1) == '\n'
              || sb.charAt(pos - 1) == '\t' || sb.charAt(pos - 1) == '?') {
            sb = sb.replace(pos, pos + 1, " "); // test
          } else if (sb.charAt(pos - 1) == '0') {
            sb = sb.replace(pos, pos + 1, " ~ "); // 新增全形符號判斷修正
          }
        } else {
          sb = sb.replace(pos, pos + 1, " "); // Test
        }
      } else {
        break;
      }
    }
    return sb.toString();
  }

  // 20060807 新增刪除行動電話的'-'
  /**
   * Delete dash.
   * 
   * @param str the str
   * @return the string
   */
  public static String deleteDash(String str) {
    if (str.indexOf("-") != -1) {
      String temp[] = str.split("-");
      str = "";
      for (int i = 0; i < temp.length; i++) {
        str += temp[i];
      }
    }
    return str;
  }

  /**
   * 依Byte數來截取字串
   * 
   * <pre>基數從1開始<br>
   * 要是beginIdex的位置是全形字,但開始截截取的只有全形的一半,則會從該全形字開始取,避免破字<br>
   * 要是endIdex的位置是全形字,且結束截取的位置只有全形的一半,則取到前一個Byte,避免破字<br>
   * eg: subStringb("鴿1068中01文abc", 2, 5) = "106"<br>
   * eg: subStringb("鴿1068中01文abc", 7, 11) = "中01"</pre>
   * 
   * .
   * 
   * @param str 來源字串
   * @param beginIndex 開始Byte(從1開始)
   * @param endIndex 結束的Byte
   * @return 截取完的字串
   * @author Kuo-Chien Hsieh 20090406
   */
  public static String subStringb(String str, int beginIndex, int endIndex) {
    StringBuffer result = new StringBuffer();
    if (str != null) {
      int byteNum = 0;
      char[] val = str.toCharArray();
      for (int i = 0; i < val.length; i++) {
        // 已到截取的endIndex,不用再取
        if (i >= endIndex) {
          break;
        }
        if (StringUtils.isFullWord(val[i])) {
          byteNum += 2;
        } else {
          byteNum++;
        }
        if (byteNum >= beginIndex && byteNum < endIndex) {
          result.append(String.valueOf(val[i]));
        }
      }
    }
    return result.toString();
  }

  /**
   * 依UTF8 Byte數來截取字串(全形佔3Bytes)
   * 
   * <pre>基數從1開始<br>
   * 要是beginIdex的位置是全形字,但開始截截取的只有全形的一半,則會從該全形字開始取,避免破字<br>
   * 要是endIdex的位置是全形字,且結束截取的位置只有全形的一半,則取到前一個Byte,避免破字<br>
   * eg: subStringb_utf8("鴿1068中01文abc", 2, 5) = "鴿10"<br>
   * eg: subStringb_utf8("鴿1068中01文abc", 7, 14) = "8中0"</pre>
   * 
   * .
   * 
   * @param str 來源字串
   * @param beginIndex 開始Byte(從1開始)
   * @param endIndex 結束的Byte
   * @return 截取完的字串
   * @author Kuo-Chien Hsieh 20090406
   */
  public static String subStringb_utf8(String str, int beginIndex, int endIndex) {
    StringBuffer result = new StringBuffer();
    if (str != null) {
      int byteNum = 0;
      char[] val = str.toCharArray();
      for (int i = 0; i < val.length; i++) {
        // 已到截取的endIndex,不用再取
        if (i >= endIndex) {
          break;
        }
        if (StringUtils.isFullWord(val[i])) {
          byteNum += 3;
        } else {
          byteNum++;
        }
        if (byteNum >= beginIndex && byteNum <= endIndex) {
          result.append(String.valueOf(val[i]));
        }
      }
    }
    return result.toString();
  }

  /**
   * 取得字串所佔的Byte數.
   * 
   * @param str 來源字串
   * @return Byte數
   * @author Kuo-Chien Hsieh 20090406
   */
  public static int lengthb(String str) {
    int length = 0;
    if (str != null) {
      char[] val = str.toCharArray();
      length = val.length;
    }
    return length;
  }

  /**
   * 取得UTF-8字串所佔的Byte數
   * 
   * <pre>UTF-8,全形佔3個Bytes,半形1個Byte</pre>
   * 
   * .
   * 
   * @param str 來源字串
   * @return UTF-8所佔的Byte數
   * @author Kuo-Chien Hsieh 20090406
   */
  public static int lengthb_utf8(String str) {
    int bytes = 0;
    if (str != null) {
      char[] val = str.toCharArray();
      for (int i = 0; i < val.length; i++) {
        if (StringUtils.isFullWord(val[i])) {
          bytes += 3;
        } else {
          bytes++;
        }
      }
    }
    return bytes;
  }

  /**
   * 取得字串中的全形字
   * 
   * <pre>eg:"123我Abc=0880　是%&*中文" ==> "我　是中文"</pre>
   * 
   * .
   * 
   * @param str 來源字串
   * @return 字串中所有的全形字
   * @author Kuo-Chien Hsieh 20090406
   */
  public static String getFullWord(String str) {
    StringBuffer result = new StringBuffer();
    if (str != null) {
      char[] val = str.toCharArray();
      // int utflen = 0;
      for (int i = 0; i < val.length; i++) {
        int c = val[i];
        // 半形
        if ((c >= 0x0001) && (c <= 0x007F)) {
          // utflen++;
          // 全形
        } else if (c > 0x07FF) {
          result.append(String.valueOf((char) c));
        }
      }
    }
    return result.toString();
  }

  /**
   * 判斷該字元是否為全形字.
   * 
   * @param chr 檢核字元
   * @return true:全形,false:半形
   * @author Kuo-Chien Hsieh 20090406
   */
  public static boolean isFullWord(char chr) {
    // 半形
    if ((chr >= 0x0001) && (chr <= 0x007F)) {
      return false;
      // 全形
    } else if (chr > 0x07FF) {
      return true;
    }
    return false;
  }

  /**
   * TRIM指定字串
   * 
   * <pre>trim("abacdef,", ",") return "abcdef"</pre>
   * 
   * .
   * 
   * @param str 來源字串
   * @param trimStr 欲TRIM掉的字串
   * @return String
   * @throws Exception the exception
   * @author Kuo-Chien Hsieh
   */
  public static String trim(String str, String trimStr) throws Exception {
    String result = str;
    try {
      if (str != null && !str.equals("") && trimStr != null && !trimStr.equals("")) {
        StringBuffer strBuffer = new StringBuffer(str);
        if (strBuffer != null && strBuffer.length() > 0) {
          while (strBuffer.lastIndexOf(trimStr) >= 0
              && strBuffer.lastIndexOf(trimStr) == (strBuffer.length() - trimStr.length())) {
            strBuffer.delete(strBuffer.lastIndexOf(trimStr), strBuffer.length());
          }
          result = strBuffer.toString();
        }
      }
    } catch (Exception e) {
      throw e;
    }
    return result;
  }

  /**
   * 將傳入的字串根據所需長度填滿.
   * 
   * @param str 原始字串
   * @param iLen 填滿後的字串長度
   * @param cChar 填補字元
   * @param bLeft 由左填補旗標
   * @return the string
   * @retun 填補後的新字串
   * @author Kuo-Chien Hsieh
   */
  public static String fill(String str, int iLen, char cChar, boolean bLeft) {
    StringBuffer strBuffer = new StringBuffer(str);
    int iStringLength = 0;
    iStringLength = StringUtils.lengthb_utf8(str);
    while (iStringLength < iLen) {
      if (bLeft) {
        strBuffer.insert(0, cChar);
      } else {
        strBuffer.append(cChar);
      }
      iStringLength++;
    }
    return strBuffer.toString();
  }

  /**
   * 取出中英混合字串,一個中文以2個byte計算 ex: getSubstring("12中文",0,4,false) return "中文".
   * 
   * @param value the value
   * @param startIndex the start index
   * @param leng 擷取長度(bytes)
   * @param fromRight 是否從右側開始擷取
   * @return SubString
   * @author Marty Hsieh 20040322
   */
  public static String substring(String value, int startIndex, int leng, boolean fromRight) {
    char[] charArray = value.toCharArray();
    int charLeng = charArray.length;
    if (startIndex >= charLeng) return "";

    StringBuffer returnValue = new StringBuffer();
    int returnLen = 0;
    int index = 0;
    int x = 0;
    int i = 0;
    if (fromRight) x = charLeng - 1;

    while (index < charLeng) {
      char ch = charArray[x];
      try {
        i = String.valueOf(ch).getBytes("Big5").length;
      } catch (java.io.UnsupportedEncodingException e) {
        logger.error("UnsupportedEncodingException : Big5 ," + e.getMessage(), e);
        i = String.valueOf(ch).getBytes().length;
      }
      index++;

      if (index >= (startIndex + 1)) {
        if (returnLen + i <= leng) {
          if (fromRight) {
            returnValue.insert(0, String.valueOf(ch));
          } else {
            returnValue.append(String.valueOf(ch));
          }
          returnLen += i;
        } else {
          break;
        }
      }
      if (fromRight)
        x--;
      else
        x++;
    } // end while

    // 因為中文擷取的關係，若最後leng < returnLen則需在此補滿指定的長度
    if (leng != returnLen) {
      for (int j = 0; j < (leng - returnLen); j++) {
        if (fromRight) {
          returnValue.insert(0, " ");
        } else {
          returnValue.append(" ");
        }
      }
    }
    return returnValue.toString();
  }

  /**
   * 全形轉半形.
   * 
   * @param str the str
   * @return 全形字串
   */
  public static String toSingleByte(String str) {
    String result = "";
    if (StringUtils.fp_isNull(str).equals("")) return "";

    try {
      for (int i = 0; i < str.length(); i++) {
        if (str.codePointAt(i) == 12288) {
          result += " ";
        } else if (str.codePointAt(i) == 8245) {
          result += "`";
        } else if (str.codePointAt(i) == 65087) {
          result += "^";
        } else {
          if (str.codePointAt(i) > 65280 && str.codePointAt(i) < 65375) {
            result += new Character((char) (str.codePointAt(i) - 65248)).toString();
          } else {
            result += new Character((char) str.codePointAt(i));
          }
        }
      }
    } catch (Exception e) {
      logger.error("toSingleByte:" + str + "," + e.getMessage(), e);
      result = str;
    }

    return result;
  }

  /**
   * 跟據傳入的字串跟型態決定要遮罩的字元 ex: maskStr("A123456789","id") return "A*****6789" maskStr("王大明","chName")
   * return "王*明".
   * 
   * @param value the value
   * @param type the type
   * @return String
   * @author Allen 20121126
   */
  public static String maskStr(String value, String type) {

    int[] maskIndex = null; // 要遮罩的字index

    if ("chName".equals(type)) {
      // 判斷字串裡面是否有中文
      if (value.getBytes().length == value.length()) { // 無中文
        maskIndex = null;
      } else { // 有中文
        value = value.replaceAll(" ", "");
        maskIndex = new int[] {1};
      }
    } else if ("birthday".equals(type)) {
      maskIndex = new int[] {2, 3, 6};
    } else if ("id".equals(type)) {
      maskIndex = new int[] {1, 2, 3, 4, 5};
    } else if ("callPhone".equals(type)) {
      maskIndex = new int[] {4, 6, 8};
    } else if ("telPhone".equals(type)) {
      maskIndex = new int[] {4, 6, 8};
    } else if ("birthday2".equals(type)) {
      maskIndex = new int[] {2, 3, 5};
    }
    StringBuffer str = new StringBuffer(value);

    if ("chName、birthday、id、callPhone、telPhone、birthday2".indexOf(type) != -1) {
      if (maskIndex != null) {
        for (int i = 0; i < maskIndex.length; i++) {
          if (maskIndex[i] < str.length()) str.setCharAt(maskIndex[i], '*');
        }
      }
    } else if ("password".indexOf(type) != -1) {
      if (str != null) {
        for (int i = 0; i < str.length(); i++) {
          if (i % 2 == 0) {
            str.setCharAt(i, '*');
          }
        }
      }
    } else if ("name".indexOf(type) != -1) {
      if (str != null) {
        for (int i = 0; i < str.length(); i++) {
          if (i % 2 != 0) {
            str.setCharAt(i, '*');
          }
        }
      }
    }
    return str.toString();
  }

  /**
   * 跟據傳入的字串串列跟型態決定要遮罩的字元[資料為串接時].
   * 
   * @param value the value
   * @param type the type
   * @param splitChar the split char
   * @return String
   * @author Allen 20121126
   */
  public static String maskStrArray(String value, String type, String splitChar) {

    String[] subStr = value.split("、");
    StringBuffer retStr = new StringBuffer();
    for (int i = 0; i < subStr.length; i++) {
      if (i == 0) {
        retStr.append(maskStr(subStr[i], type));
      } else {
        retStr.append(splitChar + maskStr(subStr[i], type));
      }
    }

    return retStr.toString();
  }



  /**
   * 回覆特定文字，以遮蔽個資或隱私.
   * 
   * @return String
   * @author Joe 20130124
   */
  public static String contDesc() {

    String retStr =
        "基於個資保護，恕不顯示您填寫於此處的內容，請<a href='/ezec/order/check_order2004.jsp' target='_blank'>登入會員中心</a>後查看【我的訂單】";

    return retStr;
  }

  /**
   * 多選文字 修整成 sql 可執行的字串，ex：「1,2,3」-->「'1','2','3'」.
   * 
   * @param oldString the old string
   * @param oldSplit 原字串分隔符號
   * @param newSplit 新字串分隔符號
   * @param addString 前後加字
   * @return the string
   */
  public static String toFormatString(String oldString, String oldSplit, String newSplit,
      String addString) {
    String newString = "";
    StringBuffer sb = new StringBuffer();
    String[] oldStr = StringUtils.split(oldString, oldSplit);

    for (int i = 0; i < oldStr.length; i++) {
      sb.append(newSplit + addString + oldStr[i] + addString);
    }
    newString = sb.toString();
    if (newString.length() > newSplit.length()) newString = newString.substring(newSplit.length());

    return newString;
  }
  
  /**
   * 依數字長度將左邊補滿0.
   * 
   * @param number the number
   * @param length the length
   * @return 回傳左邊補0的字串結果
   */
  public static String addZeroToLeft(int number, int length) {
    return String.format("%0" + length + "d", number);
  }

}
