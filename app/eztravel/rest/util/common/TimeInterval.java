/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.rest.util.common
 * @FileName: TimeInterval.java
 * @author:   tonywang
 * @date:     2013/12/15, 下午 12:47:15
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.rest.util.common;

import java.util.Calendar;

/**
 * The Class TimeInterval.
 * 
 * <pre>
 * 
 * </pre>
 */
public class TimeInterval {

  /** The start. */
  private Calendar start;
  
  /** The end. */
  private Calendar end;

  /**
   * Setup.
   * 
   * @param start the start
   * @param end the end
   */
  private void setup(Calendar start, Calendar end) {
    this.setStart(start);
    this.setEnd(end);
  }

  /**
   * Instantiates a new time interval.
   * 
   * @param start the start
   * @param end the end
   */
  public TimeInterval(Calendar start, Calendar end) {
    setup(start, end);
  }

  /**
   * Gets the start.
   * 
   * @return the start
   */
  public Calendar getStart() {
    return start;
  }

  /**
   * Sets the start.
   * 
   * @param start the new start
   */
  public void setStart(Calendar start) {
    this.start = start;
  }

  /**
   * Gets the end.
   * 
   * @return the end
   */
  public Calendar getEnd() {
    return end;
  }

  /**
   * Sets the end.
   * 
   * @param end the new end
   */
  public void setEnd(Calendar end) {
    this.end = end;
  }

  // TODO override toString

}
