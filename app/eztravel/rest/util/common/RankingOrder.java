/**
 * EZTRAVEL CONFIDENTIAL
 * 
 * @Package: eztravel.rest.util.common
 * @FileName: RankingOrder.java
 * @author: tonywang
 * @date: 2013/12/15, 下午 12:47:14
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.rest.util.common;

import javax.xml.bind.annotation.XmlEnum;

/**
 * The Enum RankingOrder.
 * 
 * <pre>
 * 
 * </pre>
 */
@XmlEnum
public enum RankingOrder {

  /** The desc. */
  DESC("descending"),
  /** The asc. */
  ASC("ascending");

  /** The name. */
  private final String name;

  /**
   * Instantiates a new ranking order.
   * 
   * @param name the name
   */
  private RankingOrder(String name) {
    this.name = name;
  }

  /**
   * Gets the name.
   * 
   * @return the name
   */
  public String getName() {
    return name;
  }

}
