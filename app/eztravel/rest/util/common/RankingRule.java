/**
 * EZTRAVEL CONFIDENTIAL
 * 
 * @Package: eztravel.rest.util.common
 * @FileName: RankingRule.java
 * @author: tonywang
 * @date: 2013/12/15, 下午 12:47:15
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.rest.util.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * The Class RankingRule.
 * 
 * <pre>
 * 
 * </pre>
 * 
 * @param <T> the generic type
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class RankingRule<T extends Enum<T>> {

  /** The order. */
  private RankingOrder order;

  /** The criterion. */
  @XmlAnyElement(lax = true)
  private T criterion;

  /**
   * Gets the order.
   * 
   * @return the order
   */
  public RankingOrder getOrder() {
    return order;
  }

  /**
   * Sets the order.
   * 
   * @param order the new order
   */
  public void setOrder(RankingOrder order) {
    this.order = order;
  }

  /**
   * Gets the criterion.
   * 
   * @return the criterion
   */
  public T getCriterion() {
    return criterion;
  }

  /**
   * Sets the criterion.
   * 
   * @param criterion the new criterion
   */
  public void setCriterion(T criterion) {
    this.criterion = criterion;
  }


}
