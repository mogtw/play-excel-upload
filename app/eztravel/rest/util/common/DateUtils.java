/**
 * EZTRAVEL CONFIDENTIAL
 * 
 * @Package: eztravel.rest.util.common
 * @FileName: DateUtils.java
 * @author: joeko
 * @date: 2014/5/7, 上午 09:57:19
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.rest.util.common;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

/**
 * The Class DateUtils.
 * 
 * <pre>
 * 
 * </pre>
 */
public class DateUtils {

  /** The Constant DEFAULT_DATE_FORMAT. */
  private static final String DEFAULT_DATE_FORMAT = "yyyy/MM/dd";

  /** The Constant DEFAULT_TIME_FORMAT. */
  @SuppressWarnings("unused")
  private static final String DEFAULT_TIME_FORMAT = "HH:mm:ss";

  /** The Constant TIME_ZONE. */
  private static final String TIME_ZONE = "Asia/Taipei";


  /**
   * Instantiates a new date utils.
   */
  public DateUtils() {}

  /**
   * Gets the date.
   * 
   * @param year the year
   * @param month the month
   * @param day the day
   * @return the date
   */
  private static Date getDate(int year, int month, int day) {
    Calendar cal = Calendar.getInstance(TimeZone.getTimeZone(TIME_ZONE));
    cal.set(year, month, day);
    return cal.getTime();
  }

  /**
   * Gets the date.
   * 
   * @param year the year
   * @param month the month
   * @param day the day
   * @param hour the hour
   * @param min the min
   * @param sec the sec
   * @return the date
   */
  private static Date getDate(int year, int month, int day, int hour, int min, int sec) {
    Calendar cal = Calendar.getInstance(TimeZone.getTimeZone(TIME_ZONE));
    cal.set(year, month, day, hour, min, sec);
    return cal.getTime();
  }

  /**
   * Gets the calendar.
   * 
   * @param year the year
   * @param month the month
   * @param day the day
   * @return the calendar
   */
  private static Calendar getCalendar(int year, int month, int day) {
    Calendar cal = Calendar.getInstance(TimeZone.getTimeZone(TIME_ZONE));
    cal.set(year, month, day);
    return cal;
  }

  /**
   * 函式名稱：getToday(format); 功　　能：抓取今天的日期 傳　　入：欲輸出的格式 傳　　回：日期.
   * 
   * @param dFormat the d format
   * @return the today
   */
  public static String getToday(String dFormat) {
    if (dFormat.length() == 0 || dFormat == null) {
      dFormat = "yyyy/MM/dd HH:mm:ss";
    }
    SimpleDateFormat formatter = new SimpleDateFormat(dFormat);
    java.util.Date currentTime_1 = new java.util.Date();
    return formatter.format(currentTime_1);
  }

  /**
   * 函式名稱：getToday(); 功　　能：抓取今天的日期 傳　　入：無傳入值 傳　　回：日期.
   * 
   * @return the today
   */
  public static String getToday() {
    return getToday("yyyyMMddHHmmss");
  }

  /**
   * 函式名稱：getDateTransform(String date); 功　　能：取得日期格式轉換 傳　　入：YYYYMMDD日期字串 傳　　回：YYYY/M/D日期字串.
   * 
   * @param date the date
   * @return the date transform
   */
  public static String getDateTransform(String date) { // 20060704新增
    if (date != null && !date.equals("")) {
      int year = Integer.parseInt(date.substring(0, 4));
      int month = Integer.parseInt(date.substring(4, 6)) - 1;
      int day = Integer.parseInt(date.substring(6, 8));

      Date d = DateUtils.getDate(year, month, day);

      SimpleDateFormat formatter = new SimpleDateFormat("yyyy/M/d");
      return formatter.format(d);
    } else {
      return "";
    }
  }

  /**
   * 函式名稱：getDateTransform2(String date); 功　　能：取得日期格式轉換 傳　　入：YYYY/M/D日期字串 傳　　回：YYYY/MM/DD日期字串.
   * 
   * @param date the date
   * @return the date transform2
   */
  public static String getDateTransform2(String date) { // 20060822新增
    if (date != null && !date.equals("")) {
      String split_date[] = date.split("/");

      int year = Integer.parseInt(split_date[0]);
      int month = Integer.parseInt(split_date[1]) - 1;
      int day = Integer.parseInt(split_date[2]);

      Date d = DateUtils.getDate(year, month, day);
      SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
      return formatter.format(d);
    } else {
      return "";
    }
  }

  /**
   * 函式名稱：getDateTransform3(String date); 功　　能：取得日期格式轉換 傳　　入：YYYY/MM/DD日期字串 傳　　回：YYYY/M/D日期字串.
   * 
   * @param date the date
   * @return the date transform3
   */
  public static String getDateTransform3(String date) { // 20060831新增
    if (date != null && !date.equals("")) {
      String split_date[] = date.split("/");

      int year = Integer.parseInt(split_date[0]);
      int month = Integer.parseInt(split_date[1]) - 1;
      int day = Integer.parseInt(split_date[2]);

      Date d = DateUtils.getDate(year, month, day);
      SimpleDateFormat formatter = new SimpleDateFormat("yyyy/M/d");
      return formatter.format(d);
    } else {
      return "";
    }
  }

  /**
   * 函式名稱：getDateTransform4(String date); 功　　能：取得日期格式轉換 傳　　入：YYYY/M/D 或 YYYY/MM/DD 日期字串
   * 傳　　回：YYYYMMDD日期字串.
   * 
   * @param date the date
   * @return the date transform4
   */
  public static String getDateTransform4(String date) { // 20060831新增
    if (date != null && !date.equals("")) {
      String split_date[] = date.split("/");

      int year = Integer.parseInt(split_date[0]);
      int month = Integer.parseInt(split_date[1]) - 1;
      int day = Integer.parseInt(split_date[2]);

      Date d = DateUtils.getDate(year, month, day);
      SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
      return formatter.format(d);
    } else {
      return "";
    }
  }

  /**
   * 函式名稱：getDateTransform5(String date); 功　　能：取得日期格式轉換 傳　　入：YYYYMMDD日期字串 傳　　回：M/D.
   * 
   * @param date the date
   * @return the date transform5
   */
  public static String getDateTransform5(String date) { // 20060831新增
    if (date != null && !date.equals("")) {
      int year = Integer.parseInt(date.substring(0, 4));
      int month = Integer.parseInt(date.substring(4, 6)) - 1;
      int day = Integer.parseInt(date.substring(6, 8));

      Date d = DateUtils.getDate(year, month, day);
      SimpleDateFormat formatter = new SimpleDateFormat("M/d");
      return formatter.format(d);
    } else {
      return "";
    }
  }

  /**
   * 函式名稱：getDateTransform5_1(String date); 功　　能：取得日期格式轉換 傳　　入：YYYYMMDD日期字串 傳　　回：MM/DD.
   * 
   * @param date the date
   * @return the date transform5_1
   */
  public static String getDateTransform5_1(String date) { // 20060831新增
    if (date != null && !date.equals("")) {
      int year = Integer.parseInt(date.substring(0, 4));
      int month = Integer.parseInt(date.substring(4, 6)) - 1;
      int day = Integer.parseInt(date.substring(6, 8));

      Date d = DateUtils.getDate(year, month, day);
      SimpleDateFormat formatter = new SimpleDateFormat("MM/dd");
      return formatter.format(d);
    } else {
      return "";
    }
  }

  /**
   * 函式名稱：getDateTransform6_1(String date); 功　　能：取得日期格式轉換 傳　　入：YYYYMMDD日期字串 傳　　回：YYYY/MM/DD (日期字串).
   * 
   * @param date the date
   * @return the date transform6_1
   */
  public static String getDateTransform6_1(String date) { // 20061113新增
    if (date != null && !date.equals("")) {
      int year = Integer.parseInt(date.substring(0, 4));
      int month = Integer.parseInt(date.substring(4, 6));
      int day = Integer.parseInt(date.substring(6, 8));

      Date d = DateUtils.getDate(year, month, day);
      SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
      return formatter.format(d);
    } else {
      return "";
    }
  }

  /**
   * 函式名稱：getDateTransform6(String date); 功　　能：取得日期格式轉換 傳　　入：YYYYMMDD日期字串 傳　　回：YYYY年M月D日 (日期字串).
   * 
   * @param date the date
   * @return the date transform6
   */
  public static String getDateTransform6(String date) { // 20061113新增
    NumberFormat formatter = new DecimalFormat("00");
    if (date != null && !date.equals("")) {
      int year = Integer.parseInt(date.substring(0, 4));
      int month = Integer.parseInt(date.substring(4, 6));
      int day = Integer.parseInt(date.substring(6, 8));

      return year + " 年 " + formatter.format(month) + " 月 " + formatter.format(day) + " 日";
    } else {
      return "";
    }
  }

  /**
   * 函式名稱：getDateTransform7(String date); 功　　能：取得日期格式轉換 傳　　入：YYYYMMDD日期字串 傳　　回：YYYY/MM/DD日期字串.
   * 
   * @param date the date
   * @return the date transform7
   */
  public static String getDateTransform7(String date) { // 20080723新增
    if (date != null && !date.equals("")) {
      int year = Integer.parseInt(date.substring(0, 4));
      int month = Integer.parseInt(date.substring(4, 6)) - 1;
      int day = Integer.parseInt(date.substring(6, 8));

      Date d = DateUtils.getDate(year, month, day);
      SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
      return formatter.format(d);
    } else {
      return "";
    }
  }



  /**
   * 函式名稱：getDateTransform8(String date); 功　　能：取得日期時間格式轉換 傳　　入：YYYYMMDDhhmm日期字串 傳　　回：YYYY年M月D日hh時mm分
   * (日期字串).
   * 
   * @param date the date
   * @return the date transform8
   */
  public static String getDateTransform8(String date) { // 20080828新增
    NumberFormat formatter = new DecimalFormat("00");
    if (date != null && !date.equals("")) {
      if (date.length() == 8) {
        return getDateTransform6(date);
      } else {
        int year = Integer.parseInt(date.substring(0, 4));
        int month = Integer.parseInt(date.substring(4, 6));
        int day = Integer.parseInt(date.substring(6, 8));
        int hh = Integer.parseInt(date.substring(8, 10));
        int mm = Integer.parseInt(date.substring(10, 12));

        return year + "年" + formatter.format(month) + "月" + formatter.format(day) + "日"
            + formatter.format(hh) + "時" + formatter.format(mm) + "分";
      }
    } else {
      return "";
    }
  }


  /**
   * Gets the date transform8_1.
   * 
   * @param date the date
   * @return the date transform8_1
   */
  public static String getDateTransform8_1(String date) { // 20080828新增
    NumberFormat formatter = new DecimalFormat("00");
    if (date != null && !date.equals("")) {
      if (date.length() == 8) {
        return getDateTransform6_1(date);
      } else {
        int year = Integer.parseInt(date.substring(0, 4));
        int month = Integer.parseInt(date.substring(4, 6));
        int day = Integer.parseInt(date.substring(6, 8));
        int hh = Integer.parseInt(date.substring(8, 10));
        int mm = Integer.parseInt(date.substring(10, 12));

        return year + "/" + formatter.format(month) + "/" + formatter.format(day) + " "
            + formatter.format(hh) + ":" + formatter.format(mm) + "";
      }
    } else {
      return "";
    }
  }

  /**
   * 函式名稱：getDateTransform9(String date); 功　　能：取得日期時間格式轉換 傳　　入：YYYYMMDDhhmmss日期字串
   * 傳　　回：YYYY年M月D日hh時mm分ss秒 (日期字串).
   * 
   * @param date the date
   * @return the date transform9
   */
  public static String getDateTransform9(String date) { // 20080828新增
    if (date != null && !date.equals("")) {
      int year = Integer.parseInt(date.substring(0, 4));
      int month = Integer.parseInt(date.substring(4, 6));
      int day = Integer.parseInt(date.substring(6, 8));
      int hh = Integer.parseInt(date.substring(8, 10));
      int mm = Integer.parseInt(date.substring(10, 12));
      int ss = Integer.parseInt(date.substring(12, 14));
      return year + "年 " + month + "月 " + day + "日 " + hh + "時 " + mm + "分 " + ss + "秒";
    } else {
      return "";
    }
  }



  /**
   * 函式名稱：getTodayAddSeconds(int,string); 功　　能：取得今天加 d 秒的日期 傳　　入：天數及格式 傳　　回：日期.
   * 
   * @param d the d
   * @param dFormat the d format
   * @return the today add seconds
   */
  public static String getTodayAddSeconds(int d, String dFormat) {
    Date date = new Date();
    return getTodayAddSeconds(date, d, dFormat);
  }

  /**
   * 函式名稱：getTodayAddSeconds(string,int,string); 功　　能：取得某一天加 d 秒的日期 傳　　入：某一天的字串和天數及格式 傳　　回：日期.
   * 
   * @param startday the startday
   * @param d the d
   * @param dFormat the d format
   * @return the today add seconds
   */
  public static String getTodayAddSeconds(String startday, int d, String dFormat) {
    Date date =
        DateUtils.getDate(Integer.parseInt(startday.substring(0, 4)),
            Integer.parseInt(startday.substring(4, 6)) - 1,
            Integer.parseInt(startday.substring(6, 8)));
    return getTodayAddSeconds(date, d, dFormat);
  }

  /**
   * 函式名稱：getTodayAddSeconds(Date,int,string); 功　　能：取得某一天加 d 秒的日期 傳　　入：某一天的Date物件和天數及格式 傳　　回：日期.
   * 
   * @param startday the startday
   * @param d the d
   * @param dFormat the d format
   * @return the today add seconds
   */
  public static String getTodayAddSeconds(Date startday, int d, String dFormat) {
    SimpleDateFormat formatter = new SimpleDateFormat(dFormat);
    Calendar calendar = new GregorianCalendar(1900, 1, 1);
    calendar.setTime(startday);
    calendar.setTime(DateUtils.getDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
        calendar.get(Calendar.DATE), calendar.get(Calendar.HOUR_OF_DAY),
        calendar.get(Calendar.MINUTE), calendar.get(Calendar.SECOND) + d));
    return formatter.format(calendar.getTime());
  }



  /**
   * 函式名稱：getTodayAddMinutes(int,string); 功　　能：取得今天加 d 分的日期 傳　　入：天數及格式 傳　　回：日期.
   * 
   * @param d the d
   * @param dFormat the d format
   * @return the today add minutes
   */
  public static String getTodayAddMinutes(int d, String dFormat) {
    Date date = new Date();
    return getTodayAddMinutes(date, d, dFormat);
  }

  /**
   * 函式名稱：getTodayAddMinutes(string,int,string); 功　　能：取得某一天加 d 分的日期 傳　　入：某一天的字串和天數及格式 傳　　回：日期.
   * 
   * @param startday the startday
   * @param d the d
   * @param dFormat the d format
   * @return the today add minutes
   */
  public static String getTodayAddMinutes(String startday, int d, String dFormat) {
    Date date =
        DateUtils.getDate(Integer.parseInt(startday.substring(0, 4)),
            Integer.parseInt(startday.substring(4, 6)) - 1,
            Integer.parseInt(startday.substring(6, 8)));
    return getTodayAddMinutes(date, d, dFormat);
  }

  /**
   * 函式名稱：getTodayAddMinutes(Date,int,string); 功　　能：取得某一天加 d 分的日期 傳　　入：某一天的Date物件和天數及格式 傳　　回：日期.
   * 
   * @param startday the startday
   * @param d the d
   * @param dFormat the d format
   * @return the today add minutes
   */
  public static String getTodayAddMinutes(Date startday, int d, String dFormat) {
    SimpleDateFormat formatter = new SimpleDateFormat(dFormat);
    Calendar calendar = new GregorianCalendar(1900, 1, 1);
    calendar.setTime(startday);
    calendar.setTime(DateUtils.getDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
        calendar.get(Calendar.DATE), calendar.get(Calendar.HOUR_OF_DAY),
        calendar.get(Calendar.MINUTE) + d, calendar.get(Calendar.SECOND)));
    return formatter.format(calendar.getTime());
  }



  /**
   * 函式名稱：getTodayAddDays(int,string); 功　　能：取得今天加 d 天的日期 傳　　入：天數及格式 傳　　回：日期.
   * 
   * @param d the d
   * @param dFormat the d format
   * @return the today add days
   */
  public static String getTodayAddDays(int d, String dFormat) {
    Date date = new Date();
    return getTodayAddDays(date, d, dFormat);
  }

  /**
   * 函式名稱：getTodayAddDays(string,int,string); 功　　能：取得某一天加 d 天的日期 傳　　入：某一天的字串和天數及格式 傳　　回：日期.
   * 
   * @param startday the startday
   * @param d the d
   * @param dFormat the d format
   * @return the today add days
   */
  public static String getTodayAddDays(String startday, int d, String dFormat) {
    Date date =
        DateUtils.getDate(Integer.parseInt(startday.substring(0, 4)),
            Integer.parseInt(startday.substring(4, 6)) - 1,
            Integer.parseInt(startday.substring(6, 8)));
    return getTodayAddDays(date, d, dFormat);
  }

  /**
   * 函式名稱：getTodayAddDays(Date,int,string); 功　　能：取得某一天加 d 天的日期 傳　　入：某一天的Date物件和天數及格式 傳　　回：日期.
   * 
   * @param startday the startday
   * @param d the d
   * @param dFormat the d format
   * @return the today add days
   */
  public static String getTodayAddDays(Date startday, int d, String dFormat) {
    SimpleDateFormat formatter = new SimpleDateFormat(dFormat);
    Calendar calendar = new GregorianCalendar(1900, 1, 1);
    calendar.setTime(startday);
    calendar.setTime(DateUtils.getDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
        calendar.get(Calendar.DATE) + d));
    return formatter.format(calendar.getTime());
  }

  /**
   * Gets the today add months.
   * 
   * @param startday the startday
   * @param d the d
   * @param dFormat the d format
   * @return the today add months
   */
  public static String getTodayAddMonths(String startday, int d, String dFormat) {
    SimpleDateFormat formatter = new SimpleDateFormat(dFormat);
    Calendar calendar = new GregorianCalendar(1900, 1, 1);
    calendar.set(Integer.parseInt(startday.substring(0, 4)),
        Integer.parseInt(startday.substring(4, 6)), Integer.parseInt(startday.substring(6, 8)));
    calendar.setTime(DateUtils.getDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH)
        + d - 1, calendar.get(Calendar.DATE)));

    return formatter.format(calendar.getTime());
  }

  /**
   * 函式名稱：getDiffDays(startday,endday); 功　　能：取得日期相差天數 傳　　入：出發日及回程日 傳　　回：天數.
   * 
   * @param startday the startday
   * @param endday the endday
   * @return the diff days
   */
  public static int getDiffDays(String startday, String endday) {
    java.util.Date _dStartDay =
        DateUtils.getDate(Integer.parseInt(startday.substring(0, 4)),
            Integer.parseInt(startday.substring(4, 6)) - 1,
            Integer.parseInt(startday.substring(6, 8)));
    java.util.Date _dEndDay =
        DateUtils.getDate(Integer.parseInt(endday.substring(0, 4)),
            Integer.parseInt(endday.substring(4, 6)) - 1, Integer.parseInt(endday.substring(6, 8)));
    int _iDiffDays =
        Integer.parseInt("" + ((_dEndDay.getTime() - _dStartDay.getTime()) / 86400000));
    return _iDiffDays;
  }

  /**
   * 函式名稱：getDiffHours(startday,endday); 功　　能：取得日期相差小時數 傳　　入：出發日及回程日 傳　　回：小時數.
   * 
   * @param startday the startday
   * @param endday the endday
   * @return the diff hours
   */
  public static int getDiffHours(String startday, String endday) {
    java.util.Date _dStartDay =
        DateUtils.getDate(Integer.parseInt(startday.substring(0, 4)),
            Integer.parseInt(startday.substring(4, 6)) - 1,
            Integer.parseInt(startday.substring(6, 8)));
    java.util.Date _dEndDay =
        DateUtils.getDate(Integer.parseInt(endday.substring(0, 4)),
            Integer.parseInt(endday.substring(4, 6)) - 1, Integer.parseInt(endday.substring(6, 8)));
    int _iDiffHours =
        Integer.parseInt("" + ((_dEndDay.getTime() - _dStartDay.getTime()) / 3600000));
    return _iDiffHours;
  }

  /**
   * 函式名稱：isValid(date); 功　　能：判斷是否為明確的日期格式 YYYYMMDD 傳　　入：日期 傳　　回：true/false.
   * 
   * @param date the date
   * @return true, if is valid
   */
  public static boolean isValid(String date) {
    if (date == null || date.length() != 8) return false;

    try {
      @SuppressWarnings("unused")
      java.util.Date d =
          DateUtils.getDate(Integer.parseInt(date.substring(0, 4)),
              Integer.parseInt(date.substring(4, 6)) - 1, Integer.parseInt(date.substring(6, 8)));
    } catch (Exception ex) {
      return false;
    }
    return true;
  }

  /**
   * 函式名稱：getDiffHours_new(String startday,String endDay); 日期需為年月日時分秒 功　　能：取得日期相差小時數(算至時分秒)
   * 傳　　入：起始日及結止日 傳　　回：小時數(double 值).
   * 
   * @param startday the startday
   * @param endday the endday
   * @return the diff hours_new
   */
  public static double getDiffHours_new(String startday, String endday) {
    java.util.Date _dStartDay =
        DateUtils.getDate(Integer.parseInt(startday.substring(0, 4)),
            Integer.parseInt(startday.substring(4, 6)) - 1,
            Integer.parseInt(startday.substring(6, 8)));
    java.util.Date _dEndDay =
        DateUtils.getDate(Integer.parseInt(endday.substring(0, 4)),
            Integer.parseInt(endday.substring(4, 6)) - 1, Integer.parseInt(endday.substring(6, 8)));
    int _iDiffDays =
        Integer.parseInt("" + ((_dEndDay.getTime() - _dStartDay.getTime()) / 86400000));
    String hh1 = startday.substring(8, 10);
    String hh2 = endday.substring(8, 10);
    String min1 = startday.substring(10, 12);
    String min2 = endday.substring(10, 12);
    String sec1 = startday.substring(12, 14);
    String sec2 = endday.substring(12, 14);
    double t2 =
        (_iDiffDays * 24) * 60 * 60 + (Double.parseDouble(hh2) * 60 * 60)
            + Double.parseDouble(min2) * 60 + Double.parseDouble(sec2);
    double t1 =
        Double.parseDouble(hh1) * 60 * 60 + Double.parseDouble(min1) * 60
            + Double.parseDouble(sec1);
    double diff = (t2 - t1) / 3600;
    return diff;
  }



  /**
   * 函式名稱：getWeek(year,month,date); 功　　能：取得星期別 傳　　入：年、月、日 傳　　回：星期別.
   * 
   * @param year the year
   * @param month the month
   * @param date the date
   * @return the week
   */
  public static String getWeek(int year, int month, int date) {
    int str_week = 0;
    final String week[] = {"日", "一", "二", "三", "四", "五", "六"};
    Calendar cal = Calendar.getInstance();
    cal.set(year, month, date);
    str_week = cal.get(Calendar.DAY_OF_WEEK) - 1;
    return week[str_week];
  }



  /**
   * 函式名稱：getDateToWeek(year,month,date); 功　　能：取得日期+星期別的格式 傳　　入： 西元年月日 傳　　回：YYYY/MM/DD 星期別.
   * 
   * @param date the date
   * @param dFormat the d format
   * @return the date to week
   */
  public static String getDateToWeek(String date, String dFormat) {
    String dateWeekInfo = "";
    final String week[] = {"日", "一", "二", "三", "四", "五", "六"};

    if (date != null && !date.equals("")) {
      int year = Integer.parseInt(date.substring(0, 4));
      int month = Integer.parseInt(date.substring(4, 6)) - 1;
      int day = Integer.parseInt(date.substring(6, 8));

      // 處理 XXXX年XX月XX日的格式
      Date d = DateUtils.getDate(year, month, day);
      Calendar cal = DateUtils.getCalendar(year, month, day);
      SimpleDateFormat formatter = new SimpleDateFormat(dFormat);

      dateWeekInfo = formatter.format(d) + "  星期" + week[cal.get(Calendar.DAY_OF_WEEK)];
    } else {
      return "";
    }
    return dateWeekInfo;
  } // end getDateToWeek



  /**
   * The main method.
   * 
   * @param args the arguments
   */
  public static void main(String args[]) {
    String dateWeekInfo = getDateToWeek("20121211", "yyyy年 MM 月 dd日");
    System.out.println("dateWeekInfo = " + dateWeekInfo);
  } // end main



  /**
   * 日期轉字串
   * 
   * <pre>
   * date2Str(new Date(), "yyyy/MM/dd") return "2010/01/01"
   * date2Str(new Date(), "yyyy-MM-dd") return "2010-01-01"
   * date2Str(new Date(), "yyyy/MM") return "2010/01"
   * </pre>
   * 
   * .
   * 
   * @author Marty Hsieh
   * @param date Date object
   * @param dateFormat 指定之日期格式
   * @return 已格式化的日期字串
   */
  public static String date2Str(java.util.Date date, String dateFormat) {
    if (dateFormat == null) {
      dateFormat = DEFAULT_DATE_FORMAT;
    }
    SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
    return sdf.format(date);
  }

  /**
   * 字串轉日期
   * 
   * <pre>
   * return Util_date.str2Date(date, DEFAULT_DATE_FORMAT);
   * </pre>
   * 
   * @author Marty Hsieh
   * @param date 西元日期字串
   * @return Date object
   * @throws ParseException the parse exception
   */
  public static java.util.Date str2Date(String date) throws ParseException {
    return DateUtils.str2Date(date, DEFAULT_DATE_FORMAT);
  }

  /**
   * 字串轉日期
   * 
   * <pre>
   * str2Date("yyyy/MM/dd","2003/01/01")
   * </pre>
   * 
   * .
   * 
   * @author Marty Hsieh
   * @param date 西元日期字串
   * @param formatStr the format str
   * @return Date object
   * @throws ParseException the parse exception
   */
  public static java.util.Date str2Date(String date, String formatStr) throws ParseException {
    if ((formatStr == null) || formatStr.trim().equalsIgnoreCase("")) {
      formatStr = DEFAULT_DATE_FORMAT;
    }
    SimpleDateFormat sdf = new SimpleDateFormat(formatStr);
    return sdf.parse(date);
  }


  /**
   * Gets the adds the months date.
   * 
   * @param startday the startday
   * @param d the d
   * @param dFormat the d format
   * @return the adds the months date
   */
  public static String getAddMonthsDate(String startday, int d, String dFormat) {
    SimpleDateFormat formatter = new SimpleDateFormat(dFormat);
    Calendar calendar =
        new GregorianCalendar(Integer.parseInt(startday.substring(0, 4)), Integer.parseInt(startday
            .substring(4, 6)) - 1, Integer.parseInt(startday.substring(6, 8)));

    // 判斷startDate是否為該月最後一天
    boolean flag =
        calendar.getActualMaximum(Calendar.DAY_OF_MONTH) == calendar.get(Calendar.DAY_OF_MONTH);

    calendar.add(Calendar.MONTH, d);
    boolean isLastday =
        calendar.getActualMaximum(Calendar.DAY_OF_MONTH) == calendar.get(Calendar.DAY_OF_MONTH);
    // 判斷startDate是最後一天，但當前日期不是該月最後一天時，將當前日期調整為該月最後一天
    if (flag && !isLastday) {
      calendar.set(Calendar.DAY_OF_MONTH, calendar.getMaximum(Calendar.DAY_OF_MONTH));
    }

    return formatter.format(calendar.getTime());
  }

}
