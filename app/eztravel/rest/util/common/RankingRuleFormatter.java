/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.rest.util.common
 * @FileName: RankingRuleFormatter.java
 * @author:   tonywang
 * @date:     2013/12/15, 下午 12:47:13
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.rest.util.common;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.format.Formatter;

/**
 * The Class RankingRuleFormatter.
 * 
 * <pre>
 * 
 * </pre>
 * 
 * @param <T> the generic type
 */
public class RankingRuleFormatter<T extends Enum<T>> implements Formatter<RankingRule<T>> {

  /** The separator. */
  private String separator = ":";
  
  /** The rule. */
  private Class<T> rule;

  /**
   * Instantiates a new ranking rule formatter.
   * 
   * @param rule the rule
   */
  public RankingRuleFormatter(Class<T> rule) {
    this.rule = rule;
  }

  /* (non-Javadoc)
   * @see org.springframework.format.Printer#print(java.lang.Object, java.util.Locale)
   */
  @Override
  public String print(RankingRule<T> object, Locale locale) {
    return object.getCriterion() + separator + object.getOrder();
  }

  /* (non-Javadoc)
   * @see org.springframework.format.Parser#parse(java.lang.String, java.util.Locale)
   */
  @Override
  public RankingRule<T> parse(String text, Locale locale) throws ParseException {
    String[] rankBy = text.split(getSeparator());
    if (rankBy.length == 2) {
      RankingRule<T> rc = new RankingRule<T>();
      rc.setCriterion(T.valueOf(rule, rankBy[0]));
      rc.setOrder(RankingOrder.valueOf(rankBy[1]));
      return rc;
    }
    throw new IllegalArgumentException(text);
  }

  /**
   * Gets the separator.
   * 
   * @return the separator
   */
  public String getSeparator() {
    return separator;
  }

  /**
   * Sets the separator.
   * 
   * @param separator the new separator
   */
  public void setSeparator(String separator) {
    this.separator = separator;
  }

}
