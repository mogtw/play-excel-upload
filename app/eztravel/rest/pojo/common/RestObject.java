/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.rest.pojo.common
 * @FileName: RestObject.java
 * @author:   tonywang
 * @date:     2013/12/15, 下午 12:47:11
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.rest.pojo.common;

import java.util.List;
import java.util.UUID;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Base class.
 * 
 * @author Rocko
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class RestObject {

  /** The key. */
  private UUID key;
  
  /** The status. */
  private Integer status;
  
  /** The errors. */
  private List<RestError> errors;

  /**
   * Gets the key.
   * 
   * @return the key
   */
  public UUID getKey() {
    return key;
  }

  /**
   * Sets the key.
   * 
   * @param key the new key
   */
  public void setKey(UUID key) {
    this.key = key;
  }

  /**
   * Gets the status.
   * 
   * @return the status
   */
  public Integer getStatus() {
    return status;
  }

  /**
   * Sets the status.
   * 
   * @param status the new status
   */
  public void setStatus(Integer status) {
    this.status = status;
  }

  /**
   * Gets the errors.
   * 
   * @return the errors
   */
  public List<RestError> getErrors() {
    return errors;
  }

  /**
   * Sets the errors.
   * 
   * @param errors the new errors
   */
  public void setErrors(List<RestError> errors) {
    this.errors = errors;
  }

}
