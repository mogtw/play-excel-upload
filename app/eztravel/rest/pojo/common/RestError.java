/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.rest.pojo.common
 * @FileName: RestError.java
 * @author:   tonywang
 * @date:     2013/12/15, 下午 12:47:16
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.rest.pojo.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * The Class RestError.
 * 
 * <pre>
 * 
 * </pre>
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class RestError {

  /** The code. */
  private Integer code;
  
  /** The message. */
  private String message;

  /** The description. */
  private String description;
  /**
   * Gets the code.
   * 
   * @return the code
   */
  public Integer getCode() {
    return code;
  }

  /**
   * Sets the code.
   * 
   * @param code the new code
   */
  public void setCode(Integer code) {
    this.code = code;
  }

  /**
   * Gets the message.
   * 
   * @return the message
   */
  public String getMessage() {
    return message;
  }

  /**
   * Sets the message.
   * 
   * @param message the new message
   */
  public void setMessage(String message) {
    this.message = message;
  }

  /**
   * Gets the description.
   * 
   * @return the description
   */
  public String getDescription() {
    return description;
  }

  /**
   * Sets the description.
   * 
   * @param description the new description
   */
  public void setDescription(String description) {
    this.description = description;
  }
  
  

}
