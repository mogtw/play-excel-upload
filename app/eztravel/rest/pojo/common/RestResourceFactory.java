/**
 * EZTRAVEL CONFIDENTIAL
 * @Package:  eztravel.rest.pojo.common
 * @FileName: RestResourceFactory.java
 * @author:   tonywang
 * @date:     2013/12/15, 下午 12:47:13
 * 
 * <pre>
 *  Copyright 2013-2014 The ezTravel Co., Ltd. all rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of ezTravel Co., Ltd. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to ezTravel Co., Ltd. and its suppliers
 *  and may be covered by TAIWAN and Foreign Patents, patents in 
 *  process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from ezTravel Co., Ltd.
 *  </pre>
 */
package eztravel.rest.pojo.common;

import java.util.UUID;

/**
 * A factory for creating RestResource objects.
 * 
 * @author Rocko
 */
public class RestResourceFactory {

  /**
   * New instance.
   * 
   * @param <E> the element type
   * @return the rest resource
   */
  public static <E> RestResource<E> newInstance() {
    RestResource<E> r = new RestResource<E>();
    r.setKey(UUID.randomUUID());
    return r;
  }

}
