package service.impl.excel;

import java.util.List;

import models.pojo.FileInfo;
import play.mvc.Result;
import play.mvc.Http.MultipartFormData;
import play.mvc.Http.MultipartFormData.FilePart;
import service.MultipartFormDataValidateService;
import utils.MyUtils;

public class MultipartFormDataValidateServiceImpl implements MultipartFormDataValidateService {

	@Override
	public String validate(MultipartFormData body) {				
		
		List<FileInfo> lstFileInfo = new MyUtils().getImportFileInfoList();
		
		for(int i = 0; i < lstFileInfo.size(); i++ ) {
			FilePart filePart = body.getFile(lstFileInfo.get(i).getFileName());			
			if(filePart == null) {
				return lstFileInfo.get(i).getFileDescription() + " 檔案需要上傳!";
			}			
		}
		return "";		
	}

}
