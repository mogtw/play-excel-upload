package service.impl.excel;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import models.pojo.FileInfo;
import play.mvc.Http.MultipartFormData.FilePart;
import service.CreatePfProImportException;
import service.CreatePfProImportService;
import utils.MyUtils;
import utils.parser.excel.ExcelReader;
import utils.parser.excel.WorkSheetContentHandler;
import utils.parser.excel.impl.ItemExcelRowContentCallbackImpl;
import utils.parser.excel.impl.MainExcelRowContentCallbackImpl;
import utils.parser.excel.impl.PfProAirinfoItemParser;
import utils.parser.excel.impl.PfProAllotItemParser;
import utils.parser.excel.impl.PfProDImgsItemParser;
import utils.parser.excel.impl.PfProDScheduledItemParser;
import utils.parser.excel.impl.PfProImgsItemParser;
import utils.parser.excel.impl.PfProItemParser;
import utils.parser.excel.impl.PfProPriceItemParser;
import utils.parser.excel.impl.PfProRefItemParser;
import utils.parser.excel.impl.XlsxReader;
import eztravel.core.pojo.frnplatform.PfPro;
import eztravel.core.pojo.frnplatform.PfProAirinfo;
import eztravel.core.pojo.frnplatform.PfProAllot;
import eztravel.core.pojo.frnplatform.PfProDImgs;
import eztravel.core.pojo.frnplatform.PfProDScheduled;
import eztravel.core.pojo.frnplatform.PfProImgs;
import eztravel.core.pojo.frnplatform.PfProPrice;
import eztravel.core.pojo.frnplatform.PfProRef;
import eztravel.core.pojo.frnplatform.wrapper.PfProGroup;
import eztravel.core.pojo.frnplatform.wrapper.PfProImport;
import eztravel.core.pojo.frnplatform.wrapper.PfProImportDetail;

public class CreatePfProImportServiceImpl implements CreatePfProImportService {
	private String vendNo;
	private String userId;
	private List<PfProImportDetail> lstPfProImportDetail = new ArrayList<PfProImportDetail>();
	
	
	public CreatePfProImportServiceImpl(String vendNo, String userId) {
		this.vendNo = vendNo;
		this.userId = userId;
	}

	public void validate(Map<String, FilePart> fileParts) throws CreatePfProImportException {				
		
		List<FileInfo> lstFileInfo = new MyUtils().getImportFileInfoList();
		
		for(int i = 0; i < lstFileInfo.size(); i++ ) {
			String fileName = lstFileInfo.get(i).getFileName();
			FilePart filePart = fileParts.get(fileName);
						
			if(filePart == null) {
				throw new CreatePfProImportException(lstFileInfo.get(i).getFileDescription() + " 檔案需要上傳!");
			} else if(!filePart.getFilename().equals(fileName)) {
				throw new CreatePfProImportException(lstFileInfo.get(i).getFileDescription() + " 檔案名不符合規定(" +fileName + ")"); 
			}			
		}
	}
	
	@Override
	public void addFiles(Map<String, FilePart> fileParts) throws CreatePfProImportException {
						
		try {
			this.validate(fileParts);
			
			List<PfPro> lstPfPro = new ArrayList<PfPro>();
			List<PfProRef> lstPfProRef = new ArrayList<PfProRef>();
			Map<String, List<PfProImgs>> mapPfProImgs = new HashMap<String, List<PfProImgs>>(); 
			Map<String, List<PfProAirinfo>> mapPfProAirinfo = new HashMap<String, List<PfProAirinfo>>();
			Map<String, List<PfProDImgs>> mapPfProDImgs = new HashMap<String, List<PfProDImgs>>();
			Map<String, List<PfProDScheduled>> mapPfProDScheduled = new HashMap<String, List<PfProDScheduled>>();
			Map<String, List<PfProPrice>> mapPfProPrice = new HashMap<String, List<PfProPrice>>();
			Map<String, List<PfProAllot>> mapPfProAllot = new HashMap<String, List<PfProAllot>>();
									
			List<PfProAllot> lstProAllot = null;
						
			// 解新 excel
			ExcelReader reader = new XlsxReader();
															
			WorkSheetContentHandler workSheetContentHandler = new WorkSheetContentHandler(
					new MainExcelRowContentCallbackImpl<PfPro>(lstPfPro, vendNo, new PfProItemParser(), "主商品內容檔"));
			reader.read(fileParts.get("1_prod.xlsx").getFile().getAbsolutePath(), 0, workSheetContentHandler);			
			
			workSheetContentHandler.setCallback(new MainExcelRowContentCallbackImpl<PfProRef>(lstPfProRef, vendNo, new PfProRefItemParser(), "主商品內容檔參考資訊檔"));
			reader.read(fileParts.get("1_prod.xlsx").getFile().getAbsolutePath(), 0, workSheetContentHandler);
			
			workSheetContentHandler.setCallback(new ItemExcelRowContentCallbackImpl<PfProImgs>(mapPfProImgs, vendNo, new PfProImgsItemParser(), "主商品圖檔"));
			reader.read(fileParts.get("2_prod_imgs.xlsx").getFile().getAbsolutePath(), 0, workSheetContentHandler);
						
			workSheetContentHandler.setCallback(new ItemExcelRowContentCallbackImpl<PfProAirinfo>(mapPfProAirinfo, vendNo, new PfProAirinfoItemParser(), "航班檔"));
			reader.read(fileParts.get("3_airinfo.xlsx").getFile().getAbsolutePath(), 0, workSheetContentHandler);
			
			workSheetContentHandler.setCallback(new ItemExcelRowContentCallbackImpl<PfProDImgs>(mapPfProDImgs, vendNo, new PfProDImgsItemParser(), "個團by Day圖檔"));
			reader.read(fileParts.get("4_d_imgs.xlsx").getFile().getAbsolutePath(), 0, workSheetContentHandler);
			
			workSheetContentHandler.setCallback(new ItemExcelRowContentCallbackImpl<PfProDScheduled>(mapPfProDScheduled, vendNo, new PfProDScheduledItemParser(), "行程內容檔"));
			reader.read(fileParts.get("5_scheduled.xlsx").getFile().getAbsolutePath(), 0, workSheetContentHandler);
			
			workSheetContentHandler.setCallback(new ItemExcelRowContentCallbackImpl<PfProPrice>(mapPfProPrice, vendNo, new PfProPriceItemParser(), "個團價格檔"));
			reader.read(fileParts.get("6_prod_price.xlsx").getFile().getAbsolutePath(), 0, workSheetContentHandler);

			workSheetContentHandler.setCallback(new ItemExcelRowContentCallbackImpl<PfProAllot>(mapPfProAllot, vendNo, new PfProAllotItemParser(), "個團機位數量檔"));
			reader.read(fileParts.get("7_allot.xlsx").getFile().getAbsolutePath(), 0, workSheetContentHandler);

			// 建立 PfProImportDetail list
			for(int i = 0; i < lstPfPro.size(); i++) {
				PfPro pfPro = lstPfPro.get(i);				
				String pfProdNo = pfPro.getPfProdNo();
				
				PfProGroup pfProGroup = new PfProGroup();
				pfProGroup.setPfPro(pfPro);
				pfProGroup.setPfProRef(lstPfProRef.get(i));
				
				pfProGroup.setPfProImgs(getRelationItemFromMap(mapPfProImgs, pfProdNo));
				pfProGroup.setPfProAirinfos(getRelationItemFromMap(mapPfProAirinfo, pfProdNo));				
				pfProGroup.setPfProDImgs(getRelationItemFromMap(mapPfProDImgs, pfProdNo));
				pfProGroup.setPfProDScheduleds(getRelationItemFromMap(mapPfProDScheduled, pfProdNo));
				pfProGroup.setPfProPrices(getRelationItemFromMap(mapPfProPrice, pfProdNo));
				lstProAllot = mapPfProAllot.get(pfProdNo);
								
				if(lstProAllot != null && lstProAllot.size() > 0) {
					pfProGroup.setPfProAllot(lstProAllot.get(0));
				} else {
//					pfProGroup.setPfProAllot(new PfProAllot());
				}
								
				PfProImportDetail pfProImportDetail = new PfProImportDetail();
				pfProImportDetail.setPfProdNo(pfProdNo);
				pfProImportDetail.setProdContent(pfProGroup);								
				
				lstPfProImportDetail.add(pfProImportDetail);									
			}									
		} catch(Exception ex) {
			throw new CreatePfProImportException(ex.getMessage()); 
		}		
	}

	@Override
	public List<PfProImport> createPfProImportList(Integer bGroupNo) throws CreatePfProImportException {
		
		final int SLICE_SIZE = 500;	// 每 500 做切割
		int toIndex = 0;
		Short bGroupCount = getGroupCount(lstPfProImportDetail.size(), SLICE_SIZE); 
		
		try {
			
			List<PfProImport> lstPfProImport = new ArrayList<PfProImport>();	// 每 500 個商品一筆
						
//			if(lstPfProImport.size() > 0) {
//				bGroupNo = getNextBGroupNo();
//			}
			
			for(int fromIndex = 0; fromIndex < lstPfProImportDetail.size(); fromIndex += SLICE_SIZE) {
				
				if(fromIndex + SLICE_SIZE > lstPfProImportDetail.size()) {
					toIndex = lstPfProImportDetail.size();
				} else {
					toIndex = fromIndex + SLICE_SIZE; 
				}
				
				PfProImport pfProImport = new PfProImport(vendNo, 0 , userId, bGroupNo, bGroupCount);
				pfProImport.setPfProImportDetails(lstPfProImportDetail.subList(fromIndex, toIndex));
				pfProImport.setTotalNum(pfProImport.getPfProImportDetails().size());
				lstPfProImport.add(pfProImport);
			}
			return lstPfProImport;			
			
		} catch(Exception e) {
			throw new CreatePfProImportException(e.getMessage());
		}		
	}
	
	private <T> List<T> getRelationItemFromMap(Map<String, List<T>> map, String pfProNo) {
		List<T> list = map.get(pfProNo);
		
		if(list == null) {
			list = new ArrayList<T>();
		}
				
		return list;
	}
		
	// 產生 PfProImport.groupCount 
	private Short getGroupCount(int processCount, int separateCount) {
		int result = (processCount / separateCount) + ((processCount % separateCount) > 0 ? 1 : 0);
		return new Short((short) result);
	}			
}
