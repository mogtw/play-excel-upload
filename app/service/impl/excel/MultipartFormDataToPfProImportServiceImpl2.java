package service.impl.excel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import models.pojo.FileInfo;
import play.libs.WS;
import play.mvc.Http.MultipartFormData;
import play.mvc.Http.MultipartFormData.FilePart;
import service.MultipartFormDataToPfProImportService;
import utils.MyUtils;
import utils.RestUtils;
import utils.parser.excel.ExcelReader;
import utils.parser.excel.WorkSheetContentHandler;
import utils.parser.excel.impl.ItemExcelRowContentCallbackImpl;
import utils.parser.excel.impl.MainExcelRowContentCallbackImpl;
import utils.parser.excel.impl.PfProAirinfoItemParser;
import utils.parser.excel.impl.PfProAllotItemParser;
import utils.parser.excel.impl.PfProDImgsItemParser;
import utils.parser.excel.impl.PfProDScheduledItemParser;
import utils.parser.excel.impl.PfProImgsItemParser;
import utils.parser.excel.impl.PfProItemParser;
import utils.parser.excel.impl.PfProPriceItemParser;
import utils.parser.excel.impl.PfProRefItemParser;
import utils.parser.excel.impl.XlsxReader;
import eztravel.core.pojo.frnplatform.PfPro;
import eztravel.core.pojo.frnplatform.PfProAirinfo;
import eztravel.core.pojo.frnplatform.PfProAllot;
import eztravel.core.pojo.frnplatform.PfProDImgs;
import eztravel.core.pojo.frnplatform.PfProDScheduled;
import eztravel.core.pojo.frnplatform.PfProImgs;
import eztravel.core.pojo.frnplatform.PfProPrice;
import eztravel.core.pojo.frnplatform.PfProRef;
import eztravel.core.pojo.frnplatform.wrapper.PfProGroup;
import eztravel.core.pojo.frnplatform.wrapper.PfProImport;
import eztravel.core.pojo.frnplatform.wrapper.PfProImportDetail;

public class MultipartFormDataToPfProImportServiceImpl2 implements MultipartFormDataToPfProImportService {

	@Override
	public String validate(MultipartFormData body) {				
		
		List<FileInfo> lstFileInfo = new MyUtils().getImportFileInfoList();
		
		for(int i = 0; i < lstFileInfo.size(); i++ ) {
			String fileName = lstFileInfo.get(i).getFileName();
			FilePart filePart = body.getFile(fileName);
			
			
			if(filePart == null) {
				return lstFileInfo.get(i).getFileDescription() + " 檔案需要上傳!";
			} else if(!filePart.getFilename().equals(fileName + ".xlsx")) {
				return lstFileInfo.get(i).getFileDescription() + " 檔案名不符合規定(" +fileName + ")"; 
			}			
		}
		return "";		
	}
	
	@Override
	public List<PfProImport> createPfProImportListFormMultipartFormData(MultipartFormData body, String vendNo, String userId) throws Exception {		
		MyUtils myUtils = new MyUtils();
		
//		if(groupNo != null) {
//			throw new Exception(groupNo.toString());
//		}
				
		try {
			
			String jsonStringResult = WS.url("http://ezdevsrvt02.eztravel.com.tw:8080/frnplatform/rest/v1/pfproimport/getNextBGroupNo")
					.get().get(3000).getBody();
			
			Integer	groupNo = new RestUtils().getSimpleRestResult(jsonStringResult);
			
			List<FileInfo> fileInfoes = myUtils.getImportFileInfoList();
			List<PfProImport> lstPfProImport = new ArrayList<PfProImport>();	// 每 500 個商品一筆
			
			List<PfProImportDetail> lstPfProImportDetails = new ArrayList<PfProImportDetail>();
			List<PfProAllot> lstProAllot = null;
			
			String mainFilename = fileInfoes.get(0).getFileName();
//			FilePart mainFilePar = body.getFile(mainFilename);
			
//			MainFileParser mainFileParser = new MainFileParser();
//			ItemFileParser itemFileParser = new ItemFileParser();
			
			ExcelReader reader = new XlsxReader();
			
			
			List<PfPro> lstPfPro = new ArrayList<PfPro>();
			List<PfProRef> lstPfProRef = new ArrayList<PfProRef>();
			Map<String, List<PfProImgs>> mapPfProImgs = new HashMap<String, List<PfProImgs>>(); 
			Map<String, List<PfProAirinfo>> mapPfProAirinfo = new HashMap<String, List<PfProAirinfo>>();
			Map<String, List<PfProDImgs>> mapPfProDImgs = new HashMap<String, List<PfProDImgs>>();
			Map<String, List<PfProDScheduled>> mapPfProDScheduled = new HashMap<String, List<PfProDScheduled>>();
			Map<String, List<PfProPrice>> mapPfProPrice = new HashMap<String, List<PfProPrice>>();
			Map<String, List<PfProAllot>> mapPfProAllot = new HashMap<String, List<PfProAllot>>();
						
			WorkSheetContentHandler workSheetContentHandler = new WorkSheetContentHandler(
					new MainExcelRowContentCallbackImpl<PfPro>(lstPfPro, vendNo, new PfProItemParser(), "主商品內容檔"));
			reader.read(body.getFile("1_prod").getFile().getAbsolutePath(), 0, workSheetContentHandler);			
			
			workSheetContentHandler.setCallback(new MainExcelRowContentCallbackImpl<PfProRef>(lstPfProRef, vendNo, new PfProRefItemParser(), "主商品內容檔參考資訊檔"));
			reader.read(body.getFile("1_prod").getFile().getAbsolutePath(), 0, workSheetContentHandler);
			
			workSheetContentHandler.setCallback(new ItemExcelRowContentCallbackImpl<PfProImgs>(mapPfProImgs, vendNo, new PfProImgsItemParser(), "主商品圖檔"));
			reader.read(body.getFile("2_prod_imgs").getFile().getAbsolutePath(), 0, workSheetContentHandler);
			
			workSheetContentHandler.setCallback(new ItemExcelRowContentCallbackImpl<PfProAirinfo>(mapPfProAirinfo, vendNo, new PfProAirinfoItemParser(), "航班檔"));
			reader.read(body.getFile("3_airinfo").getFile().getAbsolutePath(), 0, workSheetContentHandler);
			
			workSheetContentHandler.setCallback(new ItemExcelRowContentCallbackImpl<PfProDImgs>(mapPfProDImgs, vendNo, new PfProDImgsItemParser(), "個團by Day圖檔"));
			reader.read(body.getFile("4_d_imgs").getFile().getAbsolutePath(), 0, workSheetContentHandler);
//			System.gc();
			workSheetContentHandler.setCallback(new ItemExcelRowContentCallbackImpl<PfProDScheduled>(mapPfProDScheduled, vendNo, new PfProDScheduledItemParser(), "行程內容檔"));
			reader.read(body.getFile("5_scheduled").getFile().getAbsolutePath(), 0, workSheetContentHandler);
//			System.gc();
			
			workSheetContentHandler.setCallback(new ItemExcelRowContentCallbackImpl<PfProPrice>(mapPfProPrice, vendNo, new PfProPriceItemParser(), "個團價格檔"));
			reader.read(body.getFile("6_prod_price").getFile().getAbsolutePath(), 0, workSheetContentHandler);
//
			workSheetContentHandler.setCallback(new ItemExcelRowContentCallbackImpl<PfProAllot>(mapPfProAllot, vendNo, new PfProAllotItemParser(), "個團機位數量檔"));
			reader.read(body.getFile("7_allot").getFile().getAbsolutePath(), 0, workSheetContentHandler);
			
//			List<PfPro> lstPfPro = mainFileParser.parse(mainFilePar.getFile(), vendNo, new PfProItemParser(), "主商品內容檔");			
//			List<PfProRef> lstPfProRef = mainFileParser.parse(mainFilePar.getFile(), vendNo, new PfProRefItemParser(), "主商品內容檔參考資訊檔");
//									
//			Map<String, List<PfProImgs>> mapPfProImgs = itemFileParser.parse(body.getFile("2_prod_imgs").getFile(), vendNo, new PfProImgsItemParser(), "主商品圖檔");			
//			Map<String, List<PfProAirinfo>> mapPfProAirinfo = itemFileParser.parse(body.getFile("3_airinfo").getFile(), vendNo, new PfProAirinfoItemParser(), "航班檔");
//			Map<String, List<PfProDImgs>> mapPfProDImgs = itemFileParser.parse(body.getFile("4_d_imgs").getFile(), vendNo, new PfProDImgsItemParser(), "個團by Day圖檔");
//			Map<String, List<PfProDScheduled>> mapPfProDScheduled =itemFileParser.parse(body.getFile("5_scheduled").getFile(), vendNo, new PfProDScheduledItemParser(), "行程內容檔");
//			
//			Map<String, List<PfProPrice>> mapPfProPrice = itemFileParser.parse(body.getFile("6_prod_price").getFile(), vendNo, new PfProPriceItemParser(), "個團價格檔");
//			Map<String, List<PfProAllot>> mapPfProAllot = itemFileParser.parse(body.getFile("7_allot").getFile(), vendNo, new PfProAllotItemParser(), "個團機位數量檔");			
			
			Short groupCount = myUtils.getGroupCount(lstPfPro.size(), 500); 
			boolean debug;
			debug = false;
			if(debug) {
				throw new IllegalArgumentException("debug");
			}
			
			for(int i = 0; i < lstPfPro.size(); i++) {
				PfPro pfPro = lstPfPro.get(i);				
				String pfProdNo = pfPro.getPfProdNo();
				
				PfProGroup pfProGroup = new PfProGroup();
				pfProGroup.setPfPro(pfPro);
				pfProGroup.setPfProRef(lstPfProRef.get(i));
				
				pfProGroup.setPfProImgs(getRelationItemFromMap(mapPfProImgs, pfProdNo));
				pfProGroup.setPfProAirinfos(getRelationItemFromMap(mapPfProAirinfo, pfProdNo));				
				pfProGroup.setPfProDImgs(getRelationItemFromMap(mapPfProDImgs, pfProdNo));
				pfProGroup.setPfProDScheduleds(getRelationItemFromMap(mapPfProDScheduled, pfProdNo));
				pfProGroup.setPfProPrices(getRelationItemFromMap(mapPfProPrice, pfProdNo));
				lstProAllot = mapPfProAllot.get(pfProdNo);
								
				if(lstProAllot != null && lstProAllot.size() > 0) {
					pfProGroup.setPfProAllot(lstProAllot.get(0));
				} else {
//					pfProGroup.setPfProAllot(new PfProAllot());
				}
								
				PfProImportDetail pfProImportDetail = new PfProImportDetail();
				pfProImportDetail.setPfProdNo(pfProdNo);
				pfProImportDetail.setProdContent(pfProGroup);
				lstPfProImportDetails.add(pfProImportDetail);
				
				if(lstPfProImportDetails.size() >= 500) {	// 每 500 做切割
					PfProImport pfProImport = new PfProImport(vendNo, lstPfProImportDetails.size(), userId, 
							groupNo, groupCount);
					
					pfProImport.setPfProImportDetails(lstPfProImportDetails);
					lstPfProImport.add(pfProImport);
					lstPfProImportDetails = new ArrayList<PfProImportDetail>();
				}				
			}
			
			if(lstPfProImportDetails.size() > 0) {
				PfProImport pfProImport = new PfProImport(vendNo, lstPfProImportDetails.size(), userId, 
						groupNo, groupCount);
				pfProImport.setPfProImportDetails(lstPfProImportDetails);
				lstPfProImport.add(pfProImport);
			}
			
			return lstPfProImport;
		} catch(Exception ex) {
			throw ex; 
		} finally {
			// 上傳後刪除所有的暫存檔
			for(FilePart filePart : body.getFiles()) {
				try {
					filePart.getFile().delete();
				} catch(Exception e) {
					throw new Exception("hello");
				}				
			}
		}
	}

//	private PfProAllot createDefaultPfProAllot() {
////		PfProAllot pfProAllot = new pfProAllot
//	}
	private <T> List<T> getRelationItemFromMap(Map<String, List<T>> map, String pfProNo) {
		List<T> list = map.get(pfProNo);
		
		if(list == null) {
			list = new ArrayList<T>();
		}
				
		return list;
	}
}
