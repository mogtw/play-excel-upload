package service;

import play.mvc.Http.MultipartFormData;

public interface MultipartFormDataValidateService {
	String validate(MultipartFormData body);
}
