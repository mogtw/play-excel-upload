package service;

public class CreatePfProImportException extends Exception {
	
	private static final long serialVersionUID = 7499825460275853185L;

	public CreatePfProImportException(String message, Throwable cause) {
		super(message, cause);
	}

	public CreatePfProImportException(String message) {
		super(message);
	}

}
