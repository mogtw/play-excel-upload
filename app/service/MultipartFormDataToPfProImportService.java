package service;

import java.util.List;

import eztravel.core.pojo.frnplatform.wrapper.PfProImport;
import play.mvc.Http.MultipartFormData;

public interface MultipartFormDataToPfProImportService {
	String validate(MultipartFormData body);
	List<PfProImport> createPfProImportListFormMultipartFormData(MultipartFormData data, String vendNo, String userId) throws Exception;
}
