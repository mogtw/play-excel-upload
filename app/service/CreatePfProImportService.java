package service;

import java.util.List;
import java.util.Map;

import play.mvc.Http.MultipartFormData.FilePart;
import eztravel.core.pojo.frnplatform.wrapper.PfProImport;

public interface CreatePfProImportService {	
	public void addFiles(Map<String, FilePart> fileParts) throws CreatePfProImportException;
	List<PfProImport> createPfProImportList(Integer bGroupNo) throws CreatePfProImportException;
}
