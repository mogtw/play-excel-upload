package controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import models.Job;
import models.JobResult;
import models.JobStatus;
import play.api.libs.json.Json;
import play.mvc.Result;
import static play.mvc.Results.*;

public class JobJson {
	public static Result submit() throws Throwable {
		return ok("120");
	}
	
	public static Result list() throws Throwable {
		List<Job> lists = new ArrayList<Job>();
		lists.add(createJob("1", "20140810 16:52", "雄獅", "異常"));
		lists.add(createJob("2", "20140811 16:50", "康福", "正常"));
		lists.add(createJob("3", "20140811 15:52", "雄獅", "異常"));
		lists.add(createJob("4", "20140815 13:52", "雄獅", "異常"));
		lists.add(createJob("5", "20140821 16:10", "雄獅", "異常"));
		
		return ok(new ObjectMapper().writeValueAsString(lists));
	}
	
	public static Result summary() throws Throwable {
		List<JobStatus> lists = new ArrayList<JobStatus>();
		lists.add(createJobStatus("1", "雄獅", 100, 1));
		lists.add(createJobStatus("2", "康福", 299, 1));
		lists.add(createJobStatus("3", "雄獅", 28, 2));
		lists.add(createJobStatus("4", "雄獅", 50, 3));
		lists.add(createJobStatus("5", "雄獅", 250, 5));
		
		return ok(new ObjectMapper().writeValueAsString(lists));
	}
	
	public static Result result(String id) throws Throwable {
		Map<String, List<JobResult>> map = new HashMap<String, List<JobResult>>();
		
		List<JobResult> lists;
		
		lists = new ArrayList<JobResult>();
		lists.add(createJobResult("雄獅", "JHA02140819A", "011", "reason1", "error1"));
		lists.add(createJobResult("雄獅", "JHA02140819B", "011", "reason2", "error2"));
		map.put("1", lists);
		
		lists = new ArrayList<JobResult>();
		lists.add(createJobResult("康福", "JHA02140819C", "012", "reason3", "error3"));
		lists.add(createJobResult("康福", "JHA02140819D", "012", "reason4", "error4"));
		map.put("2", lists);

		lists = new ArrayList<JobResult>();
		lists.add(createJobResult("雄獅", "JHA02140819C", "013", "reason3", "error3" ));		
		map.put("3", lists);
		
		lists = new ArrayList<JobResult>();
		lists.add(createJobResult("雄獅", "JHA02140819D", "014", "reason4", "error4"));		
		lists.add(createJobResult("雄獅", "JHA02140819E", "014", "reason4", "error4"));
		lists.add(createJobResult("雄獅", "JHA02140819F", "014", "reason4", "error4"));		
		map.put("4", lists);
		
		lists = new ArrayList<JobResult>();
		lists.add(createJobResult("雄獅", "JHA02140819G", "015", "reason5", "error5"));		
		lists.add(createJobResult("雄獅", "JHA02140819H", "015", "reason5", "error5"));
		lists.add(createJobResult("雄獅", "JHA02140819I", "015", "reason5", "error5"));		
		map.put("5", lists);
		
		return ok(new ObjectMapper().writeValueAsString(map.get(id)));
	}
	
	private static Job createJob(String id, String job_dt, String vendor, String status) {
		Job job = new Job();
		job.id = id;
		job.job_dt = job_dt;
		job.vendor = vendor;
		job.status = status;
		
		return job;
	}
	
	private static JobStatus createJobStatus(String id, String vendor, int success, int failure) {
		JobStatus jobStatus = new JobStatus();
		jobStatus.id = id;
		jobStatus.vendor = vendor;
		jobStatus.success = success;
		jobStatus.failure = failure;
		
		return jobStatus;
	}
	
	private static JobResult createJobResult(String vendor, String g_prod_no, String error_code, String error_reason, String error_content) {
		JobResult jobResult = new JobResult();
		jobResult.vendor = vendor;
		jobResult.g_prod_no = g_prod_no;
		jobResult.error_code = error_code;
		jobResult.error_reason = error_reason;
		jobResult.error_content = error_content;
		
		return jobResult;
	}
}
