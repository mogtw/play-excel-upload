package controllers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import models.PfProOutputVO;
import models.VndMainVO;
import play.data.DynamicForm;
import play.data.Form;
import play.libs.F.Function;
import play.libs.F.Function0;
import play.libs.F.Promise;
import play.libs.Json;
import play.libs.WS;
import play.mvc.Controller;
import play.mvc.Http.MultipartFormData;
import play.mvc.Http.MultipartFormData.FilePart;
import play.mvc.Http.RequestBody;
import play.mvc.Result;
import service.CreatePfProImportException;
import service.CreatePfProImportService;
import service.MultipartFormDataToPfProImportService;
import service.impl.excel.CreatePfProImportServiceImpl;
import service.impl.excel.MultipartFormDataToPfProImportServiceImpl2;
import utils.MyUtils;
import utils.RestUtils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import eztravel.core.pojo.frnplatform.PfProImpDetail;
import eztravel.core.pojo.frnplatform.PfProImpMain;
import eztravel.core.pojo.frnplatform.wrapper.PfProImport;
import eztravel.core.pojo.frnplatform.wrapper.PfProOutput;
import eztravel.rest.util.common.StringUtils;

public class Index extends Controller {

	public static Result test_login() {
		return ok(views.html.test_login.render());
	}
	
	/*
	 * 檢查上傳檔, 並讀出excel內容
	 * POST
	 */
	public static Promise<Result> submit2() {
		
		return Promise.promise(new Function0<Result>() {
			
			@Override
			public Result apply() throws Throwable {
				RestUtils restUtils = new RestUtils();
				MyUtils myUtils = new MyUtils();
				
				String userId =  myUtils.getUserIdFromRequest(request());
				if(StringUtils.fp_isNull(userId) == "") {
					return unauthorized("尚未經過使用者認證");
				}
//				userId = "001053";
				
				List<PfProOutputVO> lstPfProOutputVO = new ArrayList<PfProOutputVO>(); 
							
				MultipartFormData body = request().body().asMultipartFormData();
				if(body == null) {
					return badRequest("無效的回效資料!");
				}
												
				String vendNo = myUtils.getVendNoFromFormUrlEncodeMap(body.asFormUrlEncoded());
				
				if(StringUtils.fp_isNull(vendNo) == "") {
					return ok("請輸入平台供應商!");
				}				
//				vendNo = "VDR0000001943";	// 5福
//				vendNo = "VDR0000001942";	// 可樂				
				
//				if(vendNo == null) {
//					return badRequest("供應商不可空白!");
//				}															
				List<PfProImport> lstPfProImport = null;
				try {
					CreatePfProImportService createPfProImportService = new CreatePfProImportServiceImpl(vendNo, userId);
					Map<String, Map<String, FilePart>> filePartsMap = myUtils.classifyFilePartsByKey(body.getFiles());
					
					for(String fileGroup : filePartsMap.keySet()) {
						Map<String, FilePart> fileParts = filePartsMap.get(fileGroup);
						createPfProImportService.addFiles(fileParts);						
					}
					
					String result = "";
					lstPfProImport = createPfProImportService.createPfProImportList(restUtils.getNextBGroupNo());
					int fromIndex = 1;
					int toIndex = 0;
					
					for(int i = 0; i < lstPfProImport.size(); i++) {											
						PfProImport pfProImport = lstPfProImport.get(i);

						toIndex += pfProImport.getPfProImportDetails().size();
						
						String jsonString = new ObjectMapper().writeValueAsString(pfProImport);
//						boolean debug = true;
//						if(debug == true)
//							return ok(jsonString);
						
						String jsonStringResult = WS.url("http://ezdevsrvt02.eztravel.com.tw:8080/frnplatform/rest/v1/pfproimport/importDatas")
								.post(jsonString).get(30000).getBody();
						
//						String jsonStringResult = "{\"key\":\"f5654446-aa3d-4d5b-bbe6-facdb1e689e2\",\"items\":[{\"jobNo\":\"301\",\"jobStatus\":\"OK\"}]}";
								
						if(jsonStringResult == null) {
							// Serverr 異常?
							lstPfProOutputVO.add(new PfProOutputVO(i, " ", fromIndex + " ~ " + toIndex, "上傳異常"));														
						} else {												
//							RestResource<PfProOutput> restResult = new ObjectMapper().readValue(jsonStringResult, new TypeReference<RestResource<PfProOutput>>(){});
							String processResult = null;
							
							try {
								// {"key":"f5654446-aa3d-4d5b-bbe6-facdb1e689e2","items":[{"jobNo":"301","jobStatus":"OK"}]}
								PfProOutput pfProOutput = restUtils.getPfProOutputResult(jsonStringResult);
//								processResult = "匯入單號: " + pfProOutput.getJobNo() + " 上傳結果: " + pfProOutput.getJobStatus();
								lstPfProOutputVO.add(new PfProOutputVO(i, pfProOutput.getJobNo(), fromIndex + " ~ " + toIndex,  pfProOutput.getJobStatus()));
							} catch(Exception ex) {
//								processResult = ex.getMessage();
								lstPfProOutputVO.add(new PfProOutputVO(i, " ", fromIndex + " ~ " + toIndex,  ex.getMessage()));
							}
							
//							result += utils.DateUtils.getDateTime() + 
//									(lstPfProImport.size() > 1 ? " 第 " + String.valueOf(i + 1) + " 批" : "") + 
//									processResult + "<br />";
						}
						
						fromIndex += pfProImport.getPfProImportDetails().size();
					}
					
					response().setContentType("text/html; charset=utf-8");
//					return ok("<h1>" + result + "</h1>").as("text/html");
					return ok(views.html.ajax.upload_success.render(lstPfProOutputVO));
					
				} catch(CreatePfProImportException ex) {
					return ok(ex.getMessage());
				} catch(Exception ex) {
					return ok(ex.toString());
				} finally {
					if(lstPfProImport != null) {
						lstPfProImport.clear();
					}
					lstPfProImport = null;
				}
//				if(restResult.getErrors() != null) {
//				RestError restError = restResult.getErrors().get(0);
//				processResult = restError.getMessage();
//			} else {
//				PfProOutput pfProOutput = restResult.getItems().get(0);
//				processResult = "匯入單號: " + pfProOutput.getJobNo() + " 上傳結果: " + pfProOutput.getJobStatus(); 																									
								
			}					
		});
	}
	
	public static Result mock_up() {
		return ok(views.html.upload_mock.render());
	}
	
	public static Promise<Result> submit_up() {
		return Promise.promise(new Function0<Result>() {
			
			@Override
			public Result apply() throws Throwable {
				RestUtils restUtils = new RestUtils();
				MyUtils myUtils = new MyUtils();
				
				MultipartFormData body = request().body().asMultipartFormData();
				if(body == null) {
					return badRequest("無效的回效資料!");
				}
				return internalServerError("server error");
				
//				Map filePartsMap = myUtils.classifyFilePartsByKey(body.getFiles());
//				
//				
//				MultipartFormDataToPfProImportService service = new MultipartFormDataToPfProImportServiceImpl2();				
//				String vendNo = request().getQueryString("vendNo");
//				vendNo = "VDR0000001942";
//				
//				if(vendNo == null) {
//					return badRequest("供應商不可空白!");
//				}
//				String errorMessage = service.validate(body);
//				if(!errorMessage.equals("")) {
//					return badRequest(errorMessage);
//				}
//				
//				String result = "";
//				List<PfProImport> lstPfProImport = service.createPfProImportListFormMultipartFormData(body, vendNo, "001053");
//				for(int i = 0; i < lstPfProImport.size(); i++) {					
//					PfProImport pfProImport = lstPfProImport.get(i);
//					
//					String jsonString = new ObjectMapper().writeValueAsString(pfProImport);
////					boolean debug = true;
////					if(debug == true)
////						return ok(jsonString);
//					
//					String jsonStringResult = WS.url("http://ezdevsrvt02.eztravel.com.tw:8080/frnplatform/rest/v1/pfproimport/importDatas")
//							.post(jsonString).get(30000).getBody();
//					
////					String jsonStringResult = "{\"key\":\"f5654446-aa3d-4d5b-bbe6-facdb1e689e2\",\"items\":[{\"jobNo\":\"301\",\"jobStatus\":\"OK\"}]}";
//							
//					if(jsonStringResult == null) {
//						// Serverr 異常?
//					} else {												
////						RestResource<PfProOutput> restResult = new ObjectMapper().readValue(jsonStringResult, new TypeReference<RestResource<PfProOutput>>(){});
//						String processResult = null;
//						
//						try {
//							// {"key":"f5654446-aa3d-4d5b-bbe6-facdb1e689e2","items":[{"jobNo":"301","jobStatus":"OK"}]}
//							PfProOutput pfProOutput = restUtils.getPfProOutputResult(jsonStringResult);
//							processResult = "匯入單號: " + pfProOutput.getJobNo() + " 上傳結果: " + pfProOutput.getJobStatus();
//						} catch(Exception ex) {
//							processResult = ex.getMessage();
//						}
//						
////						if(restResult.getErrors() != null) {
////							RestError restError = restResult.getErrors().get(0);
////							processResult = restError.getMessage();
////						} else {
////							PfProOutput pfProOutput = restResult.getItems().get(0);
////							processResult = "匯入單號: " + pfProOutput.getJobNo() + " 上傳結果: " + pfProOutput.getJobStatus(); 
////						}
//						result += utils.DateUtils.getDateTime() + 
//								(lstPfProImport.size() > 1 ? " 第 " + String.valueOf(i + 1) + " 批" : "") + 
//								processResult + "<br />";
//					}
//				}
//				
//				response().setContentType("text/html; charset=utf-8");
//				return ok("<h1>" + result + "</h1>").as("text/html");
			}					
		});
	}
	
	public static Result memory() {
		int mb = 1024*1024;
		String result = "";
		//Getting the runtime reference from system
		Runtime runtime = Runtime.getRuntime();
		
		result += "##### Heap utilization statistics [MB] #####<br />";
		
		//Print used memory
		result += "Used Memory:" + (runtime.totalMemory() - runtime.freeMemory()) / mb + "<br />";

		//Print free memory
		result += "Free Memory: " + runtime.freeMemory() / mb + "<br />";
		
		//Print total available memory
		result += "Total Memory:" + runtime.totalMemory() / mb + "<br />";

		//Print Maximum available memory
		result += "Max Memory:" + runtime.maxMemory() / mb + "<br />";
		
		return ok(result).as("text/html");
	}
	
	public static Promise<Result> index() {		
		String vendNo = "VDR0000001942";
		String importDate = "20140918"; 
		
		 final Promise<Result> resultPromise = WS.url("http://ezdevsrvt02.eztravel.com.tw:8080/frnplatform/rest/v1/pfproimport/" + vendNo + "/" + importDate + "/listMainByVendNoAndCreateDate").get().map(
				 new Function<WS.Response, Result>() {
		                public Result apply(WS.Response response) throws Throwable {
		                	RestUtils restUtils = new RestUtils();
		                	
 		                	String josnStringResult = response.getBody();
 		                	List<PfProImpMain> pfProImpMains = restUtils.getPfProImpMainResult(josnStringResult);
 		                	Map<String, VndMainVO> vndMainVoMap = restUtils.getVndMainVOMap(restUtils.getVndMainJsonString());
 		                	Collections.sort(pfProImpMains, new Comparator<PfProImpMain>() {

								@Override
								public int compare(PfProImpMain o1,
										PfProImpMain o2) {
										long value = o1.getJobNo() - o2.getJobNo();
										if(value < 1) 
											return -1;
										else if(value > 0)
											return 1;
										else return 0;
								} 		                		
 		                	});
 		                	
 		                	for(PfProImpMain impMain : pfProImpMains) {
 		                		VndMainVO vndMainVo = vndMainVoMap.get(impMain.getVendNo());
 		                		if(vndMainVo != null) {
 		                			impMain.setVendNo(vndMainVo.vendNameCh);	// 轉換成中文
 		                		} 		                		
// 		                		impMain.setProcessDt(DateUtils.);
 		                	}
// 		                	String userId = request().cookie("userId").value();
 		                	return ok(views.html.job.render(pfProImpMains, vndMainVoMap));
		                	
//		                	List<PfProImpMain> lstPfProImpMain = null;
//		                	
////		                	return viewresponse)(response.asJson());
////		                	return views.html.job.rener(parseJobs(response.asJson().asText()));
////		                	return ok("test");
//		                	String body = response.getBody();
//		                	if(body == null) {
//		                		lstPfProImpMain = new ArrayList<PfProImpMain>();
//		                	} else {
//		                		lstPfProImpMain =new MyUtils().json2PfProImpMainList(body);		                		
//		                	}		                	
////		                	return ok(views.html.job.render(jobs));
		         
		                }
				 }
		    );
		    return resultPromise;		    			
	}
	
	// 網頁上傳 html	
	public static Result upload() {
		return ok(views.html.upload2.render());
	}

	/*
	 * 檢查上傳檔, 並讀出excel內容
	 * POST
	 */
	public static Promise<Result> submit() {
		
		return Promise.promise(new Function0<Result>() {
			
			@Override
			public Result apply() throws Throwable {
				RestUtils restUtils = new RestUtils();
				
				MultipartFormData body = request().body().asMultipartFormData();
				if(body == null) {
					return badRequest("無效的回效資料!");
				}
		
				
				MultipartFormDataToPfProImportService service = new MultipartFormDataToPfProImportServiceImpl2();				
				String vendNo = request().getQueryString("vendNo");
				vendNo = "VDR0000001942";
				
				if(vendNo == null) {
					return badRequest("供應商不可空白!");
				}
								
				String errorMessage = service.validate(body);
				if(!errorMessage.equals("")) {
					return badRequest(errorMessage);
				}
				
				String result = "";
				List<PfProImport> lstPfProImport = service.createPfProImportListFormMultipartFormData(body, vendNo, "001053");
				for(int i = 0; i < lstPfProImport.size(); i++) {					
					PfProImport pfProImport = lstPfProImport.get(i);
					
					String jsonString = new ObjectMapper().writeValueAsString(pfProImport);
//					boolean debug = true;
//					if(debug == true)
//						return ok(jsonString);
					
					String jsonStringResult = WS.url("http://ezdevsrvt02.eztravel.com.tw:8080/frnplatform/rest/v1/pfproimport/importDatas")
							.post(jsonString).get(30000).getBody();
					
//					String jsonStringResult = "{\"key\":\"f5654446-aa3d-4d5b-bbe6-facdb1e689e2\",\"items\":[{\"jobNo\":\"301\",\"jobStatus\":\"OK\"}]}";
							
					if(jsonStringResult == null) {
						// Serverr 異常?
					} else {												
//						RestResource<PfProOutput> restResult = new ObjectMapper().readValue(jsonStringResult, new TypeReference<RestResource<PfProOutput>>(){});
						String processResult = null;
						
						try {
							// {"key":"f5654446-aa3d-4d5b-bbe6-facdb1e689e2","items":[{"jobNo":"301","jobStatus":"OK"}]}
							PfProOutput pfProOutput = restUtils.getPfProOutputResult(jsonStringResult);
							processResult = "匯入單號: " + pfProOutput.getJobNo() + " 上傳結果: " + pfProOutput.getJobStatus();
						} catch(Exception ex) {
							processResult = ex.getMessage();
						}
						
//						if(restResult.getErrors() != null) {
//							RestError restError = restResult.getErrors().get(0);
//							processResult = restError.getMessage();
//						} else {
//							PfProOutput pfProOutput = restResult.getItems().get(0);
//							processResult = "匯入單號: " + pfProOutput.getJobNo() + " 上傳結果: " + pfProOutput.getJobStatus(); 
//						}
						result += utils.DateUtils.getDateTime() + 
								(lstPfProImport.size() > 1 ? " 第 " + String.valueOf(i + 1) + " 批" : "") + 
								processResult + "<br />";
					}
				}
				
				response().setContentType("text/html; charset=utf-8");
				return ok("<h1>" + result + "</h1>").as("text/html");
			}					
		});
	}
	
	public static Result up() {
		return ok(views.html.uploadtest.render());
	}
	
	/*
	 * 檢查上傳檔, 並讀出excel內容
	 * POST
	 */
	public static Promise<Result> testSumit() {
		
		return Promise.promise(new Function0<Result>() {
			
			@Override
			public Result apply() throws Throwable {
				
				MultipartFormData body = request().body().asMultipartFormData();
				if(body == null) {
					return badRequest("無效的回效資料!");
				}
				FilePart filPart = body.getFile("test1");					
				
				JsonNode json = Json.toJson("ok");
				return ok(json);
			}					
		});
	}
	
//	public static Promise<Result> list() {
//		
//		 final Promise<Result> resultPromise = WS.url("http://localhost:9000/json").get().map(
//		            new Function<WS.Response, Result>() {
//		                public Result apply(WS.Response response) throws Throwable {
//		                    
////		                	return viewresponse)(response.asJson());
////		                	return views.html.job.rener(parseJobs(response.asJson().asText()));
////		                	return ok("test");
//		                	String jobString = response.getBody();
//		                	List<Job> jobs = parseJobs(jobString);
//		                	return ok(views.html.job.render(jobs));
//		                }
//		            }
//		    );
//		    return resultPromise;		    			
//	}
//	
//	public static Promise<Result> summary() {
//		
//		 final Promise<Result> resultPromise = WS.url("http://localhost:9000/json/summary").get().map(
//		            new Function<WS.Response, Result>() {
//		                public Result apply(WS.Response response) throws Throwable {
//		                    
//		                	int status = response.getStatus();
//		                	String body = response.getBody();
//		                	if(response.getStatus() == OK) {		                				                	
//		                		List<JobStatus> result = parseJobsStatus(body);
//		                		return ok(views.html.summary.render(result));
//		                	} else {
//		                		return internalServerError("連接 Server 失敗!" + body);
//		                	}
//		                }
//		            }
//		    );
//		    return resultPromise;		    			
//		
//	}
//	
//	public static Promise<Result> jobStatus(String id) {
//		
//		 final Promise<Result> resultPromise = WS.url("http://localhost:9000/json/" + id).get().map(
//		            new Function<WS.Response, Result>() {
//		                public Result apply(WS.Response response) throws Throwable {
//		                    
//		                	String jobString = response.getBody();
////		                	List<Job> jobs = parseJobs(jobString);
////		                	return ok(views.html.job.render(jobs));
//		                	return ok("");
//		                }
//		            }
//		    );
//		    return resultPromise;		    					
//	}
//
//	public static Promise<Result> result(String id) {
//		
//		 final Promise<Result> resultPromise = WS.url("http://localhost:9000/json/result/" + id).get().map(
//		            new Function<WS.Response, Result>() {
//		                public Result apply(WS.Response response) throws Throwable {
//		                    
//		                	int status = response.getStatus();
//		                	String body = response.getBody();
//		                	
//		                	if(response.getStatus() == OK) {		                				                	
//		                		List<JobResult> result = parseJobResult(body);
//		                		return ok(views.html.result.render(result));
//		                	} else {
//		                		return internalServerError("連接 Server 失敗!" + body);
//		                	}
//		                	
//		                }
//		            }
//		    );
//		    return resultPromise;		    					
//	}	
//	
//	public static List<Job> parseJobs(String jsonString) throws Throwable {
//		List<Job> result = new ArrayList<Job>();
//		result = new ObjectMapper().readValue(jsonString, new TypeReference<List<Job>>(){});
//		
//		return result;
////		new ObjectMapper().readValue(要反序列化的json字串, 要binding的類別);
////		if(json != null) {
////			if(json.isArray()) {
////				result = new ObjectMapper().readValue(jsonString, new TypeReference<List<Job>>(){});
//////				result = new ObjectMapper().readValues(json, Job.class);
////////				for(JsonNode jsonNode : json) {
////////					Job job = new ObjectMapper().readValue
////////							
////////							Json.fromJson(jsonNode, Job.class);
////////					result.add(job);
////				}
////			} else {
////				Job job = Json.fromJson(json, Job.class);
////				result.add(job);
////			}		
////		}	
//	}	
//		
//	public static List<JobStatus> parseJobsStatus(String jsonString) throws Throwable {
//		List<JobStatus> result = new ObjectMapper().readValue(jsonString, new TypeReference<List<JobStatus>>(){});
//		
//		return result;
//	}		
	
}
