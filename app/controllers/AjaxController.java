package controllers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import models.VndMainVO;
import models.enums.BusinessType;
import models.pojo.User;
import play.data.Form;
import play.libs.WS;
import play.libs.F.Function;
import play.libs.F.Promise;
import play.mvc.Controller;
import play.mvc.Result;
import utils.MyUtils;
import utils.RestUtils;
import eztravel.core.pojo.frnplatform.PfProImpDetail;
import eztravel.core.pojo.frnplatform.PfProImpMain;
import eztravel.rest.util.common.StringUtils;

public class AjaxController extends Controller {
	private static final Form<User> userForm = Form.form(User.class);
	
	// 依 User 是否有 login, 產生 login form 
	public static Result loginForm() {				
	    
		String userId = new MyUtils().getUserIdFromRequest(request());
		
	    if(userId == null) {
	    	// 已有 login 成功
	    	return ok(views.html.ajax.login_form.render());
	    }  else {
	    	// 沒 login, 產生 login form
	    	return ok("");
	    }	    	    
	}
	
	public static Result login() {				
		
		Form<User> boundForm = userForm.bindFromRequest();
		 
	    User user = boundForm.get();
	    
	    if(user != null && StringUtils.fp_isNull(user.getLoginId()) != "") {
	    	
	    	user.setBusinessType(BusinessType.WEBORDER);
	        user.setIp(request().remoteAddress());
	        
	        Controller.response().setCookie("userId", user.getLoginId(),
		             60 * 60 * 24, "/", "eztravel.com.tw");
//		            7 * 60 * 60 * 24, "/", "eztravel.com.tw");
	        
	        user.setLoginId(StringUtils.Decode(user.getLoginId()));	// 解碼成員編
	        
	        Controller.ctx().args.put(User.CONTEXT_KEY, user);
	        	        	        	     
	        return ok("ok");
	    } else {
	    	return unauthorized("驗証失敗!");
	    }
	    
//	    User user = (User) Controller.ctx().request().body().asFormUrlEncoded();
//	    String userId = user.getLoginId();
//	    Controller.response().setCookie("userId", StringUtils.Encode(userId.getBytes()),
//	    		1 * 60 * 60 * 24, "/", "eztravel.com.tw");
	    
	    
//	        7 * 60 * 60 * 24, "/", "eztravel.com.tw");
//	    return Results.ok(views.html.frnplatform.center.index.render(userId + "登入成功"));
//	    return ok(views.html.ajax.login_ok.render());
	}
	
	// 查詢匯入主檔
	public static Promise<Result> searchImpMain(String vendNo, String importDate) {		
		
		final Promise<Result> resultPromise = WS.url("http://ezdevsrvt02.eztravel.com.tw:8080/frnplatform/rest/v1/pfproimport/" + vendNo + "/" + importDate + "/listMainByVendNoAndCreateDate").get().map(
				 new Function<WS.Response, Result>() {
		                public Result apply(WS.Response response) throws Throwable {
		                	RestUtils restUtils = new RestUtils();
		                	
		                	if(response.getStatus() == OK) {
		                		String josnStringResult = response.getBody();
		                		List<PfProImpMain> pfProImpMains = restUtils.getPfProImpMainResult(josnStringResult);
		                		Map<String, VndMainVO> vndMainVoMap = restUtils.getVndMainVOMap(restUtils.getVndMainJsonString());
		                	
		                		for(PfProImpMain impMain : pfProImpMains) {
		                			VndMainVO vndMainVo = vndMainVoMap.get(impMain.getVendNo());
		                			if(vndMainVo != null) {
		                				impMain.setVendNo(vndMainVo.vendNameCh);	// 轉換成中文
		                			} 		                		
//		                			impMain.setProcessDt(DateUtils.);
		                		}
		                		
		                		Collections.sort(pfProImpMains, new Comparator<PfProImpMain>() {

									@Override
									public int compare(PfProImpMain o1,
											PfProImpMain o2) {
										long value = o1.getJobNo() - o2.getJobNo();
										if(value < 1) 
											return -1;
										else if(value > 0)
											return 1;
										else return 0;
									}		                		
								});
		                		
		                		return ok(views.html.ajax.main_view.render(pfProImpMains));
		                	} else {
		                		return notFound("查無符合資料");
		                	}		                	
		                }
				 }
		    );
		    return resultPromise;		    			
	}

	public static Promise<Result> searchImpDetail(Long jobNo) {		
		
		 final Promise<Result> resultPromise = WS.url("http://ezdevsrvt02.eztravel.com.tw:8080/frnplatform/rest/v1/pfproimport/" + jobNo + "/listDetailByJobNo").get().map(
				 new Function<WS.Response, Result>() {
		                public Result apply(WS.Response response) throws Throwable {
		                	int seq = 0;
		                	RestUtils restUtils = new RestUtils();
		                			                	
		                	String josnStringResult = response.getBody();
		                	List<PfProImpDetail> pfProImpDetails = restUtils.getPfProImpDetailResult(josnStringResult);		                	
		                	
		                	List<PfProImpDetail> result = new ArrayList<PfProImpDetail>();
		                	
		                	Map<String, VndMainVO> vendMap = restUtils.getVndMainVOMap(restUtils.getVndMainJsonString());
		                	
		                	for(PfProImpDetail pfProImpDetail : pfProImpDetails) {	// 只顯示錯誤的明細
		                		if(pfProImpDetail.getResponseMsg() != null) {
		                			pfProImpDetail.setJobNo(Long.valueOf(++seq));	// 改顯示序號
		                			VndMainVO vndMainVO = vendMap.get(pfProImpDetail.getVendNo());
		                			if(vndMainVO != null) {
		                				pfProImpDetail.setVendNo(vndMainVO.getVendNameCh());
		                			} 		                			
		                			result.add(pfProImpDetail);
		                		}
		                	}
		                	
		                	Collections.sort(result, new Comparator<PfProImpDetail>() {

								@Override
								public int compare(PfProImpDetail o1,
										PfProImpDetail o2) {
									long value = o1.getJobNo() - o2.getJobNo();
									if(value < 1) 
										return -1;
									else if(value > 0)
										return 1;
									else return 0;
								}		                		
							});
		                	
		                	return ok(views.html.ajax.detail_view.render(result));
		                	
//		                	List<PfProImpMain> lstPfProImpMain = null;
//		                	
////		                	return viewresponse)(response.asJson());
////		                	return views.html.job.rener(parseJobs(response.asJson().asText()));
////		                	return ok("test");
//		                	String body = response.getBody();
//		                	if(body == null) {
//		                		lstPfProImpMain = new ArrayList<PfProImpMain>();
//		                	} else {
//		                		lstPfProImpMain =new MyUtils().json2PfProImpMainList(body);		                		
//		                	}		                	
////		                	return ok(views.html.job.render(jobs));
		         
		                }
				 }
		    );
		    return resultPromise;		    			
	}
}
