package controllers;

import java.util.ArrayList;
import java.util.List;

import models.Job;
import models.JobResult;
import models.JobStatus;
import play.libs.F.Function;
import play.libs.F.Function0;
import play.libs.F.Promise;
import play.libs.WS;
import play.mvc.Controller;
import play.mvc.Http.MultipartFormData;
import play.mvc.Http.MultipartFormData.FilePart;
import play.mvc.Result;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class File extends Controller {

	static final String [] excelFileNames = {"fppro", "pfpro_imgs","pfpro_airinfo", "pfpro_d_scheduled", "pfpro_price", "pfpro_allot", "pfpro_d_imgs"};
	static final String [] excelFileNameDescs =  {"主商品檔", "主商品圖檔","航班資訊", "各團行程內容", "價格檔", "機位數", "各團圖檔"};
	
	public static Result upload() {
//		return ok(views.html.upload.render());
		return ok(views.html.upload2.render());
	}
	
	
	private static String validateExcels(MultipartFormData body) {
		for(int i = 0; i < excelFileNames.length; i++ ) {
			FilePart excelFile = body.getFile(excelFileNames[i]);
			if(excelFile == null) {
				return excelFileNameDescs[i] + " 檔案需要上傳!";
			}			
		}
		return "";
	}
	
	/*
	 * 檢查上傳檔, 並讀出excel內容
	 */
	public static Promise<Result> submit() {
		
		return Promise.promise(new Function0<Result>() {
			String result = "";
			String currentProcessExcelFileNameDesc = "";
			
			@Override
			public Result apply() throws Throwable {
				
				
				MultipartFormData body = request().body().asMultipartFormData();
				String errorMessage = validateExcels(body);
				
				if(errorMessage.equals("")) {
//					return badRequest(errorMessage);
				}

				try {
					for(int i = 0; i < excelFileNames.length; i++) {
						currentProcessExcelFileNameDesc = excelFileNameDescs[i];
						FilePart excelFile = body.getFile(excelFileNames[i]);
						if(excelFile == null) continue;
								
//						FileParser parser = null; //  = new PfProImgsFileParser();
//						Map<String, List<PfProImgs>> mapPfProImgs = parser.parse(excelFile.getFile()); 
					
//						List<PfProImgs> pfProImgs = mapPfProImgs.get("CTS05CI4907A");
//						String jsonString = new ObjectMapper().writeValueAsString(pfProImgs);
					
//					return ok(jsonString);
					
//					WSRequestHolder request = WS.url("http://localhost:9000/json/submit");
//					Response response =  request.get().get(10L, TimeUnit.SECONDS);							
//					Promise<Response> response =  request.post(jsonString);
					
//						String responseResult = WS.url("http://localhost:9000/json/submit")	.post(jsonString).get(10000).getBody();
										
					
//					(response, 10.seconds)
//					String responseResult = "";
//					responseResult += WS.url("http://localhost:9000/json/submit")
//							.post(jsonString)
//							.map(
//							 new Function<WS.Response, String>() {
//					                public String apply(WS.Response response) throws Throwable {
//					                	String bodyText;
//					                	if(response.getStatus() == OK) {
//					                		bodyText = response.getBody();
//					                	} else {
//					                		bodyText = "匯入失敗";
//					                	}					                						                
//					                	return bodyText;
//					                }
//					            }
//							);
					
//				    String responseResult = response.getBody();				    
//						result += utils.DateUtils.getDateTime() + " 匯入單號:" + responseResult + " 處理狀態<br />";
//					
//						pfProImgs = mapPfProImgs.get("CTS05CI4907B");
//						jsonString = new ObjectMapper().writeValueAsString(pfProImgs);
//						responseResult = WS.url("http://localhost:9000/json/submit")	.post(jsonString).get(10000).getBody();
//						result += utils.DateUtils.getDateTime() + " 匯入單號:" + responseResult + " 處理狀態<br />";
//					
//						response().setContentType("text/html; charset=utf-8");
						return ok("<h1>" + result + "</h1>").as("text/html");
				    
						//					String fileName = excel1.getFilename();
						//					System.out.println(fileName);
						//
						//					String contentType = excel1.getContentType();
						//					System.out.println(contentType);
						//
						//					File file = excel1.getFile();

						/*
						 * 使用jxl讀取excel
						 * http://www.andykhan.com/jexcelapi/tutorial.html jar file
						 * 放在 lib/jxl.jar
						 */
					}
					
					return ok("");
				} catch(Throwable ex) {
					return badRequest("匯入檔案: " + currentProcessExcelFileNameDesc + " " + ex.getMessage());
				}
				
				
//				} else {
////					System.out.println("error Missing file");
////					return redirect(routes.Application.upload());
//				}
			}			
		});
	}

	public static Promise<Result> list() {
		
		 final Promise<Result> resultPromise = WS.url("http://localhost:9000/json").get().map(
		            new Function<WS.Response, Result>() {
		                public Result apply(WS.Response response) throws Throwable {
		                	
//		                	return viewresponse)(response.asJson());
//		                	return views.html.job.rener(parseJobs(response.asJson().asText()));
//		                	return ok("test");
		                	
		                	String jobString = response.getBody();
		                	List<Job> jobs = parseJobs(jobString);
//		                	return ok(views.html.job.render(jobs));
		                	return ok("");
		                }
		            }
		    );
		    return resultPromise;		    			
	}
	
	public static Promise<Result> summary() {
		
		 final Promise<Result> resultPromise = WS.url("http://localhost:9000/json/summary").get().map(
		            new Function<WS.Response, Result>() {
		                public Result apply(WS.Response response) throws Throwable {
		                    
		                	int status = response.getStatus();
		                	String body = response.getBody();
		                	if(response.getStatus() == OK) {		                				                	
		                		List<JobStatus> result = parseJobsStatus(body);
		                		return ok(views.html.summary.render(result));
		                	} else {
		                		return internalServerError("連接 Server 失敗!" + body);
		                	}
		                }
		            }
		    );
		    return resultPromise;		    			
		
	}
	
	public static Promise<Result> jobStatus(String id) {
		
		 final Promise<Result> resultPromise = WS.url("http://localhost:9000/json/" + id).get().map(
		            new Function<WS.Response, Result>() {
		                public Result apply(WS.Response response) throws Throwable {
		                    
		                	String jobString = response.getBody();
//		                	List<Job> jobs = parseJobs(jobString);
//		                	return ok(views.html.job.render(jobs));
		                	return ok("");
		                }
		            }
		    );
		    return resultPromise;		    					
	}

	public static Promise<Result> result(String id) {
		
		 final Promise<Result> resultPromise = WS.url("http://localhost:9000/json/result/" + id).get().map(
		            new Function<WS.Response, Result>() {
		                public Result apply(WS.Response response) throws Throwable {
		                    
		                	int status = response.getStatus();
		                	String body = response.getBody();
		                	
		                	if(response.getStatus() == OK) {		                				                	
		                		List<JobResult> result = parseJobResult(body);
		                		return ok(views.html.result.render(result));
		                	} else {
		                		return internalServerError("連接 Server 失敗!" + body);
		                	}
		                	
		                }
		            }
		    );
		    return resultPromise;		    					
	}	
	
	public static List<Job> parseJobs(String jsonString) throws Throwable {
		List<Job> result = new ArrayList<Job>();
		result = new ObjectMapper().readValue(jsonString, new TypeReference<List<Job>>(){});
		
		return result;
//		new ObjectMapper().readValue(要反序列化的json字串, 要binding的類別);
//		if(json != null) {
//			if(json.isArray()) {
//				result = new ObjectMapper().readValue(jsonString, new TypeReference<List<Job>>(){});
////				result = new ObjectMapper().readValues(json, Job.class);
//////				for(JsonNode jsonNode : json) {
//////					Job job = new ObjectMapper().readValue
//////							
//////							Json.fromJson(jsonNode, Job.class);
//////					result.add(job);
//				}
//			} else {
//				Job job = Json.fromJson(json, Job.class);
//				result.add(job);
//			}		
//		}	
	}	
		
	public static List<JobStatus> parseJobsStatus(String jsonString) throws Throwable {
		List<JobStatus> result = new ObjectMapper().readValue(jsonString, new TypeReference<List<JobStatus>>(){});
		
		return result;
	}		
	
	public static List<JobResult> parseJobResult(String jsonString) throws Throwable {
		List<JobResult> result = new ObjectMapper().readValue(jsonString, new TypeReference<List<JobResult>>(){});
		
		return result;
	}	
}
