package utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {

	/**
	* 事前準備：import java.util.Date; import java.text.SimpleDateFormat;
	* 主要是要存入 datebase 欄位格式為datetime的欄位
	*/
	public static String getDateTime(){	
		Date date = new Date();
		return formatDateTime(date);	
	} 
	
	public static String formatDateTime(Date date) {
		SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm");
		String strDate = sdFormat.format(date);
		
		return strDate;
	}
	
//	public static String formatDateTime(String da)
}
