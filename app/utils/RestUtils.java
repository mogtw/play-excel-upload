package utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import models.VndMainVO;
import play.libs.WS;
import play.libs.WS.Response;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import eztravel.core.pojo.frnplatform.PfProImpDetail;
import eztravel.core.pojo.frnplatform.PfProImpMain;
import eztravel.core.pojo.frnplatform.VndMain;
import eztravel.core.pojo.frnplatform.wrapper.PfProOutput;
import eztravel.rest.pojo.common.RestError;
import eztravel.rest.pojo.common.RestResource;

public class RestUtils {
	
	public Integer getNextBGroupNo() throws Exception {
		String jsonStringResult = WS.url("http://ezdevsrvt02.eztravel.com.tw:8080/frnplatform/rest/v1/pfproimport/getNextBGroupNo")
				.get().get(3000).getBody();
		
		Integer	groupNo = new RestUtils().getSimpleRestResult(jsonStringResult);
		
		return groupNo;
	}
	
	public <T> T getSimpleRestResult(String jsonStringResult) throws Exception {
		RestResource<T> restResult = new ObjectMapper().readValue(jsonStringResult, new TypeReference<RestResource<T>>(){});
		
		if(restResult.getErrors() != null) {
			RestError restError = restResult.getErrors().get(0);
			throw new Exception(restError.getMessage());
		} else {
			T result = restResult.getItems().get(0);
			return result; 
		}		
	}	
	
	public List<PfProImpMain> getPfProImpMainResult(String jsonStringResult) throws Exception {
		RestResource<PfProImpMain> restResult = new ObjectMapper().readValue(jsonStringResult, new TypeReference<RestResource<PfProImpMain>>(){});
		
		if(restResult.getErrors() != null) {
			RestError restError = restResult.getErrors().get(0);
			throw new Exception(restError.getMessage());
		} else {
			List<PfProImpMain> result = restResult.getItems();
			if(result == null) {
				result = new ArrayList<PfProImpMain>();
			}
			return result; 
		}		
	}
	
	public List<PfProImpDetail> getPfProImpDetailResult(String jsonStringResult) throws Exception {
		RestResource<PfProImpDetail> restResult = new ObjectMapper().readValue(jsonStringResult, new TypeReference<RestResource<PfProImpDetail>>(){});
		
		if(restResult.getErrors() != null) {
			RestError restError = restResult.getErrors().get(0);
			throw new Exception(restError.getMessage());
		} else {
			List<PfProImpDetail> result = restResult.getItems();
			if(result == null) {
				result = new ArrayList<PfProImpDetail>();
			}
			return result; 
		}		
	}
	
	public String getVndMainJsonString() {
		Response response = WS.url("http://ezdevsrvt02.eztravel.com.tw:8080/frnplatform/rest/v1/common/listAllVendors")
				.get().get(5000);
		
		String result = response.getBody();		
		return result;
	}
			
	public Map<String, VndMainVO> getVndMainVOMap(String jsonStringResult) throws Exception {
		RestResource<VndMain> restResult = new ObjectMapper().readValue(jsonStringResult, new TypeReference<RestResource<VndMain>>(){});
		Map<String, VndMainVO> map = new HashMap<String, VndMainVO>();
		
		if(restResult.getErrors() != null) {
			RestError restError = restResult.getErrors().get(0);
			throw new Exception(restError.getMessage());
		} else {
			List<VndMain> result = restResult.getItems();
			if(result != null) {
				for(VndMain vnd : result) {
					VndMainVO vndMainVo = new VndMainVO(vnd.getVendNo(), vnd.getVendNameCh());
					
					map.put(vnd.getVendNo(), vndMainVo);
				}
			}
			return map; 
		}		
	}
	

	public PfProOutput getPfProOutputResult(String jsonStringResult) throws Exception {
		RestResource<PfProOutput> restResult = new ObjectMapper().readValue(jsonStringResult, new TypeReference<RestResource<PfProOutput>>(){});
		
		if(restResult.getErrors() != null) {
			RestError restError = restResult.getErrors().get(0);
			throw new Exception(restError.getMessage());
		} else {
			PfProOutput result = restResult.getItems().get(0);
			return result; 
		}		
	}	
}
