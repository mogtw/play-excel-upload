package utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import models.Job;
import models.JobStatus;
import models.pojo.FileInfo;
import play.mvc.Http.Cookie;
import play.mvc.Http.MultipartFormData.FilePart;
import play.mvc.Http.Request;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import eztravel.core.pojo.frnplatform.PfProImpMain;
import eztravel.rest.util.common.StringUtils;


public class MyUtils {
		
	public String getVendNoFromFormUrlEncodeMap(Map<String, String[]> map) {
		String vendNo = "";
		
		if(map != null) {									
			String values[] = map.get("vendNo");
			if(values != null & values.length > 0) {
				vendNo = values[0];
			}			
		}
		
		return vendNo;
	}
	
	public String getUserIdFromRequest(Request request) {
		String userId = null;
		
		Cookie cookie = request.cookie("userId");
		
		if(cookie != null) {
			userId = StringUtils.Decode(cookie.value());
		}
		
		return userId;
	}
	
	public List<FileInfo> getImportFileInfoList() {
		List<FileInfo> result = new ArrayList<FileInfo>();
		result.add(new FileInfo("1_prod.xlsx", "主商品檔"));
		result.add(new FileInfo("2_prod_imgs.xlsx", "主商品圖檔"));
		result.add(new FileInfo("3_airinfo.xlsx", "航班資訊"));
		result.add(new FileInfo("4_d_imgs.xlsx", "商品行程圖檔"));
		result.add(new FileInfo("5_scheduled.xlsx", "商品行程檔"));
		result.add(new FileInfo("6_prod_price.xlsx", "商品價格檔"));		
		result.add(new FileInfo("7_allot.xlsx", "機位數檔"));
		
		return result;
	}		
	
	// 將傳進來的 FilePart 依 key 分類
	public Map<String, Map<String, FilePart>> classifyFilePartsByKey(List<FilePart> fileParts) {
		
		Map<String, Map<String, FilePart>> result = new HashMap<String, Map<String, FilePart>>();
		Map<String, FilePart> classifiedFilePars = null;
		
		for(FilePart filePart : fileParts) {
			String key = filePart.getKey().trim();
			classifiedFilePars = result.get(key);
			if(null == classifiedFilePars) {
				classifiedFilePars = new HashMap<String, FilePart>();
				result.put(key, classifiedFilePars);
			}
			classifiedFilePars.put(filePart.getFilename(), filePart);						
		}
		
		return result;
	}
	
	public static List<JobStatus> parseJobsStatus(String jsonString) throws Throwable {
		List<JobStatus> result = new ObjectMapper().readValue(jsonString, new TypeReference<List<JobStatus>>(){});
		
		return result;
	}
	
	public static List<Job> parseJob(String jsonString) throws Throwable {
		List<Job> result = new ObjectMapper().readValue(jsonString, new TypeReference<List<Job>>(){});
	
		return result;
	}
	
	public static List<PfProImpMain> json2PfProImpMainList(String jsonString) throws Throwable {
		List<PfProImpMain> result = new ObjectMapper().readValue(jsonString, new TypeReference<List<PfProImpMain>>(){});
		
		return result;
	}
	
	// 產生 PfProImport.groupCount 
		public Short getGroupCount(int processCount, int separateCount) {
			int result = (processCount / separateCount) + ((processCount % separateCount) > 0 ? 1 : 0);
			return new Short((short) result);
		}
}
