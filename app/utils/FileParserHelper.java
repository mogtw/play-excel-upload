package utils;

import java.math.BigDecimal;

import org.springframework.util.StringUtils;

import jxl.Sheet;

public class FileParserHelper {
	private int currentRow;	// 目前 row
	private int currentColumn;	// 目前 行
	private Sheet sheet;
	
	public FileParserHelper(Sheet sheet) {
		this.sheet = sheet;		
	}
				
	public int getCurrentRow() {
		return currentRow;
	}

	public void setCurrentRow(int currentRow) {
		this.currentRow = currentRow;
	}
	
	public int getCurrentColumn() {
		return currentColumn;
	}

	public void setCurrentColumn(int currentColumn) {
		this.currentColumn = currentColumn;
	}

	public String getCurrentColumnName() {
		String result = null;
		
		if(sheet.getRows() > 0) {
			result = getCellValue(currentColumn, 0);
		}
		return result;
	};
	
	public String getCellValue(int col, int row) {
		this.currentRow = row;
		this.currentColumn = col;
		return sheet.getCell(col, row).getContents();
	}
	
	public String getTrimCellValue(int col, int row) {
		this.currentRow = row;
		this.currentColumn = col;
		return StringUtils.trimWhitespace(sheet.getCell(col, row).getContents());
	}
	
	public Short getShortCellValue( int col, int row ) {
		this.currentRow = row;
		this.currentColumn = col;
		return Short.parseShort(sheet.getCell(col, row).getContents());
	}
	
	public int getIntCellValue( int col, int row ) {
		this.currentRow = row;
		this.currentColumn = col;
		return Integer.parseInt(sheet.getCell(col, row).getContents());
	}
	
	public BigDecimal getBigDecimalCellValue( int col, int row ) {
		this.currentRow = row;
		this.currentColumn = col;
		return new BigDecimal(sheet.getCell(col, row).getContents());
	}
	
}
