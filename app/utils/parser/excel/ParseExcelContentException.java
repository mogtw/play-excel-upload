package utils.parser.excel;

public class ParseExcelContentException extends Exception {
	private static final long serialVersionUID = 7822364316041515107L;
	private int column;
		
	public int getColumn() {
		return column;
	}

	public void setColumn(int column) {
		this.column = column;
	}
 
	public ParseExcelContentException(int column, String message) {		
		super(message);
		this.column = column;
	}	
}
