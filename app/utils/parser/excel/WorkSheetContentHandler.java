package utils.parser.excel;

import java.util.HashMap;
import java.util.Map;

import org.apache.poi.xssf.model.SharedStringsTable;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class WorkSheetContentHandler extends DefaultHandler {

	private SharedStringsTable table = null;	
	private Map<Integer, String> map = new HashMap<Integer, String>();
	private int currentRow = 0;
	private int currentColumn = 0;
	private ExcelRowContentCallback callback;
	private StringBuilder value = new StringBuilder();
	private boolean nextIsString;
	
	public WorkSheetContentHandler(ExcelRowContentCallback callback){
		this.callback = callback;
	}
	
	public void setSharedStringsTable(SharedStringsTable table) {
		this.table = table;
	}
	
	public void setCallback(ExcelRowContentCallback callback) {
		this.callback = callback;
	}

	/* (non-Javadoc)
	 * @see org.xml.sax.helpers.DefaultHandler#characters(char[], int, int)
	 */
	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		value.append(new String(ch,start,length));				
	}

	/* (non-Javadoc)
	 * @see org.xml.sax.helpers.DefaultHandler#endDocument()
	 */
	@Override
	public void endDocument() throws SAXException {

	}

	/* (non-Javadoc)
	 * @see org.xml.sax.helpers.DefaultHandler#startElement(java.lang.String, java.lang.String, java.lang.String, org.xml.sax.Attributes)
	 */
	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		
//		try {
//			writer.write("starElement uri: " + uri + ", localName:" + localName + ", qName: " + qName + "\n");
//			writer.write("attributes:");
//			for(int i =0; i < attributes.getLength(); i++) {
//				writer.write("attributes qname : " + attributes.getQName(i) + 
//						" localName: " + attributes.getLocalName(i) + 
//						" type: " + attributes.getType(i) + 
//						" value: " + attributes.getValue(i) + "\n"
//						);
//			}
//			
//		} catch (IOException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
		
		if(qName.equals("row")) {	// 每一筆 row 開始, 清掉 map 資料
//			map.clear();
			currentRow = Integer.parseInt(attributes.getValue("r"));	// r-r:1			
		} else if(qName.equals("c")){
			String c = attributes.getValue("r");			
			currentColumn = getColumn(c);	// A1 -> 1
			
			String cellType = attributes.getValue("t");
			if(cellType != null && cellType.equals("s")) {
				nextIsString = true;
			} else {
				nextIsString = false;
			}
			
//			try {
//				writer.write("c:" + attributes.toString());
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}						
			//	 attributes.getType(i) - CDATA
			// r - 欄位 A1, B1....
			// t - 型態 inlineStr, n(s:1 數字型態(n)才有)
		}
		else if(qName.equals("v")) {
//			System.out.println("v: " + attributes);	// ?? 有資料		
		}
		else {
//			try {
//				writer.write("startElement-other:" + attributes.toString());
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//				try {
//					writer.write("startElement-other:" +e.toString());
//				} catch (IOException e1) {
//					// TODO Auto-generated catch block
//					e1.printStackTrace();
//				}
//			}
		}
   		value.setLength(0);
	}
	
	/* (non-Javadoc)
	 * @see org.xml.sax.helpers.DefaultHandler#endElement(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
			
//		System.out.println("qName: " + value);
			try {				
				if(nextIsString) {
					int idx = Integer.parseInt(value.toString());	// 新版的 Excel 處理方式 
					value.setLength(0);	
					value.append(new XSSFRichTextString(table.getEntryAt(idx)).toString());
					nextIsString = false;
				}
				
				if(qName.equals("v")){	// cell 的內容 
					//					value = table.getEntryAt(Integer.valueOf(value.trim())).getT();	
					//				System.out.println("v:" + value);
					map.put(currentColumn - 1, value.toString());
			
				} else if(qName.equals("t") || qName.equals("c")) {	// 字串
					map.put(currentColumn - 1, value.toString());
					
				} else if(qName.equals("row")){
					if(currentRow > 1) { 
						callback.processRow(currentRow, map);
					}
					else {
						callback.processHeaderRow(map);
					}
					map.clear();
				} else {
//					writer.write("endElement - else " + qName + "\n");
				}
			} catch(Exception ex) {
				throw new SAXException(ex);
			}		
	}
	
	// 欄位轉換 A1 -> 1
	private int getColumn(String c) {
		int result = 0;
		final int EXCEL_COLUMN_BASE_VALUE = new Character('A').charValue();
		final int NUMBER_OF_ALPHABET = 26;
		for(int i = 0; i < c.length(); i++) {
			Character ch = c.charAt(i);
			if(!Character.isLetter(ch)) break;
			
			result = result * NUMBER_OF_ALPHABET + (ch.charValue() - EXCEL_COLUMN_BASE_VALUE + 1);		
		}
		
		return result;
	}
}
