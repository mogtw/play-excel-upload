package utils.parser.excel;


public interface ExcelReader {

		void read(String excelFile, int sheetNumber, WorkSheetContentHandler contentHandler) throws Exception;
		
}
