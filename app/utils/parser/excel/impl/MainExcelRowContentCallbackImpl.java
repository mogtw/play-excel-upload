package utils.parser.excel.impl;

import java.util.List;
import java.util.Map;

import utils.parser.excel.ExcelRowContentCallback;
import utils.parser.excel.ItemParser;
import utils.parser.excel.ParseExcelContentException;

public class MainExcelRowContentCallbackImpl<T> extends AbstractExcelRowContentCallbackImpl implements ExcelRowContentCallback {
	private List<T> list;
	private String vendNo;
	private ItemParser<T> itemParser;
	
	public MainExcelRowContentCallbackImpl(List<T> list, String vendNo, ItemParser<T> itemParser, String fileDesc) {
		this.list = list;
		this.vendNo = vendNo;
		this.itemParser = itemParser;
		this.setFileDesc(fileDesc);
	}
			
	@Override
	public void processRow(int rowNum, Map<Integer, String> map)
			throws Exception {
		
		try {
			
			setCurrentParseRow(rowNum);
			T item = itemParser.parseItem(vendNo, map);
			list.add(item);
			
		} catch(ParseExcelContentException ex) {
			throw new Exception(getFileDesc() + " 第 " + getCurrentParseRow() + " 筆 " + 
					getColumnName(ex.getColumn()) + " 資料解析錯誤: " + ex.getMessage());
		}		
	}
}
