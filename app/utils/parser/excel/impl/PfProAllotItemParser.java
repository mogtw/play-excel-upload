package utils.parser.excel.impl;
import java.util.Map;

import utils.parser.excel.ItemParser;
import utils.parser.excel.ParseExcelContentException;
import eztravel.core.pojo.frnplatform.PfProAllot;


public class PfProAllotItemParser  extends AbstractItemParser implements ItemParser<PfProAllot> {
	
	@Override
	public PfProAllot parseItem(String vendNo, Map<Integer, String> map)
			throws ParseExcelContentException {
	
		PfProAllot item = new PfProAllot();
		
		item.setPfProdNo(parseString(0, map.get(0)));
		item.setVendNo(parseString(1, map.get(1)));
		
		if(!vendNo.equals(item.getVendNo())) {
			throw new ParseExcelContentException(1, "供應商編號 " + item.getVendNo() + " 不符!");
		}
		
		item.setSaleDt(parseString(2, map.get(2)));
		item.setTotQty(parseInt(3, map.get(3)));
		item.setAllotQty(parseInt(4, map.get(4)));				
		item.setOrdQty(parseInt(5, map.get(5)));
		item.setRemark(parseString(6, map.get(6)));
//		item.setUseYn(parseString(7, map.get(7)));
		item.setUseYn("Y");
		
		return item;		
	}

}
