package utils.parser.excel.impl;

import java.util.Map;

import utils.parser.excel.ItemParser;
import utils.parser.excel.ParseExcelContentException;
import eztravel.core.pojo.frnplatform.PfProPrice;

public class PfProPriceItemParser extends AbstractItemParser implements ItemParser<PfProPrice> {

	@Override
	public PfProPrice parseItem(String vendNo,
			Map<Integer, String> map) throws ParseExcelContentException {
		
		PfProPrice item = new PfProPrice();
		
		item.setPfProdNo(parseString(0, map.get(0)));
		item.setVendNo(parseString(1, map.get(1)));
		
		if(!vendNo.equals(item.getVendNo())) {
			throw new ParseExcelContentException(1, "供應商編號 " + item.getVendNo() + " 不符!");
		}
		
		item.setSaleDt(parseString(2, map.get(2)));
		
		try {
			String roomtype = parseString(3, map.get(3)); 
//			item.setHtlNum("142");	// 先寫死 142, 再加檢核
			item.setHtlNum("14" + roomtype.substring(2));	// 先寫死 142, 再加檢核
//			item.setHtlNum("14" + roomtype);
		} catch(Exception ex) {
			throw new ParseExcelContentException(3, ex.getMessage());
		}
		
		
		try {		
			String tktType = parseString(4, map.get(4));
			if("A01".equals(tktType)) {
				tktType = "391";
			} else if("A02".equals(tktType)) {
				tktType = "393";
			} else if("A03".equals(tktType)) {
				tktType = "394";
			} else if("A04".equals(tktType)) {
				tktType = "3940";
			} else if("A05".equals(tktType)) {
				tktType = "3942";
			}
			item.setCond2Type(tktType);
			
		} catch(Exception ex) {
			throw new ParseExcelContentException(4, ex.getMessage());
		}	
			
		try {
			String bedType = parseString(5, map.get(5));
			if("B01".equals(bedType)) {
				bedType = "251";
			} else if("B02".equals(bedType)) {
				bedType = "252";
			} else if("B03".equals(bedType)) {
				bedType = "253";
			}
			item.setCond3Type(bedType);
		} catch(Exception ex) {
			throw new ParseExcelContentException(5, ex.getMessage());
		}
							
		item.setMinPrice1(parseBigDecimal(6, map.get(6)));
		item.setAgentCost(parseBigDecimal(7, map.get(7)));
		
		return item;		
	}

	

}
