package utils.parser.excel.impl;

import java.util.Map;

import utils.parser.excel.ItemParser;
import utils.parser.excel.ParseExcelContentException;
import eztravel.core.pojo.frnplatform.PfProRef;

public class PfProRefItemParser extends AbstractItemParser implements ItemParser<PfProRef> {

	@Override
	public PfProRef parseItem(String vendNo, Map<Integer, String> map)
			throws ParseExcelContentException {
		
		PfProRef item = new PfProRef();
		
		item.setPfProdNo(parseString(0, map.get(0)));
		item.setVendNo(parseString(1, map.get(1)));

		if (!vendNo.equals(item.getVendNo())) {
			throw new ParseExcelContentException(1, "供應商編號 " + item.getVendNo()
					+ " 不符!");
		}

		item.setFeatures(parseString(17, map.get(17)));
		item.setServiceFee(parseInt(18, map.get(18)));
		item.setDeposit(parseInt(19, map.get(19)));
		item.setTourCitys(parseString(20, map.get(20)));
		item.setTaxes(parseInt(0, "0"));
//		item.setTaxes(parseInt(31, map.get(31)));		
		item.setTourCitysNm(parseString(21, map.get(21)));
		item.setDepartAirportNm(parseString(22, map.get(22)));
		item.setFeeIncludes(parseString(23, map.get(23)));
		item.setExcludingItems(parseString(24, map.get(24)));
		item.setTourHLights(parseString(25, map.get(25)));
		item.setSpPlan(parseString(26, map.get(26)));
		item.setgTourRule(parseString(27, map.get(27)));
		item.setManLimit(parseInt(28, map.get(28)));
		item.setTravelDay(parseInt(29, map.get(29)));
		item.setTravelDayNm(parseString(30, map.get(30)));
		
		return item;
	}	
}
