package utils.parser.excel.impl;

import java.util.Map;

import utils.parser.excel.ItemParser;
import utils.parser.excel.ParseExcelContentException;
import eztravel.core.pojo.frnplatform.PfProAirinfo;

public class PfProAirinfoItemParser extends AbstractItemParser implements ItemParser<PfProAirinfo> {
	
	@Override
	public PfProAirinfo parseItem(String vendNo, Map<Integer, String> map)
			throws ParseExcelContentException {
		
		PfProAirinfo item = new PfProAirinfo();
		
		item.setPfProdNo(parseString(0, map.get(0)));
		item.setVendNo(parseString(1, map.get(1)));
		
		if(!vendNo.equals(item.getVendNo())) {
			throw new ParseExcelContentException(1, "供應商編號 " + item.getVendNo() + " 不符!");
		}
		
		item.setSeqno(parseShort(2, map.get(2)));
		item.setRoundCd(parseShort(3, map.get(3)));
		item.setDepartDate(parseString(4, map.get(4)));
//		item.setQty(parseShort(5, map.get(5)));
		item.setQty(parseShort(5, "-1"));
		item.setAircompany(parseString(5, map.get(5)));				
		item.setAircompanyCode(parseString(6, map.get(6)));
		item.setScheduleNo(parseString(7, map.get(7)));	// 航班編號
		item.setDepartAirportNm(parseString(8, map.get(8)));	
		item.setArriveAirportNm(parseString(9, map.get(9)));
		item.setDepartAirportCode(parseString(10, map.get(10)));	
		item.setArriveAirportCode(parseString(11, map.get(11)));
		item.setDepartTime(parseString(12, map.get(12)));
		item.setArriveTime(parseString(13, map.get(13)));
		
		return item;
		
	}

}
