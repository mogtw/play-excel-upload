package utils.parser.excel.impl;

import java.io.InputStream;
import java.util.Iterator;

import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.eventusermodel.XSSFReader;
import org.apache.poi.xssf.model.SharedStringsTable;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import utils.parser.excel.ExcelReader;
import utils.parser.excel.WorkSheetContentHandler;

public class XlsxReader implements ExcelReader {
	
	public void read(String excelFile, int sheetNumber, WorkSheetContentHandler contentHandler) throws Exception {
		InputStream sheet = null;
		OPCPackage pkg = null;
				
		try {			
			pkg = OPCPackage.open(excelFile);
			
			XSSFReader reader =  new XSSFReader(OPCPackage.open(excelFile));
			XMLReader xmlReader = XMLReaderFactory.createXMLReader("org.apache.xerces.parsers.SAXParser");
			
			// sharedStrings.xml实体
			SharedStringsTable table = reader.getSharedStringsTable();
			contentHandler.setSharedStringsTable(table);
			xmlReader.setContentHandler(contentHandler); //实现该接口的一个实例);
		
			Iterator<InputStream> sheets = reader.getSheetsData();
			if(sheets.hasNext()) {	// 只處理第一頁
				sheet = sheets.next();
				InputSource sheetSource = new InputSource(sheet);
				xmlReader.parse(sheetSource);
			}
			
//			sheet = reader.getSheet("rId"+ (3 + sheetNumber ));	// 第一頁好像是以 rId3 開始?
//			sheet = reader.getSheet("rId"+ (2 ));	// 第一頁好像是以 rId3 開始?

			xmlReader = null;
			
		} finally {			
			IOUtils.closeQuietly(sheet);
			IOUtils.closeQuietly(pkg);
		}
	}
}
	
