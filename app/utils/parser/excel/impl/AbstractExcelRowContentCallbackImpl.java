package utils.parser.excel.impl;

import java.util.HashMap;
import java.util.Map;

import utils.parser.excel.ExcelRowContentCallback;

public abstract class AbstractExcelRowContentCallbackImpl implements ExcelRowContentCallback {
	private String fileDesc;
	private Map<Integer, String> header;
	
	private int currentParseRow;		// 目前 row
			
	public int getCurrentParseRow() {
		return currentParseRow;
	}

	public void setCurrentParseRow(int currentParseRow) {
		this.currentParseRow = currentParseRow;
	}

	@Override
	public void processHeaderRow(Map<Integer, String> map) {
		if(header == null) {
			 header = new HashMap<Integer, String>();
		}
		header.putAll(map);		
	}
	
	public void setFileDesc(String fileDesc) {
		this.fileDesc = fileDesc;
	}		
		
	public String getFileDesc() {
		return fileDesc;
	}

	// 取得欄位名稱
	public String getColumnName(int column) {
		String result = "";
		if(header != null) {
			result = header.get(column);
		}		
		return result != null ? result : "";
	}
	
	protected String buildExcelRowContentReadExceptionMessage(int col, String exceptionMessage) {
		String result = fileDesc + "第 " + getCurrentParseRow() + " 筆 " + 
			getColumnName(col) + " 資料解析錯誤: " + exceptionMessage;
	
		return result;
	}	
}
