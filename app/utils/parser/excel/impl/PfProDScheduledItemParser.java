package utils.parser.excel.impl;

import java.util.Map;

import utils.parser.excel.ItemParser;
import utils.parser.excel.ParseExcelContentException;
import eztravel.core.pojo.frnplatform.PfProDScheduled;

public class PfProDScheduledItemParser extends AbstractItemParser implements ItemParser<PfProDScheduled> {


	@Override
	public PfProDScheduled parseItem(String vendNo, Map<Integer, String> map)
			throws ParseExcelContentException {

		PfProDScheduled item = new PfProDScheduled();
		
		item.setPfProdNo(parseString(0, map.get(0)));
		item.setVendNo(parseString(1, map.get(1)));
		
		if(!vendNo.equals(item.getVendNo())) {	
			throw new ParseExcelContentException(1, "供應商編號 " + item.getVendNo() + " 不符!");
		}
		
		item.setDays(parseString(2, map.get(2)));
		item.setTimeSeq(parseString(3, map.get(3)));
		item.setProcBrief(parseString(4, map.get(4)));
		item.setProcContext(parseString(5, map.get(5)));
		item.setBreakfast(parseString(6, map.get(6)));
		item.setMorningTea(parseString(7, map.get(7)));
		item.setLunch(parseString(8, map.get(8)));
		item.setAfternoonTea(parseString(9, map.get(9)));
		item.setDinner(parseString(10, map.get(10)));
		item.setNightSnack(parseString(11, map.get(11)));
		item.setHtlDesc(parseString(12, map.get(12)));
		item.setHtlDescUrl(parseString(13, map.get(13)));
		
		return item;
	}

}
