package utils.parser.excel.impl;

import java.util.Map;

import utils.parser.excel.ItemParser;
import utils.parser.excel.ParseExcelContentException;
import eztravel.core.pojo.frnplatform.PfProImgs;

public class PfProImgsItemParser extends AbstractItemParser implements ItemParser<PfProImgs> {

	@Override
	public PfProImgs parseItem(String vendNo,
			Map<Integer, String> map) throws ParseExcelContentException {
		
		PfProImgs item = new PfProImgs();
		item.setPfProdNo(parseString(0, map.get(0)));
		item.setVendNo(parseString(0, map.get(1)));
		
		if(! vendNo.equals(item.getVendNo())) {
			throw new ParseExcelContentException(1, "供應商編號 " + item.getVendNo() + " 不符!");
		}
		
		item.setImgSeqno(parseShort(2, map.get(2)));
		item.setImgUrl(parseString(3, map.get(3)));
		item.setImgName(" ");
		
		return item;
	}

}
