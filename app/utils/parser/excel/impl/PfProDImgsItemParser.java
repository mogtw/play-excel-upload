package utils.parser.excel.impl;

import java.util.Map;

import utils.parser.excel.ItemParser;
import utils.parser.excel.ParseExcelContentException;
import eztravel.core.pojo.frnplatform.PfProDImgs;

public class PfProDImgsItemParser extends AbstractItemParser implements ItemParser<PfProDImgs> {

	@Override
	public PfProDImgs parseItem(String vendNo, Map<Integer, String> map)
			throws ParseExcelContentException {
		
		PfProDImgs item = new PfProDImgs();
		
		item.setPfProdNo(parseString(0, map.get(0)));
		item.setVendNo(parseString(1, map.get(1)));
		
		if(!vendNo.equals(item.getVendNo())) {
			throw new IllegalArgumentException("供應商編號 " + item.getVendNo() + " 不符!");
		}
						
		item.setImgSeqno(parseShort(2, map.get(2)));
		item.setDays(parseString(3, map.get(3)));				
		item.setImgUrl(parseString(4, map.get(4)));
//		item.setImgName(parseString(5, map.get(5)));
		item.setImgName(" ");
		
		return item;
	}
		
}
