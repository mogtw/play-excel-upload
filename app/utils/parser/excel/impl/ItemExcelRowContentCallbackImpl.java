package utils.parser.excel.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import utils.parser.excel.ExcelRowContentCallback;
import utils.parser.excel.ItemParser;
import utils.parser.excel.ParseExcelContentException;

public class ItemExcelRowContentCallbackImpl<T> extends AbstractExcelRowContentCallbackImpl implements ExcelRowContentCallback {
	private Map<String, List<T>> maps;
	private String vendNo;
	private ItemParser<T> itemParser;
	
	public ItemExcelRowContentCallbackImpl(Map<String, List<T>> maps, String vendNo, ItemParser<T> itemParser, String fileDesc) {
		this.maps = maps;
		this.vendNo = vendNo;
		this.itemParser = itemParser;
		this.setFileDesc(fileDesc);
	}
			
	@Override
	public void processRow(int rowNum, Map<Integer, String> map)
			throws Exception {
		
		List<T> list;
		
		try {
			
			setCurrentParseRow(rowNum);
			String key = map.get(0);	// 第一行的資料都是 Key 值
			T item = itemParser.parseItem(vendNo, map);
						 
			list = maps.get(key);
			if(list == null) {
				list = new ArrayList<T>();
				maps.put(key, list);
			}
			list.add(item);
			
		} catch(ParseExcelContentException ex) {
			throw new Exception(getFileDesc() + " 第 " + getCurrentParseRow() + " 筆 " + 
					getColumnName(ex.getColumn()) + " 資料解析錯誤: " + ex.getMessage());
		}		
	}
}
