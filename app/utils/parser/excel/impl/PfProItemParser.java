package utils.parser.excel.impl;

import java.util.Map;

import utils.parser.excel.ItemParser;
import utils.parser.excel.ParseExcelContentException;
import eztravel.core.pojo.frnplatform.PfPro;

public class PfProItemParser extends AbstractItemParser implements ItemParser<PfPro> {
	
	@Override
	public PfPro parseItem(String vendNo, Map<Integer, String> map)
			throws ParseExcelContentException {
		
		PfPro item = new PfPro();
		
		item.setPfProdNo(parseString(0, map.get(0)));
		item.setVendNo(parseString(1, map.get(1)));
		
		if(!vendNo.equals(item.getVendNo())) {
			throw new ParseExcelContentException(1, "供應商編號 " + item.getVendNo() + " 不符!");
		}
		
		item.setCountryCd(parseString(2, map.get(2)));	// 國別
		item.setLineCd(parseShort(3, map.get(3)));		// 線別
		item.setProdType("FRN");	
		item.setRegionCd(parseString(4, map.get(4)));
		item.setPfGProdNo(parseString(5, map.get(5)));
		item.setProdNm(parseString(6, map.get(6)));
//		item.setBegValidDt(parseString(8, map.get(8)));
//		item.setEndVallidDt(parseString(9, map.get(9)));
//		item.setBegSaleDt(parseString(10, map.get(10)));
//		item.setEndSaleDt(parseString(11, map.get(11)));
//		item.setSaleType(parseShort(12, map.get(12)));
		item.setSaleType(parseShort(3, "3"));	// 
		item.setUpDt(parseString(7, map.get(7)));
		item.setDownDt(parseString(8, map.get(8)));
		item.setPromotionMsg(parseString(9, map.get(9)));
		item.setRemark(parseString(10, map.get(10)));
		item.setPromotionSeq(parseShort(0, "1"));	// 促銷順位 
		item.setPromotionYn(parseString(11, map.get(11)));
		item.setUpYn("Y");
		item.setBonusProdYn("Y");
		item.setProdPromotion(parseString(12, map.get(12)));
		item.setPrice1(parseBigDecimal(13, map.get(13)));
		item.setDepartArea(parseString(14, map.get(14)));
		item.setPassportYn(parseString(15, map.get(15)));
		item.setB2eYn("Y");
		item.setB2bYn("N");
		item.setDeadline(parseString(16, map.get(16)));

		return item;
	}	
}
