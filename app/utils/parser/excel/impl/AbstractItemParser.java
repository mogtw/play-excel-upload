package utils.parser.excel.impl;

import java.math.BigDecimal;

import eztravel.rest.util.common.StringUtils;
import utils.parser.excel.ParseExcelContentException;

public abstract class AbstractItemParser {

	protected Short parseShort(int col, String value) throws ParseExcelContentException {		
		try {			
			if(StringUtils.fp_isNull(value) != "") {
				return (short)Double.parseDouble(value);
			} else {
				return Short.valueOf("0");
			}			
		} catch(Exception ex) {
			throw new ParseExcelContentException(col, ex.getMessage());
		}
	}

	protected int parseInt(int col, String value) throws ParseExcelContentException {
		try {			
			if(StringUtils.fp_isNull(value) != "") {
				return  (int)Double.parseDouble(value);
			} else {
				return 0;
			}
		} catch(Exception ex) {
			throw new ParseExcelContentException(col, ex.getMessage());
		}				
	}

	protected BigDecimal parseBigDecimal(int col, String value) throws ParseExcelContentException {
		try {					
			if(StringUtils.fp_isNull(value) != "") {
				return new BigDecimal(value);
			} else {
				return BigDecimal.valueOf(0);
			}			
		} catch(Exception ex) {
			throw new ParseExcelContentException(col, ex.getMessage());
		}
	}
	
	protected String parseString(int col, String value) {
		return value;		
	}
		
}
