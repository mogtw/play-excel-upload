package utils.parser.excel;

import java.util.Map;

public interface ExcelRowContentCallback {	
	void processHeaderRow(Map<Integer, String> map);
    void processRow(int rowNum, Map<Integer, String> map) throws Exception;
}
