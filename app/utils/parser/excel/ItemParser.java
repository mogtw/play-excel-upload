package utils.parser.excel;

import java.util.Map;

public interface ItemParser<T> {		
	T parseItem(String vendNo, Map<Integer, String> map) throws ParseExcelContentException;		
}
